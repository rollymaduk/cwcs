const GRAPH_FIND_PACKAGE = 'MATCH (p:PACKAGE {id:{packageId}})';
const GRAPH_FIND_USER = 'MATCH (u:USER {id:{userId}})';
const GRAPH_FIND_REFERRAL = 'MATCH (u2:USER{id:{uplineId}}) WHERE size((u2)-[:DOWNLINE]->())<{maxDownlines}';
const GRAPH_FIND_DOWNLINES = 'OPTIONAL MATCH (:USER{id:{uplineId}})-[:DOWNLINE*]->(n)' +
    ' WHERE size((n)-[:DOWNLINE]->())<{maxDownlines}';
const GRAPH_FIND_UPLINE = 'MATCH (upline:USER{id:user.id})';
const GRAPH_RELATE_USER_TO_PACKAGE = 'MERGE (u)-[pt:PARENT]->(p)';
const GRAPH_RELATE_USER_TO_UPLINE = 'MERGE (u)<-[d:DOWNLINE]-(upline)';
const GRAPH_DOWNLINE_EXISTS = 'MATCH (u) WHERE NOT exists((u)<-[:DOWNLINE]-())';
const GRAPH_FIND_RELATED_UPLINES = 'MATCH pth=(u)-[:DOWNLINE*1..]->(d) WHERE ' +
    'length(pth)<=p.level';
const GRAPH_ALL_DOWNLINES = 'MATCH ({id:{userId}})-[:DOWNLINE*]->(d)';

export const GRAPH_ADD_USER = 'MERGE (u:USER {email:{email},' +
    'fullname:{fullname},id:{userId},isActive:{isActive},createdAt:TIMESTAMP()})';

export default{
  GRAPH_ADD_PACKAGE: 'MERGE (p:PACKAGE {title:{title},id:{packageId},maxDownlines:{maxDownlines}' +
        ',group:{group},level:{level},createdAt:TIMESTAMP()}) RETURN  p',
  GRAPH_REMOVE_PACKAGE: `${GRAPH_FIND_PACKAGE} OPTIONAL ${GRAPH_FIND_PACKAGE}<--(u)
   DETACH DELETE p,u`,

  GRAPH_FIND_REWARD_UPLINES: `MATCH (:USER{id:{userId}})<-[:DOWNLINE*]-(u)-[pt:PARENT]->(p) 
WITH u,p,pt ${GRAPH_FIND_RELATED_UPLINES} WITH u,pt,p,count(d) as cnt 
WHERE cnt=toInteger(p.maxDownlines) WITH u,p,pt MATCH(p2:PACKAGE{group:p.group}) WHERE 
p2.maxDownlines>p.maxDownlines WITH pt,HEAD(COLLECT(p2)) as package,u as upline ORDER BY
 package.maxDownlines MERGE (upline)-[:PARENT]->(package) DELETE pt RETURN upline, package`,

  GRAPH_DEACTIVATE_USERS: `MATCH(p:PACKAGE{group:{group}}) WITH p ORDER BY p.maxDownlines
   DESC LIMIT 1 MATCH (p)<-[:PARENT]-(u) WITH p,u ${GRAPH_FIND_RELATED_UPLINES} WITH 
   p,u,count(d) as cnt WHERE cnt=p.maxDownlines SET u.isActive=false RETURN u,p,cnt`,

  GRAPH_REMOVE_USER: `${GRAPH_FIND_USER} DETACH DELETE u`,

  GRAPH_UPDATE_USER: `${GRAPH_FIND_USER} SET u += {edit} RETURN u`,

  GRAPH_ADD_USER_TO_PACKAGE: `${GRAPH_FIND_PACKAGE} ${GRAPH_ADD_USER}
   WITH u,p ${GRAPH_RELATE_USER_TO_PACKAGE} RETURN u,p`,

  GRAPH_UPDATE_PACKAGE: `${GRAPH_FIND_PACKAGE} SET p += {edit} RETURN  p`,

  GRAPH_ADD_USER_TO_UPLINE: `${GRAPH_FIND_REFERRAL} WITH collect(DISTINCT {key:id(u2),id: u2.id})
   as referral ${GRAPH_FIND_DOWNLINES} WITH n,referral ORDER BY n.createdAt WITH 
   head(referral + collect(DISTINCT {key:id(n),id: n.id})) AS user ${GRAPH_FIND_UPLINE}
    WITH upline ${GRAPH_FIND_PACKAGE} ${GRAPH_ADD_USER} WITH u,p,upline
     ${GRAPH_RELATE_USER_TO_PACKAGE} WITH u,p,pt,upline ${GRAPH_DOWNLINE_EXISTS}
      ${GRAPH_RELATE_USER_TO_UPLINE} RETURN upline,u,p,pt,d`,

  GRAPH_GET_PACKAGE_USERCOUNT: 'MATCH (p:PACKAGE{id:{packageId}})<-[]-(u) RETURN count(u) as userCount',

  GRAPH_GET_ALL_DOWNLINES: `${GRAPH_ALL_DOWNLINES} RETURN d`,
  GRAPH_GET_DOWNLINES_COUNT: `${GRAPH_ALL_DOWNLINES} RETURN count(d) as downlines`,

};
