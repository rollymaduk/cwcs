import neo4j from 'neo4j';
import queries,{GRAPH_ADD_USER} from './queries';


export default class Neo4JServer {
  constructor(configs) {
    const {url,username,password} = configs;
    this.db = new neo4j.GraphDatabase({
      url,
      auth: {username,password},
    });
  }

  _execute({query,params,options = {lean:true}},tranx) {
    const self = this;
    return new Promise((resolve,reject) => {
      if (tranx) {
        return tranx.cypher({query,params,...options},(err,result) => {
          (err) ? reject(err) : resolve(result);
        });
      }
      return self.db.cypher({query,params,...options},(err,result) => {
        (err) ? reject(err) : resolve(result);
      });
    });
  }

  getTransaction() {
    return this.db.beginTransaction();
  }

  addUserToPackage({packageId,user},tranx) {
    const params = {packageId,...user};
    return this._execute({query: queries.GRAPH_ADD_USER_TO_PACKAGE,
        params},tranx);
  }

  addUserToUpline({uplineId,maxDownlines,packageId,user},tranx) {
    const params = {packageId,uplineId,maxDownlines,...user};
    return this._execute({query: queries.GRAPH_ADD_USER_TO_UPLINE,params},tranx);
  }

  addUser(user,tranx) {
    const params = {...user};
    return this._execute({query: GRAPH_ADD_USER,params},tranx);
  }

  updateUser({userId,edit},tranx) {
    const params = {userId,edit,tranx};
    return this._execute({query: queries.GRAPH_UPDATE_USER,params},tranx);
  }

  rewardUserUplines(userId,tranx) {
    const params = {userId};
    return this._execute({query: queries.GRAPH_FIND_REWARD_UPLINES,params},tranx);
  }
  getDownlines(userId,tranx) {
    const params = {userId};
    return this._execute({query: queries.GRAPH_GET_ALL_DOWNLINES,params},tranx);
  }
  getDownlineCount(userId,tranx) {
    const params = {userId};
    return this._execute({query: queries.GRAPH_GET_DOWNLINES_COUNT,params},tranx);
  }
  removeUser(userId,tranx) {
    const params = {userId};
    return this._execute({query: queries.GRAPH_REMOVE_USER,params},tranx);
  }
  addPackage({title,packageId,group,maxDownlines,level},tranx) {
    const params = {title,packageId,group,maxDownlines,level};
    return this._execute({query: queries.GRAPH_ADD_PACKAGE,params},tranx);
  }
  updatePackage({packageId,edit},tranx) {
    const params = {packageId,edit};
    return this._execute({query: queries.GRAPH_UPDATE_PACKAGE,params},tranx);
  }
  removePackage(packageId,tranx) {
    const params = {packageId};
    return this._execute({query: queries.GRAPH_REMOVE_PACKAGE,params},tranx);
  }
}

export const getNeo4JServer = (options = {}) => {
  const {
        NEO4J_HOST: url,
        NEO4J_PASS: password,
        NEO4J_USER: username,
    } = process.env;
  return new Neo4JServer({url,password,username,...options});
};

