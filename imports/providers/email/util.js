const API_PATH = '/api/v1/send'; const API_METHOD = 'POST';
import {get,curry} from 'lodash/fp';
import {set} from 'lodash';
const cache = {};
import moment from 'moment';
const getCacheCreatedAt = get('createdAt');

export const prepareMailBody = (data,templateMapper = {}) => {
  const {template: templateKey,data: template_data = {},address} = data;
  return {
    template: templateMapper[templateKey] || templateKey,
    template_data,
    recipient: {
      address
    }
  };
};

export const prepareData = (data,templateMapper) => {
  if (data) {
    return {
      method: API_METHOD,
      path: API_PATH,
      body: prepareMailBody(data,templateMapper)
    };
  }
  return null;
};

export const send = (mail,api) => {
  if (mail) {
    return new Promise((resolve,reject) => {
      return api.send(mail,(err,result) => {
        (err) ? reject(err) : resolve(result);
      });
    });
  }
  return Promise.reject(new Error('message argument not provided!'));
};

export const sendABatch = (mails,api) => {
  if (mails && mails.length) {
    return new Promise((resolve,reject) => {
      return api.batch(mails,(err,result) => {
        (err) ? reject(err) : resolve(result);
      });
    });
  }
  return Promise.reject(new Error('message argument not provided!'));
};

export const fetchTemplates = (api,duration = '1h') => {
  const spanType = duration.charAt(1);
  const spanLength = Number(duration.charAt(0));
  const timeout = moment.duration(spanLength,spanType).asMilliseconds();

  return new Promise((resolve, reject) => {
    const now = new Date().getTime();
    if ((now - getCacheCreatedAt(cache) || 0) > timeout) {
      cache.createdAt = null;
      cache.templates = null;
    }
    if (cache.templates) {
      return resolve(cache.templates);
    }

    api.getTemplates((err, templates) => {
      if (err) {
        return reject(err);
      }
      let result = templates;
      if (templates && typeof templates === 'object') {
        result = {};
        templates.forEach((template) => {
          set(result,template.name,template.id);
        });
      }
      cache.createdAt = (new Date()).getTime();
      cache.templates = result;
      resolve(result);
    });
  });
};
