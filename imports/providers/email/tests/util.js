let regeneratorRuntime = require('regenerator-runtime');
const {describe,it,beforeEach} = global;
import {expect} from 'chai';
import merge from 'deepmerge';
import {spy,stub} from 'sinon';
import sinon from 'sinon'
import {prepareData,sendABatch,fetchTemplates} from '../util';
const FAKE_ADDRESS = 'fake@address.com';
const FAKE_TEMPLATE = 'fakeTemplate';
const FAKE_MESSAGE = 'fake message';
const HALF_HOUR =1800000;
const ONE_HOUR_HALF =5400000;
const FIRST_CALL_RESULT = 'data from service';
const SECOND_CALL_RESULT = 'second data from service';
const now = new Date()


describe('util',function () {

  describe('prepareData',function () {
      const result = {
          path: '/api/v1/send',
          method: 'POST',
          body: {
              recipient: {
                  address: FAKE_ADDRESS
              },
              template: '',
              template_data: {}
          }
      };
      beforeEach(function () {
          this.data = {
              template: FAKE_TEMPLATE,
              address: FAKE_ADDRESS,
              data: {foo: 'foo',bar: 'bar'},
          };

          this.templateMapper = {fakeTemplate: 'template-code'};

      });
    it('should prepare data for mail api',function () {
      const {templateMapper,data} = this;
      const expectedBody = {body: {template: 'template-code',template_data: data.data}};
      expect(prepareData(data,templateMapper)).to.deep.equal(merge(result,expectedBody));
    });

    it('should return template key as template when mapper is empty',function () {
      const {data} = this;
      expect(prepareData(data).body.template).to.be.equal(FAKE_TEMPLATE);
    });

    it('should return null with no data args',function () {
      expect(prepareData()).to.be.equal(null);
    });
  });

  describe('sendABatch', function () {
    beforeEach(function () {
      this.api = stub({batch: () => {}});

    });
    afterEach(function () {
      this.api.batch.reset();
    });

    it('should return message on success', async function() {
      const {api} = this;
      api.batch.yields(null,'success data');
      const res = await sendABatch([ 'mail1','mail2' ],api);
      console.log(res);
      expect(res).to.be.equal('success data');
    });

      it('should return error on failure', async function() {
          const {api} = this;
          api.batch.yields({message:'oops!'},null);
          try{
              const res = await sendABatch([ 'mail1','mail2' ],api);

          }catch(err){
              expect(err.message).to.be.equal('oops!');
          }

      });

    it('should return error when mail args is empty or null',async function () {
        const {api} = this;
        try{
            const res =await sendABatch([],api);
            expect(res).to.be.equal('success data');
        }catch(err){
          expect(err.message).to.equal('message argument not provided!')
        }
    });

  });

});

