const regeneratorRuntime = require('regenerator-runtime');
const {describe,it} = global;
import {expect} from 'chai';
import {spy,stub,useFakeTimers} from 'sinon';
import sinon from 'sinon';
import {fetchTemplates} from '../util';
const HALF_HOUR = 1800000;
const ONE_HOUR_HALF = 5400000;
const FIRST_CALL_RESULT = 'data from service';
const SECOND_CALL_RESULT = 'second data from service';
const now = new Date();

describe('fetchData', () => {
    /* it.skip('', async function() {
        const api=  stub({getTemplates:()=>{}})
        const clock = useFakeTimers(now.getTime())
        clock.restore()
        api.getTemplates.reset()
    })*/

  it.skip('should return data from cache when less than timeout',async function () {
    const api = stub({getTemplates: () => {}});
    const clock = useFakeTimers(now.getTime());
    api.getTemplates.onCall(0).yields(null,FIRST_CALL_RESULT);
    api.getTemplates.onCall(1).yields(null,SECOND_CALL_RESULT);
    await fetchTemplates(api);
    clock.tick(HALF_HOUR);
    await fetchTemplates(api);
    expect(api.getTemplates.callCount).to.equal(1);
        // expect(result).to.equal(FIRST_CALL_RESULT);
    clock.restore();
    api.getTemplates.reset();
  });

    it.skip('should return data from api when more than timeout', async function (){
        const api=  stub({getTemplates:()=>{}})
        const clock = useFakeTimers(now.getTime())
        api.getTemplates.onCall(0).yields(null,FIRST_CALL_RESULT);
        api.getTemplates.onCall(1).yields(null,SECOND_CALL_RESULT);
        await fetchTemplates(api);
        clock.tick(ONE_HOUR_HALF);
        await fetchTemplates(api);
        expect(api.getTemplates.callCount).to.equal(2);
        // expect(result).to.equal(SECOND_CALL_RESULT);
        clock.restore()
        api.getTemplates.reset()
    });

    it('should return mapped object on success ',async function(){
        const api=  stub({getTemplates:()=>{}});
        api.getTemplates.yields(null,[{name:'templateName',id:"templateId"}]);
        const result = await fetchTemplates(api);
        expect(result).to.deep.equal({templateName:"templateId"})
        api.getTemplates.reset()
    });

     it.skip('should throw error on error',async function(){
         const api=  stub({getTemplates:()=>{}});
         try{
             api.getTemplates.yields({message:'oops!'},null);
             const result = await fetchTemplates(api);
             expect(result).to.deep.equal([{templateName:"templateId"}])
             api.getTemplates.reset()
         }catch(err){
             expect(err.message).to.equal('oops!')
             api.getTemplates.reset()

         }
     })
});
