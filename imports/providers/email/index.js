import {compose,chunk,map,flatten} from 'lodash/fp';
import sendWithUs from 'sendwithus';
import {isArray} from 'lodash';
import co from 'co';
const defaultOptions = {batchSize: 10};
import {fetchTemplates,sendABatch,prepareData,send,prepareMailBody} from './util';
import extendSendWithUs from './extendedSendwithus';


export default function (apiKey,options) {
  const opts = {...options,...defaultOptions};
  const api = extendSendWithUs(sendWithUs(apiKey));
  return {
    create(data) {
      return co(function* () {
        const templateMaps = yield fetchTemplates(api) || {};
        if (isArray(data)) {
          const batchMails = chunk(opts.batchSize);
          const prepareBatchData = map((item) => prepareData(item,templateMaps));
          const sendBatches = map((mails) => sendABatch(mails,api));
          const result = yield Promise.all(compose(sendBatches,batchMails,prepareBatchData)(data));
          return flatten(result);
        }
        const mail = prepareMailBody(data,templateMaps);
        return yield send(mail,api);

      });
    }
  };
}


