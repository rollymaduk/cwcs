const validate = require('validate.js')
import dot from 'dot-object';
import {get,set} from 'lodash';

export default function schemaToValidation(constraint,values) {
  const validated = validate(values,constraint);
  if (validated) {
    Object.keys(validated).forEach((key) => {
      set(validated,key,get(validated,[ key,[ 0 ] ]));
    });
    return dot.object(validated);
  }
  return validated;


}
