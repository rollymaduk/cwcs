import {curry} from 'lodash/fp';
import queryBuilder from './queryBuilder';


/* handle errors in fetch data function*/
export default curry((query,params,callback) => {
  query.setParams(queryBuilder(params));
  if (callback) {
    const pageNumber = params && params.pageNumber || 0;
    query.fetch((err,data) => {
      callback.call(this,{data,pageNumber});
    });
  }
  return query;
});
