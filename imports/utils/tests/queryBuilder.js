const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import queryBuilder from '../queryBuilder';

describe('utils.queryBuilder',() => {
  it('returns a valid query object when given a request',function () {
    const request = {pageNumber: 2,pageSize: 5};
    const expected = {selector: {},options: {limit: 5,skip: 5}};
    expect(queryBuilder(request)).to.deep.equal(expected);
  });

  it('should remove skip value when pageSize 0 ',function () {
    const request = {pageSize: 0};
    const expected = {selector: {},options: {limit: 10}};
    expect(queryBuilder(request)).to.deep.equal(expected);
  });

  it('should remove limit value when pageNumber is 0 ',function () {
    const request = {selector: {},pageNumber: 0};
    const expected = {selector: {},options: {limit: 10}};
    expect(queryBuilder(request)).to.deep.equal(expected);
  });
});
