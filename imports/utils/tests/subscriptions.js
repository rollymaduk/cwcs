const {describe, it,afterEach,beforeEach} = global;
import {expect} from 'chai';
import {spy, stub} from 'sinon';
import subscriptions,{setSubsReady} from '../subscriptions';

describe('package_type.lib.subscriptions', () => {
  const Tracker = {autorun: stub()};

  const Constants = {
    publications: {PACKAGE_TYPE_LIST: 'mock_pkg_list'},
    localStates: {PACKAGE_TYPE_PARAMS: 'mock_pkg_params',
            PACKAGE_TYPE_SUBS_READY: 'mock_subs_params'}

  };
  const Meteor = stub({subscribe: () => {}});
  Meteor.subscribe.returns({ready: () => true});
  Tracker.autorun.yields();
  beforeEach(function () {
    this.LocalState = stub({set: () => {},get: () => {}});
  });

  afterEach(function () {
    this.LocalState.set.reset();
    this.LocalState.get.reset();
  });

  it.skip('should not subscribe with no pageNumber and or pageSize params',function () {
    const {LocalState} = this;
    LocalState.get.returns({pageSize: 0,pageNumber: 0});
   // LocalState.get.returns(Constants.localStates.PACKAGE_TYPE_PARAMS)
    const context = {Meteor,Constants,LocalState,Tracker};
    subscriptions(context);
    // console.log(ready.callCount)
    expect(LocalState.set.args[1]).to.deep.equal([ '@packageTypes.subscription.ready',false ]);
  });


  it.skip('should subscribe with  pageNumber and  pageSize params',function () {
    const {LocalState} = this;
    LocalState.get.returns({pageSize: 1,pageNumber: 10});
        // LocalState.get.returns(Constants.localStates.PACKAGE_TYPE_PARAMS)
    const context = {Meteor,Constants,LocalState,Tracker};
    subscriptions(context);
    expect(LocalState.set.args[1]).to.deep.equal([ '@packageTypes.subscription.ready',true ]);
  });
});

describe('lib.utils.subsReady',function () {
  it('should set subsReady with publication name',function () {
    const LocalState = {set: spy()};
    const AppSubs ={}
    setSubsReady({LocalState,AppSubs},'fake_pub','fake_value');
    expect(LocalState.set.args[0]).to.deep
        .equal([ '@application.subs.ready',{fake_pub: 'fake_value'} ]);
  });
});
