const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {getPackageTitle} from '../index';

describe('util.getPackageTitle',function () {
  it('should concatenate category,level,stage',() => {
    const packageType = {category: 'Cat1',stage: 'Stage1',level: 'Lev1'};
    expect(getPackageTitle(packageType,'-')).to.equal('Cat1-Stage1-Lev1');
  });
});
