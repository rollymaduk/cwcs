const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import schemaToValidator from '../schema_to_validation';

describe('utils.schema_to_validator',function () {
  it('should return a valid error object ',function () {
    const constraint = {firstName: {presence: true}};
    const myObject = {};
    expect(schemaToValidator(constraint,myObject))
        .to.deep.equal({firstName: 'First name can\'t be blank'});
  });

  it('should return a valid error object for multiple objects'
      ,function () {
        const constraint = {
          firstName: {presence: true},
          lastName: {presence: true},
        };
        const myObject = {};
        expect(schemaToValidator(constraint,myObject))
              .to.deep.equal({
                firstName: 'First name can\'t be blank',
                lastName: 'Last name can\'t be blank'
              });

      });

  it('should return empty object when valid'
        ,function () {
          const constraint = {
            firstName: {presence: true}
          };
          const myObject = {firstName: 'foo'};
          expect(schemaToValidator(constraint,myObject))
                .to.deep.equal(undefined);

        });
  it('should return nested object when passed nested object'
        ,function () {
          const constraint = {
            'person.firstName': {presence: true}
          };
          const myObject = {};
          expect(schemaToValidator(constraint,myObject))
                .to.deep.equal({person:{firstName: 'Person first name can\'t be blank'}});

        });
});
