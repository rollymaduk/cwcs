import {User} from '../../lib/models';
import {queryBuilder} from '../../imports/utils';


export default function (params,callbackFn) {
  const UserModel = new User();
  if (params.selector.userId) {
    const filter = queryBuilder(params);
    return UserModel.getDownlines(filter,(err,data) => {
      if (callbackFn) {
        callbackFn({pageNumber: params.pageNumber,data});
      }
      return data;
    });

  }
}
