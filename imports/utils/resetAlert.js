import {globalStates} from '../../lib/constants/index';

export default function ({LocalState}) {
  LocalState.set(globalStates.APPLICATION_ALERTS,{
    title: '',
    show: false,
    text: '',
  });
}
