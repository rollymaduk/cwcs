import schemaToValidation from './schema_to_validation';
import queryBuilder from './queryBuilder';
import resetAlert from './resetAlert';
import showModalAlert from './showModalAlert';
import fetchData from './fetchData';
import fetchDataStatic from './fetchDataStatic';
import getUserDownlines from './getDownlines';
import {join} from 'lodash';
import {Random} from 'meteor/random';
const {$} = global;
const SEPARATOR = '-';

export {
    schemaToValidation,
    queryBuilder,
    resetAlert,
    showModalAlert,
    fetchData,
    fetchDataStatic,
    getUserDownlines,
};

export const toggleBodyClass = (state,className) => () => {
  if (state) {
    $('body').toggleClass(className,state);
  } else {
    $('body').toggleClass(className);
  }

};


export const createRouterGroup = (router,options) => {
  const {prefix,name,triggersEnter = [],triggersExit = []} = options;
  if (router && prefix && name) {
    return router.group({prefix,name,triggersEnter,triggersExit});
  }
  return null;
};

export const getPackageTitle = (packageType) => {
  const {category,level,stage} = packageType;
  return join([ category,stage,level ],SEPARATOR);
};

export const getFullName = (user) => {
  if (user && user.profile) {
    const {firstName,lastName} = user.profile;
    return join([ firstName,lastName ],' ');
  }
  return null;
};

export const getGenderPresets = () => {
  return [ {value: 'M',label: 'Male'},{value: 'F',label: 'Female'} ];
};

const getToken = () => {
  return {
    token: Random.secret(),
    when: new Date()
  };
};

export const getVerifyEmailToken = (address) => {
  return {...getToken(),address};
};

export const getResetEmailToken = (email) => {
  return {...getToken(),email,reason: 'reset'};
};

export const hasAccessFor = ({Meteor,Roles},roles,err) => {
  const userId = Meteor.userId();
  const allow = Roles.userIsInRole(userId,roles);
  if (!allow && err) {
    throw new Meteor.Error(err.statusCode, err.message);
  }
  return allow;
};


