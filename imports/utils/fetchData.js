import {curry} from 'lodash/fp';
import queryBuilder from './queryBuilder';


/* handle errors in fetch data function*/
export default curry((query,params,callback,tracker) => {
  const filters = queryBuilder(params);
  const allowSkip = params.allowSkip || true;
  tracker.autorun(() => {
    query.setParams(filters);
    const handle = query.subscribe();
    if (handle.ready() && callback) {
      const data = query.fetch({allowSkip});
      const pageNumber = params && params.pageNumber || 0;
      callback.call(this,{data,pageNumber});
      // c.stop();
    }
  });

  return query;
});
