import {globalStates} from '../../lib/constants/index';

export default function ({LocalState},{title,text,type,data}) {
  LocalState.set(globalStates.APPLICATION_ALERTS,{
    title,text,type,data,show: true
  });
}
