import merge from 'deepmerge';

export default function (request) {
  let {selector,options,pageNumber,pageSize:limit} = requestNormalizer(request);
  const skip = ((pageNumber || 1) * limit) - limit;
  limit = limit || 10;
  options = (skip) ? {...options,limit,skip} : {...options,limit};
  return {selector,options};
}

export const requestNormalizer = (request) => {
  const baseRequest = {selector: {},options: {},pageNumber: 0,pageSize: 0};
  return merge(baseRequest,request);
};
