import React  from 'react';
import PropTypes from 'prop-types';
import IboxHeader from './ibox_header';
import classnames from 'classnames';



const Ibox = (props) => {
  const {actionComponent,children,title,className,headerClass} = props;
  const IboxClasses = classnames(className,'ibox','float-e-margins')
  return (
        <div className={IboxClasses}>
            <IboxHeader className={headerClass} actionComponent={actionComponent} title={title}/>
            <div className="ibox-content">
                {children}
            </div>
        </div>
    );
};


Ibox.propTypes = {};
Ibox.defaultProps = {};

export default Ibox;

