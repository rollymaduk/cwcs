import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Knob from 'react-canvas-knob';


class CustomKnob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value || 0
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({value: nextProps.value});
  }

  render() {
    return (
            <Knob
                {...this.props}
            />
        );
  }
}

CustomKnob.propTypes = {};
CustomKnob.defaultProps = {};

export default CustomKnob;

