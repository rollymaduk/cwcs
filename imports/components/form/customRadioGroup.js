import React from 'react';
import PropTypes from 'prop-types';
import {buildHandler} from 'react-form/lib/formInputs/util';
import {RadioGroup,Radio} from 'react-icheck';
import HOC from './hoc';

const CustomRadioGroup = ({api,onBlur,onChange,noTouch,...rest}) => {
  const {setValue, getValue, setTouched} = api;
  const {options,name} = rest;
  return ((options.length) ?
      <RadioGroup
          name={name}
      value={getValue()}
      onChange={buildHandler(onChange,(e) => setValue(e.target.value,noTouch))}
      onBlur ={buildHandler(onBlur,() => setTouched)}
      >
          {options.map((option,i) => (
              <Radio
                  key={i}
                  radioClass="iradio_square-green"
                  value={option.value}
                  label={option.label}
          />))}
      </RadioGroup> : null);
};

CustomRadioGroup.propTypes = {
  options: PropTypes.array
};
CustomRadioGroup.defaultProps = {};

export default HOC(CustomRadioGroup);
