import React from 'react';
import PropTypes from 'prop-types';
import { WithContext as AutoSuggest} from 'react-tag-input';
import {buildHandler} from 'react-form/lib/formInputs/util';
import HOC from './hoc';

function AutoSuggestTag({api,onBlur, noTouch,tags = [],suggestions = [], ...rest}) {
  const {setValue, getValue, setTouched} = api;
  const handleAddition = (tag) => {
    const _tags = getValue(tags);
    const _item = {id: _tags.length + 1,text: tag};
    _tags.push( _item );
    setValue(_tags,noTouch);
  };
  const handleDelete = (i) => {
    const _tags = getValue(tags);
    _tags.splice(i,1);
    setValue(_tags,noTouch);
  };
  return (
        <AutoSuggest
            {...rest}
            handleAddition={handleAddition}
            handleDelete={handleDelete}
            handleInputBlur={buildHandler(onBlur,() => setTouched())}
            tags={getValue(tags)}
            suggestions={suggestions}
        />
    );
}

AutoSuggestTag.propTypes = {};
AutoSuggestTag.defaultProps = {};

export default HOC(AutoSuggestTag);
