import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {FormField} from 'react-form';

function Field({field,component,...rest}) {
  const {label,hasLabel = true,className} = rest;
  const classNames = classnames(className,'form-control');
  return (
      <FormField field={field}>
          {({getError,getTouched}) => {
            const formGroupClasses = classnames('form-group',{'has-error': getError() && getTouched()});
            return (
                <div className={formGroupClasses}>
                    {(label && hasLabel) ? <label className="control-label">{label}</label> : null}
                    { React.createElement(component,{field,...rest,className: classNames})}
                </div>
            );
          } }
      </FormField>
      );
}

Field.propTypes = {
  field: PropTypes.string.isRequired,
  component: PropTypes.any
};
Field.defaultProps = {};

export default Field;
