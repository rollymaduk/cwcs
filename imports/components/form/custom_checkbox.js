import React from 'react';
import PropTypes from 'prop-types';
import {Checkbox} from 'react-icheck';
import HOC from './hoc';
import {buildHandler} from 'react-form/lib/formInputs/util';

function CustomCheckbox({api,onBlur,onChange,noTouch,...rest}) {
  const {setValue, getValue, setTouched} = api;
  return (
        <Checkbox
            {...rest}
            checked={getValue(false)}
            checkboxClass="icheckbox_square-green"
            onChange={buildHandler(onChange,(e) => setValue(e.target.checked,noTouch))}
            onBlur ={buildHandler(onBlur,() => setTouched)}
        />
    );
}

CustomCheckbox.propTypes = {};
CustomCheckbox.defaultProps = {};

export default HOC(CustomCheckbox) ;
