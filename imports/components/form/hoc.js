import {FormInput} from 'react-form';
import React from 'react';
export default (Component) => ({ field, showErrors, errorBefore, isForm,...rest}) => {
  return (
    <FormInput field={field} showErrors={showErrors} errorBefore={errorBefore} isForm={isForm}>
        {(api) => <Component {...rest} api={{...api}}/>}
    </FormInput>
 );
};
