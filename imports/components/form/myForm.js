import React,{Component,createElement} from 'react';
import {Form} from 'react-form';
import LoadingButton from '../loadingButton';
import classnames from 'classnames';

export default function createForm(Fields) {
  return class myForm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isBusy: false
      };
      this.handleSubmit = this.handleSubmit.bind(this);
      this.submitDone = this.submitDone.bind(this);
    }

    submitDone() {
      this.setState({isBusy: false});
    }

    handleSubmit(values,state,props) {
      const {handleSubmit,controlState = true} = this.props;
      if (controlState) {
        this.setState({isBusy: true});
      }
      handleSubmit(values,state,props,this.submitDone);
    }

    render() {
      const {defaultValues,handleValidate,isBusy,buttonLabel,
                buttonClass,showButton = true,customActionButtons,actionProps,...rest} = this.props;
      const buttonClasses = classnames('btn',{'btn-primary btn-md': !buttonClass},buttonClass);
      return (
                <Form
                    onSubmit={this.handleSubmit}
                    validate={handleValidate}
                    defaultValues={defaultValues}
                >
                    {({submitForm}) => (
                        <form onSubmit={submitForm}>
                            <Fields {...rest}/>
                            {(showButton) ?
                                <LoadingButton
                                    className={buttonClasses}
                                    loading={isBusy || this.state.isBusy}
                                    type="submit"
                                    label={buttonLabel}/> : null}
                            {(customActionButtons) ?
                                createElement(customActionButtons,{...actionProps}) : null}
                        </form>)
                    }
                </Form>
            );
    }
    };
}

