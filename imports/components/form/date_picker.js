import React from 'react';
import PropTypes from 'prop-types';
import {buildHandler} from 'react-form/lib/formInputs/util';
import DatePicker from 'react-bootstrap-date-picker';
import HOC from './hoc';

const CustomDatePicker = ({api,onBlur,onChange,noTouch,...rest}) => {
  const {setValue, getValue, setTouched} = api;
  return (
        <DatePicker
            {...rest}
            value={getValue(new Date().toISOString())}
            onChange={buildHandler(onChange,(value) => setValue(value,noTouch))}
            onBlur ={buildHandler(onBlur,() => setTouched)}
        />
    );
};

CustomDatePicker.propTypes = {};
CustomDatePicker.defaultProps = {};

export default HOC(CustomDatePicker);
