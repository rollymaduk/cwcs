import React from 'react';
import PropTypes from 'prop-types';
import SweetAlert from 'sweetalert-react';
import {omit,get} from 'lodash/fp';
const getOnConfirm = get('onConfirm')
const omitOnConfirm = omit([ 'onConfirm' ]);
const getData = get('data')

const ModalAlert = ({modal,modalActions}) => {
  const onConfirm = getOnConfirm(modalActions)
  const data = getData(modal);
  return (
      <SweetAlert
            onConfirm={() => onConfirm(data)}
            {...omitOnConfirm(modalActions)}
            {...modal}
    />
    );
};

ModalAlert.propTypes = {
  modalActions: PropTypes.object,
  modal: PropTypes.object.isRequired
};
ModalAlert.defaultProps = {};

export default ModalAlert;
