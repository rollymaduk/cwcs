import React from 'react';
import classnames from 'classnames';
const DropDownButton = ({menus,status,className,handleClick}) => {
  const classNames = classnames('btn','dropdown-toggle',{'btn-primary': !className},className);
  return (
        <div className="btn-group">
            <button data-toggle="dropdown"
                    className={classNames} aria-expanded="false">
                {status} <span className="caret"/>
            </button>
            <ul className="dropdown-menu">
                {(menus && menus.length) ? menus.map((menu,i) => (
                    <li key={i}>
                        <a href="#" onClick={() => handleClick(menu.value)}>{menu.label}</a>
                    </li>
                )) : null}
            </ul>
        </div>
    );
};

DropDownButton.propTypes = {};
DropDownButton.defaultProps = {};

export default DropDownButton;
