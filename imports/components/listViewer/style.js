export const styleConfig = {
  classNames: {
    Table: 'table table-bordered table-striped',
    PreviousButton: 'btn btn-sm btn-default m-t-xs',
    NextButton: 'btn btn-sm btn-default m-t-xs',
    Pagination: 'pagination form-inline',
    PageDropdown: 'form-control input-sm',
  }};
