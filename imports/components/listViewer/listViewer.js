import Griddle, {plugins,RowDefinition,ColumnDefinition} from 'griddle-react';
import EnhancedComponent from './EnhancedComponent';
import {pick} from 'lodash/fp';
import React from 'react';
import {components} from './custom';
import {styleConfig} from './style';
import PropTypes from 'prop-types';
const pickEvents = pick([ 'onNext','onGetPage','onPrevious','onFilter' ]);

function prepareColumn(column) {
  if (typeof column === 'string') {
    return {id: column};
  }
  return (column && column.customComponent) ?
      {...column,customComponent: EnhancedComponent(column.customComponent)} : {...column};
}


function ListViewer(props) {
  const {currentPage ,pageSize,recordCount,fields = [],data = [],actionComponent} = props;
  const columns = (actionComponent) ? fields.concat('actions') : fields;
  return (
      <div className="table-responsive">
        <Griddle
            data={data}
        events={{...pickEvents(props)}}
        components={{...components}}
            styleConfig={{...styleConfig}}

        pageProperties={{
          currentPage,
          pageSize,
          recordCount,
        }}
        >{(fields.length) ?
            <RowDefinition>

                {columns.map((column,i) => (
                    (actionComponent && column === 'actions') ?
                        <ColumnDefinition
                            key={i}
                            id="actions"
                            customComponent={EnhancedComponent(actionComponent)}
                        /> :
                    <ColumnDefinition
                        key={i}
                        {...prepareColumn(column)}
                    />
                ))}

            </RowDefinition> : null}

        </Griddle>
      </div>
    );
}

ListViewer.propTypes = {};
ListViewer.defaultProps = {};

export default ListViewer;
