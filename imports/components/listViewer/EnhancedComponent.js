import {connect} from 'react-redux';
import {selectors} from 'griddle-react';

const rowDataSelector = (state, { griddleKey }) => {
  return state
        .get('data')
        .find(rowMap => rowMap.get('griddleKey') === griddleKey)
        .toJSON();
};
export default connect((state,props) => {
  return {rowData: rowDataSelector(state,props)};
});
