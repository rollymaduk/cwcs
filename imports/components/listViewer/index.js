import React, {Component} from 'react';
import ListViewer from './listViewer';
import {get,pick} from 'lodash/fp';
const fetchData = get('fetchData');
const pickProps = pick([ 'fields','actionComponent' ]);
const pickStateProps = pick([ 'currentPage','pageSize' ]);
const pickEvents = pick([ 'onNext','onGetPage','onPrevious','onFilter' ]);

class ListViewerContainer extends Component {
  constructor(props,context) {
    super(props);
    this.initTableState(this.props);
    this.onFilter = this.onFilter.bind(this);
    this.onGetPage = this.onGetPage.bind(this);
    this.onPrevious = this.onPrevious.bind(this);
    this.onNext = this.onNext.bind(this);
    this.updateTableState = this.updateTableState.bind(this);
  }


  initTableState(props) {

    this.state = {
      currentPage: props && props.startPage || 1,
      pageSize: props && props.pageSize,
      recordCount: props && props.recordCount
    };

  }


  updateTableState({data,pageNumber: currentPage}) {
    this.setState({ data, currentPage });
  }

  onNext() {
    const {state,props,updateTableState} = this;
    const {currentPage,pageSize} = state;
    const pageNumber = currentPage + 1;
    fetchData(props)({pageNumber,pageSize},updateTableState);
  }

  onFilter(filterText) {
    const {state,props,updateTableState} = this;
    const {pageSize} = state;
    fetchData(props)({selector: {$text: {$search: filterText}},allowSkip: false,pageSize},
        updateTableState);
  }

  onPrevious() {
    const {state,props,updateTableState} = this;
    const {currentPage,pageSize} = state;
    const pageNumber = currentPage - 1;
    fetchData(props)({pageNumber,pageSize},updateTableState);
  }

  onGetPage(pageNumber) {
    const {state,props,updateTableState} = this;
    const {pageSize} = state;
    fetchData(props)({pageNumber,pageSize},updateTableState);
  }

  render() {
    const {props,state} = this;

    return (
            <ListViewer data={state.data || props.data}
                        recordCount={state.recordCount || props.recordCount}
                {...pickEvents(this)}
                 {...pickProps(props)} {...pickStateProps(state)} />
        );
  }
}

ListViewerContainer.propTypes = {};
ListViewerContainer.defaultProps = {};

export default ListViewerContainer;

