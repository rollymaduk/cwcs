import Filter from './filter';
import Layout from './layout';
import SettingsWrapper from './settingsWrapper';
export const components = {
  Filter,
  Layout,
  SettingsWrapper,
};
