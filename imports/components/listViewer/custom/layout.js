import React from 'react';

const Layout = ({Table, Pagination, Filter, SettingsWrapper, className, style}) => (
    <div className={className} style={style}>
        <div className="row">
            <SettingsWrapper />
            <Filter />
        </div>
        <Table />
        <Pagination />
    </div>
)

export default Layout;