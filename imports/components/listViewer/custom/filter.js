import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
/* todo connect button to filter event*/
const Filter = ({className,style,setFilter}) => {
  const classNames = classnames('input-sm','form-control',className);
  return (
      <div className="col-sm-3">
        <div className="input-group">
            <input type="text" onChange={e => setFilter(e.target.value)}
                   placeholder="Search" style={style} className={classNames}/>
                <span className="input-group-btn">
                    <button type="button" className="btn btn-sm btn-primary"> Go!</button>
                </span>
        </div>
      </div>
    );
};

Filter.propTypes = {};
Filter.defaultProps = {};

export default Filter;
