import React from 'react';
import classnames from 'classnames';

const LinkButton = ({color,size,to,label,className}) => {
  const classNames = classnames(`btn-${size}`,'btn',`btn-${color}`,className);
  return (
        <a href={to} className={classNames}>{label}</a>
    );
};

LinkButton.propTypes = {};
LinkButton.defaultProps = {
  color: 'primary',
  size: 'xs',
  to: '#',
  label: 'create new'
};

export default LinkButton;
