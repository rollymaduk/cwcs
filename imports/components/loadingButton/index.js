import LaddaButton from 'react-ladda';
import React from 'react';
import PropTypes from 'prop-types';


function Button({label,handleClick,...rest}) {
  return (
        <LaddaButton
            onClick={handleClick}
            {...rest}
        >
            {(label) ? label : 'Submit'}
        </LaddaButton>
    );
}

Button.propTypes = {
  label: PropTypes.string,
  handleClick: PropTypes.func
};
Button.defaultProps = {};

export default Button;
