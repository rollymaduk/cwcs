import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function IboxHeader({title,className,actionComponent}) {
  const classNames = classnames(className,'ibox-tools');
  return (
        <div className="ibox-title">
            <h5>{title}</h5>
            <div className={classNames}>
                {actionComponent}
            </div>
        </div>
    );
}

IboxHeader.propTypes = {
  title: PropTypes.string
};
IboxHeader.defaultProps = {};

export default IboxHeader;
