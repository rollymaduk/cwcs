import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


function Widget({label,icon,text,metric,color,to}) {
  const iconClass = classnames('fa','fa-4x',`fa-${icon}`);
  const classNames = classnames('widget',`${color}-bg`,
      'p-lg','text-center')
  return (
       <a href={to}>
           <div className={classNames}>
            <div className="m-b-md">
                <i className={iconClass}/>
                <h1 className="m-xs">{metric}</h1>
                <h3 className="font-bold no-margins">
                    {label}
                </h3>
                <small>{text}</small>
            </div>
         </div>
       </a>
    );
}

Widget.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.string,
  metric: PropTypes.oneOfType([ PropTypes.string,PropTypes.number ]),
  text: PropTypes.string,
};
Widget.defaultProps = {};

export default Widget;
