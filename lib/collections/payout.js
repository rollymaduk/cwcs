import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';

const Payouts = new Mongo.Collection('payout');
Payouts.addLinks({
  user: {
    collection: Meteor.users,
    type: 'one',
    field: 'userId'
  }
});
export default Payouts;
