import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';
const Timelines = new Mongo.Collection('timelines');

Timelines.addLinks({
  user: {
    type: 'one',
    collection: Meteor.users,
    field: 'userId'
  }
});

export default Timelines;
