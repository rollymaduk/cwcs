import {Meteor} from 'meteor/meteor';
import PackageTypes from './package_type';
import {User} from '/lib/models';
import Payouts from './payout';
import {sumBy,get} from 'lodash/fp';
import Timelines from './timeline';
import {Promise} from 'meteor/promise';
const sumByDownlines = sumBy('minimumDownlines');
const sumByLoanAmount = sumBy('loanAmount');
const userModel = new User();
const getEmail = get('emails.0.address')

export default Meteor.users.addLinks({
  packageType: {
    type: 'one',
    collection: PackageTypes,
    field: 'packageTypeId'
  },
  timelines: {
    collection: Timelines,
    inversedBy: 'user'
  },
  payouts: {
    collection: Payouts,
    inversedBy: 'user',
  },
  totalDownlines: {
    resolve(user) {
      const downlines = PackageTypes.find({category: user.category}
      ,{fields: {minimumDownlines: 1}}).fetch();
      return sumByDownlines(downlines);
    }
  },
  totalEarnings: {
    resolve(user) {
      const earnings = PackageTypes.find({category: user.category}
              ,{fields: {loanAmount: 1}}).fetch();
      return sumByLoanAmount(earnings);
    }
  },
  downlines: {
    resolve(user) {
      return userModel.getDownlineCount({userId: getEmail(user)});
    }
  }
});

