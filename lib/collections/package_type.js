import {Mongo} from 'meteor/mongo';
import {Meteor} from 'meteor/meteor';
const PackageTypes = new Mongo.Collection('package_types');

PackageTypes.addLinks({
  users: {
    collection: Meteor.users,
    inversedBy: 'packageType'
  }
});


export default PackageTypes;
