import {Meteor} from 'meteor/meteor';
import {getFullName} from '/imports/utils';

Meteor.users.addReducers({
  fullName: {
    body: {
      profile: 1
    },
    reduce(object) {
      return getFullName(object);
    }
  }
});

export default function (queryParams) {
  return Meteor.users.createQuery({
    $filters: {
      roles: {$nin: [ '@admin' ]}
    },
    $filter({filters,params,options}) {
      Object.keys(params.options).forEach(key => {
        options[key] = params.options[key];
      });
      Object.keys(params.selector).forEach(key => {
        filters[key] = params.selector[key];
      });
    },
    isActive: 1,
    emails: 1,
    fullName: 1,
    profile: 1,
    billing: 1,
    account: 1,
    packageType: {name: 1}
  },queryParams);
}
