import {Payouts} from '../collections';
import {getFullName} from '/imports/utils';

Payouts.addReducers({
  fullName: {
    body: {
      user: {
        profile: {
          firstName: 1,
          lastName: 1,
        }
      }
    },
    reduce(object) {
      return getFullName(object.user);
    }
  }
});

export default function () {
  return Payouts.createQuery({
    $filter({filters,params,options}) {
      Object.keys(params.options).forEach(key => {
        options[key] = params.options[key];
      });
      Object.keys(params.selector).forEach(key => {
        filters[key] = params.selector[key];
      });
    },
    details: 1,
    packageType: 1,
    status: 1,
    loanAmount: 1,
    updatedAt: 1,
    user: {
      profile: {
        firstName: 1,
        lastName: 1
      },
      account: {
        bankDetails: {
          bankName: 1,
          accountNumber: 1,
        }
      }
    },

  });
}




