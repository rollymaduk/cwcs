// import {Users} from '/lib/collections';
import {Meteor} from 'meteor/meteor';
import {getFullName} from '/imports/utils';

import {sumBy,round,multiply,divide,compose,curry,get} from 'lodash/fp';

const divider = curry((path,object) => {
  return compose(divide,get(path))(object);
});

const getPercent = curry((numerator,denominator,object) => {
  return compose(round,
      multiply(100),
      divider(numerator,object),
      get(denominator))(object);
});

const getEarningsPercent = getPercent('earnings','totalEarnings');
const getDownlinesPercent = getPercent('downlines','totalDownlines');
const sumByLoan = sumBy('loanAmount');

// const Users = Meteor.users
Meteor.users.addReducers({
  earnings: {
    body: {
      payouts: {
        loanAmount: 1,
      }
    },
    reduce(object) {
      return sumByLoan(object.payouts);
    }
  },

  earningsPercent: {
    body: {
      totalEarnings: 1,
      earnings: 1
    },
    reduce(object) {
      return getEarningsPercent(object);
    }
  },
  downlinesPercent: {
    body: {
      totalDownlines: 1,
      downlines: 1
    },
    reduce(object) {
      const result = getDownlinesPercent(object);
      return (result > 100) ? 100 : result;
    }
  },
  fullName: {
    body: {
      profile: 1
    },
    reduce(object) {
      return getFullName(object);
    }
  }
});

export default function () {
  return Meteor.users.createQuery({
    $filters: {
      roles: {$nin: [ '@admin' ]}
    },
    $filter({filters,params,options}) {
      Object.keys(params.options).forEach(key => {
        options[key] = params.options[key];
      });
      Object.keys(params.selector).forEach(key => {
        filters[key] = params.selector[key];
      });
    },
    isActive: 1,
    emails: 1,
    fullName: 1,
    profile: 1,
    earnings: 1,
    timelines: {
      title: 1,
      description: 1,
      createdAt: 1,
    },
    category: 1,
    totalEarnings: 1,
    totalDownlines: 1,
    earningsPercent: 1,
    downlinesPercent: 1,
    billing: 1,
    downlines: 1,
    account: 1,
    packageType: {name: 1,stage: 1,level: 1}
  });
}
