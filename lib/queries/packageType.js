import {PackageTypes} from '../collections';

PackageTypes.addReducers({
  totalUsers: {
    body: {
      users: {_id: 1}
    },
    reduce(object) {
      return object && object.users && object.users.length || 0;
    }
  }
});

export default function () {
  return PackageTypes.createQuery({
    $filter({filters,params,options}) {
      options.skip = params.options.skip;
      options.limit = params.options.limit;
      Object.keys(params.selector).forEach(key => {
        filters[key] = params.selector[key];
      });
    },
    reward: 1,
    createdAt: 1,
    updatedAt: 1,
    minimumDownlines: 1,
    name: 1,
    loanAmount: 1,
    category: 1,
    stage: 1,
    level: 1,
    totalUsers: 1,
  });
}



