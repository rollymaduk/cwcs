import {getPublishName} from './util';
import {check} from 'meteor/check';
import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';



export default function (Collection,selector = {},namespace ) {
  const publishName = getPublishName(Collection,namespace);
  check(publishName, String);

  if (Meteor.isServer) {
    Meteor.startup(function () {
      Meteor.publish(publishName, function () {
        let count = 0;
        let initializing = true;

        const handle = Collection.find(selector).observeChanges({
          added: (id) => {
            count += 1;
            if (!initializing) {
              this.changed(publishName,'countId', { count });
            }
          },
          removed: (id) => {
            count -= 1;
            this.changed(publishName, 'countId', { count });
          }
                // We don't care about `changed` events.
        });

        initializing = false;
        this.added(publishName, 'countId', { count });
        this.ready();
        this.onStop(() => handle.stop());
      });
    });
  }

  if (Meteor.isClient) {
      /* todo extract into npm or atmosphere lib add subscription params alterations*/
    const collection = new Mongo.Collection(publishName);
    const handle = Meteor.subscribe(publishName);
    return {collection,name: publishName,handle};
  }

}

