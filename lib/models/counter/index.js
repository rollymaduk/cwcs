import counter from './collection_count';
import {getPublishName} from './util';

const Counters = [];
export default function (collections) {
  collections.forEach(item => {
    Counters.push(counter(item.collection,item.selector,item.namespace));
  });
  return {
    getCount: (Collection,namespace) => {
      const publishName = getPublishName(Collection,namespace);
      const myCounter = Counters.find(item => item.name === publishName);
      if (myCounter.handle.ready()) {
        return myCounter.collection.findOne().count;
      }
      return 0;
    }
  };
}
