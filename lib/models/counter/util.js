import {get} from 'lodash/fp';
const getName = get('className');
const DEFAULT_NAMESPACE = '@global';
export const getPublishName = (Collection,namespace = DEFAULT_NAMESPACE) =>
    `${namespace}.${getName(Collection)}_count`;
