import {Class,Enum} from 'meteor/jagi:astronomy';
import {PackageTypes} from '../collections';

export const Category = Enum.create({
  name: 'Category',
  identifiers: {RUBY: 'RUBY',DIAMOND: 'DIAMOND',SAPPHIRE: 'SAPPHIRE',FARM110: 'FARM110' }
});

export const Level = Enum.create({
  name: 'Level',
  identifiers: { Level1: 1,Level2: 2, Level3: 3,Level4: 4 }
});

export const Stage = Enum.create({
  name: 'Stage',
  identifiers: { STAGE1: 'STAGE1',STAGE2: 'STAGE2',STAGE3: 'STAGE3' }
});

const PackageType = Class.create({
  name: 'PackageType',
  collection: PackageTypes,
  secure: true,
  fields: {
    category: {
      type: Category,
    },
    stage: {
      type: Stage,
    },
    level: {
      type: Level,
    },
    reward: {
      type: [ Object ],
      default: [],
    },
    minimumDownlines: {
      type: Number,
      default: 0
    },
    loanAmount: {type: Number,default: 0},

    name: {
      type: String,
      optional: true,
      index: 'text'
    }
  },
  behaviors: {
    timestamp: {
      hasCreatedField: true,
      createdFieldName: 'createdAt',
      hasUpdatedField: true,
      updatedFieldName: 'updatedAt'
    }
  }

});

export const PackageTypePresets = {
  CATEGORIES: Category.getIdentifiers().map((category) => ({label: category,value: category})),
  LEVELS: Level.getIdentifiers().map((level,index) => ({label: level,value: index + 1})),
  STAGES: Stage.getIdentifiers().map((stage) => ({label: stage,value: stage})),
};

export default PackageType;
