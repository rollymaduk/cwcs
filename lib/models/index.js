import PackageType,{PackageTypePresets} from './package_type';
import User from './user';
import Payout from './payout';
import AdminUser from './admin_user';
import Timeline from './timeline';
import initCounters from './counter';
// import {ADMIN_USER,NORMAL_USER} from '/server/lib/constants';
const Counters = initCounters(
  [
        {collection: PackageType},
        {collection: Payout},
        {collection: Payout,
            selector: {status: 'new'},
            namespace: '@new'},
        {collection: User,selector: {roles: {$in: [ '@user' ]}}},
        {collection: User,
            selector: {roles: {$in: [ '@admin' ]}},
            namespace: '@admin'
        },
  ]);
export {
  PackageType,
  PackageTypePresets,
  User,
    Payout,
    Timeline,
    AdminUser,
    Counters
};
