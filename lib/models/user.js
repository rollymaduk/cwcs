import {Meteor} from 'meteor/meteor';
import {Class,Enum} from 'meteor/jagi:astronomy';
import {getFullName} from '../../imports/utils';
import {Users} from '../collections';


const AddressSchema = Class.create({
  name: 'Address',
  fields: {
    line1: {type: String,optional: true},
    line2: {type: String,optional: true},
    city: {type: String,optional: true},
    state: {type: String,optional: true},
    country: {type: String,optional: true}
  }
});
const PaymentSchema = Class.create({
  name: 'Payment',
  fields: {
    bankName: String,
    tellerNumber: {type: String,optional: true},
    accountNumber: {type: String,optional: true},
  }
});


export const Gender = Enum.create({
  name: 'Gender',
  identifiers: {MALE: 'M',FEMALE: 'F'}
});

const ProfileSchema = Class.create({
  name: 'Profile',
  fields: {
    firstName: String,
    lastName: String,
    address: {type: AddressSchema,optional: true},
    DOB: Date,
    gender: {type: Gender,default: Gender.MALE},
    phone: {type: String,optional: true}
  },
  indexes: {
    fullName: {
      fields: {
        firstName: 'text',
        lastName: 'text',
      }
    }
  }
});

const AccountSchema = Class.create({
  name: 'Account',
  fields: {
    referral: {type: String},
    bankDetails: PaymentSchema,
  }
});

const User = Class.create({
  name: 'User',
  collection: Users,
  fields: {
    username: {type: String,optional: true},
    emails: [ Object ],
    packageTypeId: {type: String,optional: true},
    profile: {type: ProfileSchema,optional: true},
    services: Object,
    category: {type: String,optional: true},
    billing: PaymentSchema,
    account: AccountSchema,
    isActive: {
      type: Boolean,
      default: false
    },
    roles: [ String ],
    fullName: {
      type: String,
      resolve: (doc) => {
        return getFullName(doc);
      }}
  },
  behaviors: {
    timestamp: {
      hasCreatedField: true,
      createdFieldName: 'createdAt',
      hasUpdatedField: true,
      updatedFieldName: 'updatedAt'
    }
  }
});



export default User;
