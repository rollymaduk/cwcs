import {Class,Enum} from 'meteor/jagi:astronomy';
import {Timelines} from '../collections';
import {EVENT_MILESTONE, EVENT_TYPE_SIGNUP} from '/lib/constants';

const TimelineType = Enum.create({
  name: 'TimelineType',
  identifiers: {
    milestone: EVENT_MILESTONE,
    signup: EVENT_TYPE_SIGNUP }
});

const Timeline = Class.create({
  name: 'Timeline',
  collection: Timelines,
  secure: true,
  fields: {
    title: String,
    description: String,
    type: TimelineType,
    userId: {
      type: String,
      optional: true
    }
  },
  behaviors: {
    timestamp: {
      hasCreatedField: true,
      createdFieldName: 'createdAt',
      hasUpdatedField: true,
      updatedFieldName: 'updatedAt'
    }
  }

});

export default Timeline;
