import {Class,Enum} from 'meteor/jagi:astronomy';
import {Payouts} from '../collections';

export const Status = Enum.create({
  name: 'Status',
  identifiers: {processing: 'processing',new: 'new',complete: 'complete' }
});


const PayoutDetailSchema = Class.create({
  name: 'PayoutDetail',
  fields: {
    item: String,
    isFulfilled: {type: Boolean,default: false}
  }
});


const Payout = Class.create({
  name: 'Payout',
  collection: Payouts,
  fields: {
    packageType: String,
    loanAmount: {type: Number,default: 0},
    userId: {type: String,optional: true},
    details: [ PayoutDetailSchema ],
    status: {type: Status,default: Status.new }
  },
  behaviors: {
    timestamp: {
      hasCreatedField: true,
      createdFieldName: 'createdAt',
      hasUpdatedField: true,
      updatedFieldName: 'updatedAt'
    }
  }

});

export default Payout;
