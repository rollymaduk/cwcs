import {Class} from 'meteor/jagi:astronomy';
import {Gender} from './user';
import {getFullName} from '../../imports/utils';
import {Users} from '../collections';

const AdminProfileSchema = Class.create({
  name: 'AdminProfile',
  fields: {
    firstName: {type: String},
    lastName: {type: String},
    phone: {type: String,optional: true},
    gender: {type: Gender,default: Gender.MALE},
  }
});

const AdminAccountSchema = Class.create({
  name: 'AdminAccount',
  packageType: {type: String,default: 'no-package'}
});


const AdminUser = Class.create({
  name: 'UserAdmin',
  collection: Users,
  fields: {
    emails: [ Object ],
    profile: AdminProfileSchema,
    isActive: {type: Boolean,default: false},
    account: AdminAccountSchema,
    fullName: {
      type: String,
      resolve: (doc) => {
        return getFullName(doc);
      }
    }
  }
});

export default AdminUser;
