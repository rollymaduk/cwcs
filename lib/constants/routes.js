export default {
  ROUTE_PUBLIC_PREFIX: '/public',
  ROUTE_PUBLIC_NAME: '@public',
  ROUTE_APP_PREFIX: '/app',
  ROUTE_APP_NAME: '@app',
  ROUTE_ACCOUNTS_PREFIX: '/accounts',
  ROUTE_ACCOUNTS_NAME: '@accounts',
  ROUTE_ADMIN_PREFIX: '/admin',
  ROUTE_ADMIN_NAME: '@admin.route.name',
  ROUTE_HOME_PATH: '/',
  ROUTE_HOME_NAME: '@home.route'

};
