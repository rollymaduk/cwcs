export default {
  APPLICATION_ALERTS: '@application.alerts',
  APPLICATION_SUBS_READY: '@application.subs.ready',
};
