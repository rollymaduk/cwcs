import globalStates from './states';
import globalRoutes from './routes';
const EVENT_MILESTONE = '@milestone';
const EVENT_TYPE_SIGNUP = '@sign_up';
const TIMELINE_TEXT = 'You move up to {name} and earned!';
const TIMELINE_TITLE = '{name}';
const PAGE_SIZE = 25;
export {
    globalStates,
    globalRoutes,
    PAGE_SIZE,
    EVENT_MILESTONE,
    EVENT_TYPE_SIGNUP,
    TIMELINE_TEXT,
    TIMELINE_TITLE,
};
