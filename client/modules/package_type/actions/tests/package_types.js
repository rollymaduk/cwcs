const {describe, it,afterEach,beforeEach} = global;
import {expect} from 'chai';
import {spy, createStubInstance,stub} from 'sinon';
import actions from '../package_types';

class PackageTypeClass {
  create() {
  }
}

describe('package_type.actions.package_types', () => {
  describe('create',function () {
    beforeEach(function () {
      this.pkgStub = createStubInstance(PackageTypeClass);
      const PackageType = spy(() => this.pkgStub);
      this.LocalState = {set: spy()};
      this.ShowAlerts = spy();
      this.FlowRouter = {go: spy()};
      this.pkgType = {category: 'Ruby'};
      this.Models = {PackageType};
    });
    afterEach(function () {
      this.pkgStub.create.reset();
      this.LocalState.set.reset();
      this.ShowAlerts.reset();
    });

    it('should create new package',function () {
      const {Models,pkgType,pkgStub} = this;
      actions.create({Models},pkgType);
      expect(pkgStub.create.callCount).to.equals(1);
      // expect(pkgStub.create.args[0][0]).to.equal(pkgType);
    });


    it('should set APPLICATION_ALERT with error message on error',function () {
      const {Models,pkgType,pkgStub,ShowAlerts} = this;
      const err = {message: 'oops!'};
      pkgStub.create.yields(err);
      actions.create({Models,ShowAlerts},pkgType);
      expect(ShowAlerts.args[0]).to.deep.equal([ err.message,'error' ]);
    });

    it('should set APPLICATION_ALERT with success message on success',function () {
      const {Models,pkgType,pkgStub,ShowAlerts,FlowRouter} = this;
      const result = {};
      pkgStub.create.yields(null,result);
      actions.create({Models,ShowAlerts,FlowRouter},pkgType);
      expect(ShowAlerts.args[0]).to
          .deep.equal([ 'Package was created successfully!','success' ]);
    });

    it('should redirect to package list page on success',function () {
      const {Models,pkgType,pkgStub,ShowAlerts,FlowRouter} = this;
      const result = {};
      pkgStub.create.yields(null,result);
      actions.create({Models,ShowAlerts,FlowRouter},pkgType);
      expect(FlowRouter.go.args[0]).to
              .deep.equal([ '@package_type.list' ]);
    });
  });

  describe('validate',function () {
    it('should validate object ',function () {
      const Utils = {schemaToValidation: spy()};
      const errors = [ {path: 'field',message: 'error message'} ];
      actions.validate({Utils},errors);
      expect(Utils.schemaToValidation.callCount).to.equal(1);
    });
  });

  describe('fetchData',function () {
    const pageNumber = 1; const pageSize = 25;
    beforeEach(function () {
      this.LocalState = stub({set() {},get() {}});
    });

    afterEach(function () {
      this.LocalState.set.reset();
      this.LocalState.get.reset();
    });

    it.skip('should set package_type_params with pageNumber and pageSize',function () {
      const {LocalState} = this;
      actions.fetchData({LocalState},{pageNumber,pageSize});
      expect(LocalState.set.args[0]).to.deep
          .equal([ '@packageTypes.params',{selector: {},options: {},pageNumber,pageSize} ]);
    });

    it.skip('should set package_type_subs to false on successfull subscription',function () {
      const {LocalState} = this;
      const callback = spy();
      LocalState.get.returns(true);
      const Collections = {PackageTypes: {find: stub()}};
      Collections.PackageTypes.find.returns({fetch: () => 'fakeCollection'});
      actions.fetchData({LocalState,Collections},{pageNumber,pageSize},callback);
      expect(LocalState.set.args[1]).to.deep.equal([ '@packageTypes.subscription.ready',false ]);
    });

    it.skip('should call callback with data and pageNumber when subscription is ready',function () {
      const {LocalState} = this;
      const callback = spy();

      const Collections = {PackageTypes: {find: stub()}};
      Collections.PackageTypes.find.returns({fetch: () => 'fakeCollection'});
      LocalState.get.returns(true);
      actions.fetchData({LocalState,Collections},{pageNumber,pageSize},callback);
      expect(callback.args[0]).to.deep.equal([ {data: 'fakeCollection',pageNumber} ]);
    });
  });

  describe('crudRemove',() => {
    it('should map localState with correct alert props',() => {
      const LocalState = {set: spy()};
      const Utils = {showModalAlert: spy()};
      const title = 'Delete Package';
      const text = 'Are you sure want to delete a package type?' +
          ' All users associated with the package will be removed!';
      actions.crudRemove({Utils,LocalState},'fake_data');
      expect(Utils.showModalAlert.args[0]).to
          .deep.equal([ {LocalState},{title,text,type: 'warning',data: 'fake_data'} ]);
    });
  });

  describe('remove',() => {
    it('should return error message and remove pop up on error ',() => {
      const LocalState = {get: spy(),set: spy()};
      const Models = {PackageType: {findOne: stub()}};
      const ShowAlerts = spy();
      const Utils = {resetAlert: spy()};
      const packageType = {_id: 'fake_id'};
      const pkgType = {removePackage: stub()};
      pkgType.removePackage.yields({message: 'oops!'},null);
      Models.PackageType.findOne.returns(pkgType);
      actions.remove({LocalState,Models,ShowAlerts,Utils},packageType);
      expect(ShowAlerts.args[0]).to.deep.equal([ 'oops!','error' ]);
      expect(Utils.resetAlert.args[0]).to.deep.equal([ {LocalState} ]);
    });

    it('should return success message and remove pop up on success ',() => {
      const LocalState = {get: spy(),set: spy()};
      const Models = {PackageType: {findOne: stub()}};
      const ShowAlerts = spy();
      const Utils = {resetAlert: spy()};
      const packageType = {_id: 'fake_id'};
      const pkgType = {removePackage: stub()};
      pkgType.removePackage.yields(null,'result');
        Models.PackageType.findOne.returns(pkgType);
      actions.remove({LocalState,Models,ShowAlerts,Utils},packageType);
      expect(ShowAlerts.args[0]).to.deep.equal([ 'Package was removed succesfully!','success' ]);
      expect(Utils.resetAlert.args[0]).to.deep.equal([ {LocalState} ]);
    });

  });

  describe('crudEdit',function () {
    it('should redirect to edit page',function () {
      const FlowRouter = {go: spy()};
      const context = {FlowRouter};
      actions.crudEdit(context,'edit_id');
      expect(FlowRouter.go.args[0]).to.deep.equal([ '@package_type.edit',{packageId: 'edit_id'} ]);
    });
  });


});
