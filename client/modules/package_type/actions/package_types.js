import {states,messages,routes} from '../lib/constants';


export default {
  update({Models,ShowAlerts,FlowRouter},packageType,callback) {
    const {PackageType} = Models;
    const pkgType = PackageType.findOne();
    pkgType.update(packageType,(err) => {
      if (err) {
        ShowAlerts(err.message, 'error');
      } else {
        ShowAlerts(messages.PACKAGE_CREATE_SUCCESS, 'success');
        FlowRouter.go(routes.PACKAGE_TYPE_LIST_NAME);
      }
      return callback && callback();
    });
  },

  create({Models,ShowAlerts,FlowRouter},packageType,callback) {
    const {PackageType} = Models;
    const pkgType = new PackageType();
    pkgType.create(packageType,(err) => {
      if (err) {
        ShowAlerts(err.message,'error');
      } else {
        ShowAlerts(messages.PACKAGE_CREATE_SUCCESS,'success');
        FlowRouter.go(routes.PACKAGE_TYPE_LIST_NAME);
      }
      return callback && callback();
    });
  },

  validate({Utils},errors) {
    return Utils.schemaToValidation(errors);
  },

  remove({Utils,ShowAlerts,Models,LocalState},packageType,callback) {
    const pkgType = Models.PackageType.findOne(packageType._id);
    pkgType.removePackage(packageType,(err) => {
      if (err) {
        ShowAlerts(err.message,'error');
        Utils.resetAlert({LocalState});
      } else {
        ShowAlerts(messages.PACKAGE_REMOVE_SUCCESS,'success');
        Utils.resetAlert({LocalState});
      }
      return callback && callback();
    });
  },

  crudEdit({FlowRouter},packageId) {
    return FlowRouter.go(routes.PACKAGE_TYPE_EDIT_NAME,{packageId});
  },

  crudRemove({Utils,LocalState},data) {
    return Utils.showModalAlert({LocalState},{
      data,
      title: messages.PACKAGE_REMOVE_TITLE,
      text: messages.PACKAGE_REMOVE_TEXT,
      type: 'warning'
    });
  },

  fetchData({Queries,Utils,Tracker,Collections},params,callback) {
    const myQuery = Queries.packageType();
    const {PackageTypes} = Collections;
    return Utils.fetchData(myQuery,params,callback,Tracker,PackageTypes);
  }
};
