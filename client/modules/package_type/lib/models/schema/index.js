
export default function ({Models}) {
  return Models.PackageType.extend({
    fields: {
      isBusy: {
        type: Boolean,
        default: false,
      }
    }
  });
}
