export default {
  PACKAGE_TYPE_PARAMS: '@packageTypes.params',
  PACKAGE_TYPE_SUBS_READY: '@packageTypes.subscription.ready',
  PACKAGE_TYPE_SELECTORS: '@packageTypes.selectors',
};
