export default {
  PACKAGE_TYPE_EDIT_NAME: '@package_type.edit',
  PACKAGE_TYPE_EDIT_PATH: '/package/:packageId',
  PACKAGE_TYPE_LIST_NAME: '@package_type.list',
  PACKAGE_TYPE_LIST_PATH: '/packages',
  PACKAGE_TYPE_CREATE_NAME: '@package_type.create',
  PACKAGE_TYPE_CREATE_PATH: '/package'
};
