export default {
  PACKAGE_CREATE_SUCCESS: 'Package was created successfully!',
  PACKAGE_UPDATE_SUCCESS: 'Package was updated successfully!',
  PACKAGE_REMOVE_SUCCESS: 'Package was removed succesfully!',
  PACKAGE_REMOVE_TEXT: 'Are you sure want to delete a package type?' +
  ' All users associated with the package will be removed!',
  PACKAGE_REMOVE_TITLE: 'Delete Package',
};
