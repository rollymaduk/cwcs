import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';


import PackageFields from '../components/package_fields.jsx';

export const composer = ({context}, onData) => {
  const {Models} = context();
  const {CATEGORIES: categories,STAGES: stages,LEVELS: levels} = Models.PackageTypePresets;
  onData(null, {categories,stages,levels});
};

export const depsMapper = (context, actions) => ({
  context: () => context
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(PackageFields);
