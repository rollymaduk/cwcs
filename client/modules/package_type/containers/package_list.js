import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {globalStates,PAGE_SIZE} from '/lib/constants';
import {routes} from '../lib/constants';

import PackageList from '../components/package_list.jsx';

export const composer = ({context,fetchData,startPage: pageNumber,pageSize}, onData) => {
  const {Models,LocalState} = context();
  const {getCount} = Models.Counters;
  fetchData({pageNumber,pageSize},({data}) => {
    onData(null,
      {
        data,
        recordCount: getCount(Models.PackageType),
        modal: LocalState.get(globalStates.APPLICATION_ALERTS),
      });
  });
};

export const depsMapper = (context, actions) => {
  const {fetchData,remove} = actions.packageTypes;
  const {resetAlert} = context.Utils;
  return {
    context: () => context,
    fetchData,
    fields: [ {id: 'name',title: 'Package'},'totalUsers','loanAmount' ],
    pageSize: PAGE_SIZE,
    startPage: 1,
    modalActions: {
      onConfirm: remove,
      onCancel: () => resetAlert(context),
      onEscapeKey: () => resetAlert(context),
      showCancelButton: true,
    },
    newPath: context.FlowRouter.path(routes.PACKAGE_TYPE_CREATE_NAME)
  };
};

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(PackageList);
