const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer} from '../package_form';

describe('package_type.containers.package_form', () => {
  describe('composer', () => {
    const getCollections = (post) => {
      const Models = {
        PackageType: {findOne: stub()}
      };
        Models.PackageType.findOne.returns(post);
      return Models;
    };

    it('with no packageId should return empty object',function () {
      const Collections = getCollections('dummy package');
      const onData = spy();
      const context = () => ({Collections});
      composer({context},onData);
      expect(onData.calledWith(null,{})).to.equal(true);
    });


    it('with packageId should return packageType ',function () {
      const Models = getCollections('dummy package');
      const onData = spy();
      const packageId = 'dummyId';
      const context = () => ({Models});
      composer({context,packageId},onData);
      expect(onData.calledWith(null,{packageType: 'dummy package'})).to.deep.equal(true);
    });

  });
});
