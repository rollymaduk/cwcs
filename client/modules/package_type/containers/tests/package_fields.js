const {describe, it} = global;
import {expect} from 'chai';
import { spy} from 'sinon';
import {composer} from '../package_fields';

describe('package_type.containers.package_fields', () => {
  describe('composer', () => {

    it('should load data with categories,levels and stages',function () {
      const CATEGORIES = [ {label: 'cat1',value: 1},{label: 'cat2',value: 2} ];
      const LEVELS = [ {label: 'level1',value: 1},{label: 'level2',value: 2} ];
      const STAGES = [ {label: 'stage1',value: 1},{label: 'stage2',value: 2} ];
      const Models = {PackageTypePresets:{CATEGORIES,LEVELS,STAGES}};
      const context = () => ({Models});
      const onData = spy();
      composer({context},onData);
      expect(onData.calledWith(null,{categories: CATEGORIES,levels: LEVELS,stages: STAGES}))
          .to.equal(true);
    });
  });
});

