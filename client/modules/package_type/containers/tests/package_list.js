const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer} from '../package_list';

describe('package_type.containers.package_list', () => {
  describe('composer', () => {
    // const Tracker = {nonreactive: cb => cb()};
    const getCollections = (posts) => {
      const Models = {
        PackageType: {find: stub()}
      };
      Models.PackageType.find.returns(posts);
      return Models;
    };

    it.skip('should return packageType list',function () {
      const Models = getCollections([ 'item1','item2' ]);
      const context = () => ({Models});
      const onData = spy();
      const fetchData = spy();
      composer({context,fetchData},onData);
      expect(onData.calledWith(null,{data: [ 'item1','item2' ]})).to.equal(true);
    });
  });
});
