import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
// import {publications} from '../../../../libs/constants/package_types';


import PackageForm from '../components/package_form.jsx';

export const composer = ({context,packageId,fetchData}, onData) => {
  if (packageId) {
    fetchData({selector: {_id: packageId},pageSize: 1,pageNumber: 0},({data}) => {
      if (data && data.length) {
        onData(null, {packageType: data[0]});
      }
    });
  } else {
    onData(null,{});
  }

};

export const depsMapper = (context, actions) => ({
  context: () => context,
  create: actions.packageTypes.create,
  update: actions.packageTypes.update,
  validate: actions.packageTypes.validate,
  fetchData: actions.packageTypes.fetchData
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(PackageForm);
