import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';

import CrudActions from '../../core/components/list_crud';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();

  onData(null, {});
};

export const depsMapper = (context, actions) => {
  const {crudEdit,crudRemove} = actions.packageTypes;
  return ({
    context: () => context,
    crudEdit,
    crudRemove,
  });
};

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(CrudActions);
