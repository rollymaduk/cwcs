import actions from './actions';
import routes from './routes.jsx';
import loadModelEvents from './lib/models/events';
import loadMethodStubs from './configs/methods';
import loadModelSchema from './lib/models/schema';

export default {
  routes,
  actions,
  load(context) {
    loadModelEvents(context);
    loadMethodStubs(context);
    loadModelSchema(context);
  }
};
