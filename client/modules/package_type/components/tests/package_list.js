const {describe, it,beforeEach,afterEach} = global;
import {expect} from 'chai';
import {shallow,mount} from 'enzyme';
import PackageList from '../package_list.jsx';

describe('package_type.components.package_list', function(){
  it('should display  data',function () {
    const data = [ {first: 'first-1',second: 'second-1'},{first: 'first-2',second: 'second-2'} ];
    const component = mount(<PackageList data={data}/>);
    expect(component.find('tr').length).to.equal(3);
  });
});

