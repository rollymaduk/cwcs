const {describe, it,beforeEach,afterEach} = global;
import React from 'react';
import {expect} from 'chai';
import {spy} from 'sinon';
import {shallow,mount} from 'enzyme';
import PackageForm from '../package_form';

describe('package_type.components.package_form', function () {
  beforeEach(function () {
    this.defaultValues = {minimumDownlines: 2};
    this.create = spy();
    this.validate = spy();
  });

  afterEach(function () {
    this.defaultValues = null;
    this.create.reset();
    this.validate.reset();
  });

  it('should display defaultValues',function () {
    const {defaultValues} = this;
    const wrapper = shallow(<PackageForm packageType={defaultValues}/>);
    expect(wrapper.props().children.props.defaultValues).to.deep.equal(defaultValues);
  });

  it.skip('should call handleSubmit on submit',function () {
    const {validate,create,defaultValues} = this;
    const wrapper = mount(<PackageForm validate={validate}
                                       create={create} packageType={defaultValues}/>);
    wrapper.find('button').get(0).click();
    expect(create.callCount).to.equal(1);
    expect(create.args[0][0]).to.deep.equal(defaultValues);
  });

  it.skip('should call handleValidate on submit',function () {
    const {validate,defaultValues} = this;
    const wrapper = mount(<PackageForm validate={validate} packageType={defaultValues}/>);
    wrapper.find('button').get(0).click();
    expect(validate.callCount).to.equal(2);
  });

});
