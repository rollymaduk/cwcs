import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import CustomListActions from '../custom_list_actions.jsx';

storiesOf('package_type.CustomListActions', module)
  .add('default view', () => {
    return (
      <CustomListActions />
    );
  })
