import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import PackageFields from '../package_fields.jsx';

storiesOf('package_type.PackageFields', module)
  .add('default view', () => {
    return (
      <PackageFields />
    );
  })
