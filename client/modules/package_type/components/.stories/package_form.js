import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import PackageForm from '../package_form.jsx';

storiesOf('package_type.PackageForm', module)
  .add('default view', () => {
    return (
      <PackageForm />
    );
  })
