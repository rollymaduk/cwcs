import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import PackageList from '../package_list.jsx';

storiesOf('package_type.PackageList', module)
  .add('default view', () => {
    return (
      <PackageList />
    );
  })
