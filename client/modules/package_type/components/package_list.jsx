import React from 'react';
import {CustomListViewer,IBox,ModalAlert,LinkButton} from '../../../../imports/components';
import ActionComponent from '../containers/crudActions';


const PackageList = ({data,fields,startPage,recordCount,pageSize,fetchData,modal,modalActions,newPath}) => {
  return (
      <IBox title="Packages" actionComponent={<LinkButton to={newPath}/>}>
          <ModalAlert modal={modal} modalActions={modalActions}/>
        <CustomListViewer
            startPage={startPage}
            fetchData={fetchData}
            pageSize={pageSize}
            recordCount={recordCount}
            fields={fields}
            data={data}
            actionComponent={ActionComponent}
        />
      </IBox>

    );
};

export default PackageList;
