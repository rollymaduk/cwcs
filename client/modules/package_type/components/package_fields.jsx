import React from 'react';
import PropTypes from 'prop-types';
import {FieldInput,AutoSuggestTag,CustomCheckbox} from '../../../../imports/components/index';
import {Select,Text} from 'react-form';

function packageFields(props) {
  const {categories = [],levels = [],stages = []} = props;
  return (
        <div>
            <FieldInput label="Category" field='category' options={categories} component={Select}/>
            <FieldInput label="Stage" field='stage' options={stages} component={Select}/>
            <FieldInput label="Level" field='level' options={levels} component={Select}/>
            <FieldInput label="Minimum Downlines"
                        field='minimumDownlines' component={Text} type="number" />


            <FieldInput field="loanAmount" label="Loan" component={Text} />
            <FieldInput field="reward" label="Rewards"
                         component={AutoSuggestTag} />
        </div>
    );
}

packageFields.propTypes = {
  categories: PropTypes.array,
  levels: PropTypes.array,
  stages: PropTypes.array,
  tags: PropTypes.array,
};
packageFields.defaultProps = {};

export default packageFields;
