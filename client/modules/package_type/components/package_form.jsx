import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {CustomForm} from '../../../../imports/components';
import Fields from '../containers/package_fields';
import {IBox} from '../../../../imports/components';
import {pick} from 'lodash/fp';
const Form = CustomForm(Fields);
const pickEvents = pick([ 'handleSubmit','handleValidate' ]);

class PackageForm extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleValidate = this.handleValidate.bind(this);
  }
  handleSubmit(values,state,props,done) {
    const {packageType} = this.props;
    const {create,update} = this.props;
    if (packageType && packageType._id) {
      update({...packageType || {},...values},done);
    } else {
      create({...packageType || {},...values});
    }
  }

  handleValidate(values) {
    //  todo improve on validation
    return {
      minimumDownlines: (!values.minimumDownlines) ? 'required' : null
    };
    // const {validate} = this.props;
    // console.log(validate(validationSchema.validate(values,{typecast: true})))
    // return validate(validationSchema.validate(values,{typecast: true}));
  }

  render() {
    const {packageType} = this.props;
    return (
        <IBox title="Packages">
          <Form
              {...pickEvents(this)}
              defaultValues = {packageType}
          />
        </IBox>
        );
  }
}

PackageForm.propTypes = {
  packageType: PropTypes.object
};
PackageForm.defaultProps = {};


export default PackageForm;
