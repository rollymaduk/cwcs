import React from 'react';
import {LoadingButton} from '../../../../imports/components';

export default ({value, griddleKey, rowData,edit,confirmRemove}) => {
    
  return (
        <div className="center-orientation">
      <span className="m-r-xs">
          <LoadingButton
              handleClick={() => edit(rowData._id)}
              className="btn-outline btn-primary btn btn-sm"
              label='edit'/>
      </span>
            <span className="m-l-xs">
        <LoadingButton
            handleClick={() => confirmRemove(rowData)}
            className="btn-outline btn-danger btn btn-sm"
            label='remove'/>
      </span>
        </div>
    );
};


