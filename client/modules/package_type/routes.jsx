import React from 'react';
import {mount} from 'react-mounter';
import {routes} from './lib/constants';
import MainLayout from '/client/modules/core/components/main_layout.jsx';
import Packageform from './containers/package_form.js';
import PackageList from './containers/package_list';
import {createAdminGroup} from '/client/lib/routeGroups';

export default function (injectDeps, context) {
  const MainLayoutCtx = injectDeps(MainLayout);

  const adminGroup = createAdminGroup(context);

  adminGroup.route(routes.PACKAGE_TYPE_CREATE_PATH, {
    name: routes.PACKAGE_TYPE_CREATE_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<Packageform />)
      });
    }
  });

  adminGroup.route(routes.PACKAGE_TYPE_LIST_PATH, {
    name: routes.PACKAGE_TYPE_LIST_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<PackageList />)
      });
    }
  });

  adminGroup.route(routes.PACKAGE_TYPE_EDIT_PATH, {
    name: routes.PACKAGE_TYPE_EDIT_NAME,
    action(params) {
      mount(MainLayoutCtx, {
        content: () => (<Packageform packageId={params.packageId}/>)
      });
    }
  });
}
