export default function (context) {
  const {Models} = context;
  Models.PackageType.extend({
    meteorMethods: {
      create() {
        console.log('package create on client');
      },
      update() {
        console.log('package update on client');
      },
      removePackage() {
        console.log('package remove on client');
      }
    }
  });
}
