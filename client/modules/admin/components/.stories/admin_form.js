import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import AdminForm from '../admin_form.jsx';

storiesOf('admin.AdminForm', module)
  .add('default view', () => {
    return (
      <AdminForm />
    );
  })
