import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import AdminList from '../admin_list.jsx';

storiesOf('admin.AdminList', module)
  .add('default view', () => {
    return (
      <AdminList />
    );
  })
