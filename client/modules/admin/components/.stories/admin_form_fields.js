import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import AdminFormFields from '../admin_form_fields.jsx';

storiesOf('admin.AdminFormFields', module)
  .add('default view', () => {
    return (
      <AdminFormFields />
    );
  })
