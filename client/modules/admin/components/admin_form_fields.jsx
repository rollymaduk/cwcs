import React from 'react';
import {FieldInput} from '../../../../imports/components/index';
import {Text,Select} from 'react-form';
import classnames from 'classnames';

const AdminForm = ({gender,isEditMode = false}) => {
  const className = classnames({'col-lg-6': !isEditMode},{'col-lg-12': isEditMode});
  return (
        <div>
            {(!isEditMode) ?
                <div className="col-lg-6">
                    <FieldInput field="email" component={Text} label="Email"/>
                    <FieldInput field="password" component={Text} type="password" label="Password"/>
                    <FieldInput field="confirmPassword"
                                component={Text} type="password" label="Repeat Password"/>
                </div> : null}
            <div className = {className} >
                < FieldInput field="profile.firstName" component={Text} label="Firstname"/>
                <FieldInput field="profile.lastName" component={Text} label="lastName"/>
                <FieldInput field="profile.phone" component={Text} label="Phone"/>
                <FieldInput field="profile.gender" options={gender}
                            component={Select} label="Gender"/>
            </div>

        </div>
    );
};

AdminForm.defaultProps = {
  gender: []
};

export default AdminForm;
