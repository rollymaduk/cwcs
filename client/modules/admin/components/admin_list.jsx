import React from 'react';
import {CustomListViewer,IBox,ModalAlert,LinkButton} from '../../../../imports/components';
import ActionComponent from '../containers/crudActions';

const AdminList = ({data,fields,startPage,recordCount,pageSize,modal,modalActions,newPath}) => (
    <IBox title="Administrators" actionComponent={<LinkButton to={newPath}/>}>
        <ModalAlert modal={modal} modalActions={modalActions}/>
        <CustomListViewer
            startPage={startPage}
            pageSize={pageSize}
            recordCount={recordCount}
            fields={fields}
            data={data}
            actionComponent={ActionComponent}
        />
    </IBox>
);

export default AdminList;
