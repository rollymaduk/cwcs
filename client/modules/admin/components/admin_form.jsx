import React from 'react';
import {CustomForm,IBox} from '../../../../imports/components';
import AdminFormFields from './admin_form_fields';
import validators from '../libs/validators';
import {schemaToValidation} from '../../../../imports/utils';

const CustomAdminForm = CustomForm(AdminFormFields);
const handleSubmit = (values,{create,admin,update}) => {
  (admin && admin._id) ? update({...admin,...values}) :
      create(values);

};
const handleValidate = (values) => {
  return schemaToValidation(validators(Boolean(values._id)),values);
};
const AdminForm = ({gender,create,admin,update}) => (
    <IBox title="Create Admin User">
        <CustomAdminForm
            defaultValues={admin}
            isEditMode={admin && admin._id}
            gender={gender}
            handleSubmit={(values) => handleSubmit(values,{create,admin,update})}
            handleValidate={(values) => handleValidate(values)}
        />
    </IBox>
);

export default AdminForm;
