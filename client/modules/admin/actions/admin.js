import {messages,routes} from '../libs/constants';
export default {


  remove({Models,LocalState,ShowAlerts,Utils},data,callback) {
    const admin = Models.AdminUser.findOne(data._id);
    admin.removeAdmin(data._id,(err,res) => {
      if (res) {
        ShowAlerts(messages.ADMIN_REMOVE_SUCCESS,'success');
        Utils.resetAlert({LocalState});
      } else {
        ShowAlerts(err.message,'error');
        Utils.resetAlert({LocalState});
      }
      return callback && callback();
    });
  },

  crudEdit({FlowRouter},adminId) {
    FlowRouter.go(routes.ADMIN_EDIT_NAME,{adminId});
  },

  update({Models,ShowAlerts,FlowRouter},user,callback) {
    const {AdminUser} = Models;
    const admin = AdminUser.findOne(user._id);
    admin.update(user,(err,res) => {
      if (res) {
        ShowAlerts(messages.ADMIN_UPDATE_SUCCESS,'success');
        FlowRouter.go(routes.ADMIN_LIST_NAME);
      } else {
        ShowAlerts(`${messages.ADMIN_UPDATE_FAILURE}: ${err.message}`,'error');
      }
      return callback && callback();
    });
  },

  create({Models,ShowAlerts,FlowRouter},user,callback) {
    const {AdminUser} = Models;
    const admin = new AdminUser();
    admin.create(user,(err,res) => {
      if (res) {
        ShowAlerts(messages.ADMIN_CREATE_SUCCESS,'success');
        FlowRouter.go(routes.ADMIN_LIST_NAME);
      } else {
        ShowAlerts(`${messages.ADMIN_CREATE_FAILURE}: ${err.message}`,'error');
      }
      return callback && callback();
    });
  },

  crudRemove({Utils,LocalState},data) {
    Utils.showModalAlert({LocalState},{
      data,
      title: messages.ADMIN_REMOVE_TITLE,
      text: messages.ADMIN_REMOVE_TEXT,
      type: 'warning'
    });
  },

  fetchData({Queries,Utils,Tracker},params,callback) {
    const {admin} = Queries;
    return Utils.fetchData(admin(),params,callback,Tracker);
  }
};
