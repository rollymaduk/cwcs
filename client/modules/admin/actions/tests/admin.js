const {describe, it, beforeEach, afterEach } = global;
import {expect} from 'chai';
import {spy,createStubInstance} from 'sinon';
import actions from '../admin';

class UserAdminClass {
  create() {

  }
}

describe('user.actions.admin', () => {
  const fakeAdmin = 'fake_admin';
  describe('create',function () {
    beforeEach(function () {
      this.adminStub = createStubInstance(UserAdminClass);
      this.ShowAlerts = spy();
      this.FlowRouter = {go: spy()};
      const AdminUser = spy(() => this.adminStub);
      this.Models = {AdminUser};
    });

    afterEach(function () {
      this.adminStub.create.reset();
      this.ShowAlerts.reset();
      this.FlowRouter.go.reset();
    });

    it('should create admin',function () {
      const {Models,adminStub} = this;
      actions.create({Models},fakeAdmin);
      expect(adminStub.create.args[0][0]).to.equal(fakeAdmin);
    });

    it('should display error message on error',function () {
      const {Models,adminStub,ShowAlerts} = this;
      adminStub.create.yields({message: 'oops!'},null);
      actions.create({Models,ShowAlerts},fakeAdmin);
      expect(ShowAlerts.args[0]).to.deep.equal([ 'Admin create failure: oops!','error' ]);
    });

    it('should display success message on success',function () {
      const {Models,adminStub,ShowAlerts,FlowRouter} = this;
      adminStub.create.yields(null,'result');
      actions.create({Models,ShowAlerts,FlowRouter},fakeAdmin);
      expect(ShowAlerts.args[0]).to.deep.equal([ 'Successfully created admin','success' ]);
    });

    it('should go to admin list page on success',function () {
      const {Models,adminStub,ShowAlerts,FlowRouter} = this;
      adminStub.create.yields(null,'result');
      actions.create({Models,ShowAlerts,FlowRouter},fakeAdmin);
      expect(FlowRouter.go.args[0]).to.deep.equal([ '@admin.list' ]);
    });

  });
  describe('fetchData',function () {
    it('should fetch admin data',function () {
      const Utils = {fetchData: spy()};
      actions.fetchData({Utils},{});
      expect(Utils.fetchData.args[0]).to.deep.equal([
          {Utils},
          {name: 'UserAdmin',
              params: '@admin.query.params',
              pubs: '@admin.list',
              callback: undefined
          },
          {}
      ]);
    });
  });
});
