import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {globalStates,PAGE_SIZE} from '/lib/constants';
import AdminList from '../components/admin_list.jsx';
import {routes} from '../libs/constants';

export const composer = ({context,fetchData,startPage: pageNumber,pageSize}, onData) => {
  const {Models,LocalState} = context();
  const {getCount} = Models.Counters;
  fetchData({pageNumber,pageSize},({data}) => {
    onData(null, {
      data,
      recordCount: getCount(Models.User,'@admin'),
      modal: LocalState.get(globalStates.APPLICATION_ALERTS),
    });
  });
};

export const depsMapper = (context, actions) => {
  const {Utils,FlowRouter} = context;
  return ({
    context: () => context,
    fetchData: actions.admin.fetchData,
    fields: [
        {id: 'fullName',title: 'Name'},
          {id: 'emails.0.address',title: 'Email'},
          {id: 'profile.gender',title: 'Gender'}
    ],
    pageSize: PAGE_SIZE,
    newPath: FlowRouter.path(routes.ADMIN_FORM_NAME),
    startPage: 1,
    modalActions: {
      onConfirm: actions.admin.remove,
      onCancel: () => Utils.resetAlert(context),
      onEscapeKey: () => Utils.resetAlert(context),
      showCancelButton: true
    }
  });
};

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(AdminList);
