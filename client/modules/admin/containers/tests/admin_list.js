const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer,depsMapper} from '../admin_list';

describe('admin.containers.admin_list', () => {
  describe('composer', () => {

//    const Tracker = {nonreactive: cb => cb()};
//    const getCollections = (post) => {
//      const Collections = {
//        Posts: {findOne: stub()}
//      };
//      Collections.Posts.findOne.returns(post);
//      return Collections;
//    };

    it.skip('should load  data',function () {
      const fetchData = stub();
      fetchData.yields({data: 'fake_data',pageNumber: 1});
      const context = spy();
      const pageSize = 1;
      const startPage = 1;
      const onData = spy();
      composer({context,fetchData,startPage,pageSize},onData);
      expect(onData.args[0]).to.deep.equals([ null,{data: 'fake_data'} ]);
    });
  });
  describe('depsMapper',function () {

    it('should map to fetchData action',function () {
      const actions = {admin: {fetchData: spy()}};
      const result = depsMapper({},actions);
      expect(result.fetchData).to.deep.equal(actions.admin.fetchData);
    });

    it('should map to modal actions',function () {
      const actions = {admin: {remove: spy()}};
      const Utils = {resetAlert: spy()};
      const result = depsMapper({Utils},actions);
      expect(result.modalActions.onConfirm).to.deep.equal(actions.admin.remove);
      result.modalActions.onCancel();
      expect(Utils.resetAlert.calledOnce);
      result.modalActions.onEscapeKey();
      expect(Utils.resetAlert.calledTwice);
    });
  });

});
