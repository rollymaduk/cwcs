import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {getGenderPresets} from '../../../../imports/utils';

import AdminForm from '../components/admin_form.jsx';

export const composer = ({context,adminId,fetchData}, onData) => {
  if (adminId) {
    fetchData({selector: {_id: adminId},pageSize: 1,pageNumber: 0},({data}) => {
      if (data && data.length) {
        onData(null, {admin: data[0]});
      }
    });
  } else {
    onData(null,{});
  }
};

export const depsMapper = (context, actions) => {
  const {fetchData,update,create} = actions.admin;
  return ({
    context: () => context,
    create,
    fetchData,
    gender: getGenderPresets(),
    update,
  });

};

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(AdminForm);
