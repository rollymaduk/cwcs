const insertValidators = {
  email: {presence: true,email: true},
  password: {presence: true,length: {minimum: 6}},
  confirmPassword: {equality: 'password'},
};
const editValidators = {
  'profile.firstName': {presence: true},
  'profile.lastName': {presence: true},
  'profile.gender': {presence: true}
};
export default function (isEditMode) {
  if (isEditMode) {
    return editValidators;

  }
  return {...insertValidators, ...editValidators};

}
