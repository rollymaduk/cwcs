export default {
  ADMIN_CREATE_FAILURE: 'Admin create failure',
  ADMIN_CREATE_SUCCESS: 'Successfully created admin',
  ADMIN_UPDATE_FAILURE: 'Admin update failure',
  ADMIN_UPDATE_SUCCESS: 'Successfully updated admin',
  ADMIN_REMOVE_SUCCESS: 'Successfully removed admin',
  ADMIN_REMOVE_TEXT: 'Are you sure want to delete an administrator?',
  ADMIN_REMOVE_TITLE: 'Delete Admin',
};
