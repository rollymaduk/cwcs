export default {
  ADMIN_LIST_PATH: '/admins',
  ADMIN_LIST_NAME: '@admin.list',
  ADMIN_FORM_PATH: '/admin',
  ADMIN_FORM_NAME: '@admin.form',
  ADMIN_EDIT_NAME: '@admin.edit',
  ADMIN_EDIT_PATH: '/admin/:adminId',
};
