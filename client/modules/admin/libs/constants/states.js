export default {
  ADMIN_QUERY_PARAMS: '@admin.query.params',
  ADMIN_SUBS_READY: '@admin.subscription.ready',
  ADMIN_QUERY_SELECTORS: '@admin.query.selectors',
};
