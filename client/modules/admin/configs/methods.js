export default function (context) {
  const {Models} = context;
  Models.AdminUser.extend({
    meteorMethods: {
      create() {
        console.log('admin create on client');
      },
      update() {
        console.log('admin update on client');
      },
      removeAdmin() {
        console.log('admin remove on client');
      }
    }
  });
}
