import React from 'react';
import {mount} from 'react-mounter';
import {routes} from './libs/constants';
import {pick} from 'lodash';
import AdminForm from './containers/admin_form';
import AdminList from './containers/admin_list';
import {createAdminGroup} from '/client/lib/routeGroups';

import MainLayout from '/client/modules/core/components/main_layout.jsx';

export default function (injectDeps, context) {
  const MainLayoutCtx = injectDeps(MainLayout);

  const adminGroup = createAdminGroup(context);

  adminGroup.route(routes.ADMIN_FORM_PATH,{
    name: routes.ADMIN_FORM_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<AdminForm/>)
      });
    }
  });

  adminGroup.route(routes.ADMIN_LIST_PATH,{
    name: routes.ADMIN_LIST_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<AdminList/>)
      });
    }
  });

  adminGroup.route(routes.ADMIN_EDIT_PATH, {
    name: routes.ADMIN_EDIT_NAME,
    action(params) {
      mount(MainLayoutCtx, {
        content: () => (<AdminForm adminId={params.adminId}/>)
      });
    }
  });
}
