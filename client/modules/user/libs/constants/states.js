export default {
  USER_QUERY_PARAMS: '@user.query.params',
  USER_SUBS_READY: '@user.subscription.ready',
  USER_QUERY_SELECTORS: '@user.query.selectors',
  REFRESH_USERLIST_TOKEN: '@refreshList',
  FORGOT_PASS_SUCCESS: '@forgot.pass.success'
};
