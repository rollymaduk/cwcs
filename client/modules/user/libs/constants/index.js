import routes from './routes';
import BANKS from './presets/banks.js';
import STATES from './presets/states';
import {getGenderPresets} from '../../../../../imports/utils'
import messages from './messages';
import states from './states';
const GENDER = getGenderPresets();

export {
    routes,
    messages,
    states,
STATES,BANKS,GENDER
};

export const steps = {
  STEP_1: 'account',
  STEP_2: 'profile',
  STEP_3: 'billing'
};
