export default {
  USER_CREATE_FAILURE: 'Registration failure',
  USER_UPDATE_FAILURE: 'User update failure',
  USER_UPDATE_SUCCESS: 'User successfully updated!',
  RESET_PASSWORD_SUCCESS: 'Password successfully reset!'
};
