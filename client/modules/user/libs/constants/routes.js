export default {
  USER_CREATE_PATH: '/register',
  USER_CREATE_NAME: '@user.registration',
  USER_LIST_PATH: '/users',
  USER_LIST_NAME: '@user.list',
  USER_CREATE_SUCCESS_PATH: '/register-success',
  USER_CREATE_SUCCESS_NAME: '@user.registration.success',
  DOWNLINE_LIST_PATH: '/downlines/:userId',
  DOWNLINE_LIST_NAME: '@downlines.list',
  LOGIN_NAME: '@login',
  LOGIN_PATH: '/login',
  FORGOT_PASS_NAME: '@forgot.password',
  FORGOT_PASS_PATH: '/forgot-password',
  VERIFY_EMAIL_NAME: '@verify.email',
  VERIFY_EMAIL_PATH: '/verify-email/:token',
  RESET_PASSWORD_NAME: '@reset.password',
  RESET_PASSWORD_PATH: '/reset-password/:token',
};

