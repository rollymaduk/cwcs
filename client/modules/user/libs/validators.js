export default {
  profile: {
    'profile.firstName': {presence: true},
    'profile.lastName': {presence: true},
    'profile.DOB': {presence: true},
    'profile.gender': {presence: true}
  },
  account: {
    email: {presence: true,email: true},
    packageTypeId: {presence: true},
    'account.referral': {presence: true,email: true},
    'account.bankDetails.bankName': {presence: true},
    'account.bankDetails.accountNumber': {presence: true},
    password: {presence: true,length: {minimum: 6}},
    confirmPassword: {equality: 'password'},
  },
  billing: {
    'billing.bankName': {presence: true},
    'billing.tellerNumber': {presence: true},
  },
  forgotPassword: {
    email: {presence: true,email: true},
  },

  resetPassword: {
    password: {presence: true,length: {minimum: 6}},
    confirmPassword: {equality: 'password'},
  },

  login: {
    email: {presence: true,email: true},
  }
};

