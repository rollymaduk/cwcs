import users from './users';
import admin from '../../admin/actions/admin';

export default {
  users,
  admin
};
