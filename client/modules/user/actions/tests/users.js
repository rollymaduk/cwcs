const {describe, it,beforeEach,afterEach} = global;
import {expect} from 'chai';
import {spy,createStubInstance, stub} from 'sinon';
import actions from '../users';

class UserClass {
  create() {

  }
}

describe('user.actions.users', () => {

  describe('create',function () {
    const userStub = createStubInstance(UserClass);
    const User = spy(() => userStub);
    const Models = {User};
    it('should redirect to registration-success page on successfull registration',
          function () {
            const FlowRouter = {go: spy()};
            userStub.create.yields(null,'result');
            actions.create({FlowRouter,Models},'fakeUser');
            expect(FlowRouter.go.args[0][0]).to.equal('@user.registration.success');
          });

    it('should display error on failed registration',
          function () {
            const ShowAlerts = spy();
            userStub.create.yields({message: 'oops!'},null);
            actions.create({ShowAlerts,Models},'fakeUser');
            expect(ShowAlerts.args[0]).to
                .deep.equal([ 'Registration failure: oops!','error' ]);
          });

    it('should call create new user',() => {
      const FlowRouter = {go: spy()};
      userStub.create.yields(null,'result');
      actions.create({FlowRouter,Models},'fakeUser');
      expect(userStub.create.args[0][0]).to
            .deep.equal('fakeUser');
    });
  });

  describe('update',function () {
    beforeEach(function () {
      this.Models = {User: {findOne: stub()}};
      this.ShowAlerts = spy();
      this.usrStub = {update: stub()};
      this.Models.User.findOne.returns(this.usrStub);
      this.fakeUser = {_id: 'fake_id',isActive: true};
    });
    afterEach(function () {
      this.Models.User.findOne.reset();
      this.ShowAlerts.reset();
      this.usrStub.update.reset();

    });
    it('should find existing user from collection',function () {
      const {Models,usrStub,fakeUser} = this;
      actions.update({Models,usrStub},fakeUser);
      expect(Models.User.findOne.args[0][0]).to.equal('fake_id');
    });

    it('should update an existing user',function () {
      const {Models,usrStub,fakeUser} = this;
      actions.update({Models},fakeUser);
      expect(usrStub.update.args[0][0]).to.deep.equal(fakeUser);
    });

    it('should display error message on error',function () {
      const {Models,usrStub,ShowAlerts,fakeUser} = this;
      usrStub.update.yields({message: 'oops!'},null);
      actions.update({Models,usrStub,ShowAlerts},fakeUser);
      expect(ShowAlerts.args[0]).to.deep.equal([ 'User update failure: oops!','error' ]);
    });

    it('should display success message on success',function () {
      const {Models,usrStub,ShowAlerts,fakeUser} = this;
      usrStub.update.yields(null,'result');
      actions.update({Models,usrStub,ShowAlerts},fakeUser);
      expect(ShowAlerts.args[0]).to.deep.equal([ 'User successfully updated!','success' ]);
    });

  });
});
