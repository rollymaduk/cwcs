import {routes,messages,states} from '../libs/constants';
import {globalRoutes} from '/lib/constants';
import UUID from 'cuid';
const USER_ID_PARAM = 'userId';
const TOKEN_PARAM = 'token';

export default {
  create({FlowRouter,Models,ShowAlerts},user,callback) {
    const {User} = Models;
    const usr = new User();
    usr.create(user,(err,result) => {
      if (result) {
        FlowRouter.go(routes.USER_CREATE_SUCCESS_NAME);
      } else {
        ShowAlerts(messages.USER_CREATE_FAILURE + ': ' + err.message,'error');
      }
      return callback && callback();
    });
  },

  update({Models,ShowAlerts,LocalState},user,callback) {
    const usr = new Models.User();
    usr.update(user,(err,result) => {
      if (result) {
        ShowAlerts(messages.USER_UPDATE_SUCCESS,'success');
        LocalState.set(states.REFRESH_USERLIST_TOKEN,UUID());
      } else {
        ShowAlerts(messages.USER_UPDATE_FAILURE + ': ' + err.message,'error');
      }
      return callback && callback();
    });
  },

  fetchData({Queries,Utils,Tracker},params,callback) {
    const {user} = Queries;
    return Utils.fetchDataStatic(user(),params,callback,Tracker);
  },

  forgotPassword({Models,ShowAlerts,LocalState},email,callback) {
    const userModel = new Models.User();
    return userModel.forgotPassword(email,(err) => {
      if (err) {
        ShowAlerts('Email does not exist!','error');
      } else {
        LocalState.set(states.FORGOT_PASS_SUCCESS,true);
      }
      return callback && callback();
    });
  },

  login({Meteor,ShowAlerts,FlowRouter},email,password) {
    return Meteor.loginWithPassword(email,password,(err) => {
      if (err) {
        ShowAlerts(err.message,'error');
      } else {
        FlowRouter.go(globalRoutes.ROUTE_HOME_NAME);
      }
    });
  },

  logout({Meteor,ShowAlerts,FlowRouter}) {
    return Meteor.logout((err) => {
      if (err) {
        ShowAlerts(err.message,'error');
      } else {
        FlowRouter.reload();
      }
    });
  },

  fetchDownlines({Utils,FlowRouter},params,callback) {
    const userId = FlowRouter.getParam(USER_ID_PARAM);
    if (userId) {
      return Utils.getUserDownlines({...params,selector: {userId}},callback);
    }
  },

  resetPassword({Accounts,ShowAlerts,FlowRouter},newPassword,callback) {
    const token = FlowRouter.getParam(TOKEN_PARAM);
    if (token) {
      Accounts.resetPassword(token,newPassword,(err) => {
        if (err) {
          return ShowAlerts(err.message,'error');
        }
        ShowAlerts(messages.RESET_PASSWORD_SUCCESS,'success');
        FlowRouter.go(routes.LOGIN_NAME);
        return callback && callback();
      });
    }
  }

};
