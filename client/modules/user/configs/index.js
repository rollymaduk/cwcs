import initMethods from './methods';
import initAccounts from './accounts';
export default function(context){
    initMethods(context);
    initAccounts(context);
}