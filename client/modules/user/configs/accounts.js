import {Accounts} from 'meteor/accounts-base';
import {states} from '../libs/constants/index';


export default function ({LocalState}) {
  Accounts.onEmailVerificationLink((token,done) => {
    Accounts.verifyEmail(token,(err) => {
      if (err) {
        console.log(err);
      }
      done();
    });
  });

  Accounts.onResetPasswordLink((token,done) => {
    LocalState.set(states.RESET_PASSWORD_TOKEN,token);
    done();
  });
}
