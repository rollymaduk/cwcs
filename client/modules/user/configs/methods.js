export default function (context) {
  const {Models} = context;
  Models.User.extend({
    meteorMethods: {
      create() {
        console.log('user create on client');
      },
      update() {
        console.log('user update on client');
      },
      removeUser() {
        console.log('user remove on client');
      },
      forgotPassword() {
        console.log('user forgotPassword on client');
      },
      sendResetPasswordEmail() {
        console.log('user sendResetPasswordEmail on client');
      },
      getDownlineCount() {
        console.log('user getDownlineCount on client');
      },
      getDownlines() {
        console.log('user getDownlines on client');
      }
    }
  });
}
