import React from 'react';
import {mount} from 'react-mounter';
import {routes,states} from './libs/constants';

import PublicLayout from '../core/components/public_layout';
import MainLayout from '../core/components/main_layout';
import LoginLayout from '../core/components/login_layout';

import Registration from './containers/registration';
import RegistrationSuccess from './components/register_success';
import UserList from './containers/user_list/user_list';
import DownLineList from './containers/user_list/downline_list';

import Login from './containers/login';
import ForgotPassword from './containers/forgot_password';
import ResetPassword from './containers/reset_password';



import {createAccountGroup, createAdminGroup,
    createAppGroup, createPublicGroup} from '/client/lib/routeGroups';


export default function (injectDeps, context) {
  const MainLayoutCtx = injectDeps(MainLayout);
  const PublicLayoutCtx = injectDeps(PublicLayout);
  const LoginLayoutCtx = injectDeps(LoginLayout);
  const {LocalState} = context;

  const appGroup = createAppGroup(context);
  const accountGroup = createAccountGroup(context);
  const adminGroup = createAdminGroup(context);
  const publicGroup = createPublicGroup(context);



  publicGroup.route(routes.USER_CREATE_PATH, {
    name: routes.USER_CREATE_NAME,
    action() {
      mount(PublicLayoutCtx, {
        content: () => (<Registration/>)
      });
    }
  });

  publicGroup.route(routes.USER_CREATE_SUCCESS_PATH, {
    name: routes.USER_CREATE_SUCCESS_NAME,
    action() {
      mount(PublicLayoutCtx, {
        content: () => (<RegistrationSuccess/>)
      });
    }
  });

  adminGroup.route(routes.USER_LIST_PATH,{
    name: routes.USER_LIST_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<UserList/>)
      });
    }
  });

  appGroup.route(routes.DOWNLINE_LIST_PATH,{
    name: routes.DOWNLINE_LIST_NAME,
    action(params) {
      mount(MainLayoutCtx, {
        content: () => (<DownLineList userId={params.userId}/>)
      });
    }
  });

  accountGroup.route(routes.LOGIN_PATH, {
    name: routes.LOGIN_NAME,
    action() {
      mount(LoginLayoutCtx, {
        content: () => (<Login />)
      });
    }
  });

  accountGroup.route(routes.FORGOT_PASS_PATH, {
    name: routes.FORGOT_PASS_NAME,
    action() {
      LocalState.set(states.FORGOT_PASS_SUCCESS,false);
      mount(LoginLayoutCtx, {
        content: () => (<ForgotPassword />)
      });
    }
  });

  accountGroup.route(routes.RESET_PASSWORD_PATH, {
    name: routes.RESET_PASSWORD_NAME,
    action() {
      mount(LoginLayoutCtx, {
        content: () => (<ResetPassword />)
      });
    }
  });
}

