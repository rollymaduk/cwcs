import React from 'react';
import {FieldInput} from '../../../../imports/components';
import {Select,Text} from 'react-form';

const AccountInfoFields = (props) => {
  const {banks = [],packageTypes = []} = props;
  return (
        <div>
            <FieldInput field="packageTypeId"
                        options={packageTypes}
                        component={Select}
                        label="Package"/>
            <FieldInput field="email" component={Text} label="Email"/>
            <FieldInput field="password" component={Text} type="password" label="Password"/>
            <FieldInput field="confirmPassword"
                        component={Text} type="password" label="Repeat Password"/>
            <FieldInput field="account.referral" component={Text} label="Referral Email"/>
            <div className="hr-line-dashed"/>

            <h2>Bank Details</h2>
            <FieldInput
                field="account.bankDetails.bankName"
                options={banks}
                label='Bank'
                component={Select}/>
            <FieldInput field="account.bankDetails.accountNumber" type="number"
                        label='Account Number' component={Text}/>
            <div className="hr-line-dashed"/>

        </div>
    );
};

export default AccountInfoFields;
