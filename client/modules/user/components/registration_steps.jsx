import React from 'react';
import {Step,Steps} from 'react-albus';
import {CustomForm,IBox} from '../../../../imports/components';
import AccountFields from './account_info_fields';
import BillingFields from './billing_fields';
import ProfileFields from './profile_fields';
const ProfileForm = CustomForm(ProfileFields);
const BillingForm = CustomForm(BillingFields);
const AccountForm = CustomForm(AccountFields);
import {pick} from 'lodash/fp';
const pickEvents = pick([ 'handleSubmit','handleValidate' ]);
import NavigationButtons from './wizard_buttons';
import {steps} from '../libs/constants';


const RegistrationWizard = (props) => {
  const {handlePrevious,presets} = props;
  return (
            <Steps>
                <Step path={steps.STEP_1}>
                    <IBox title="Account Information">
                        <AccountForm {...presets} buttonLabel="Next" {...pickEvents(props)}/>
                    </IBox>
                </Step>
                <Step path={steps.STEP_2}>
                    <IBox title="Personal Data">
                        <ProfileForm
                            {...presets}
                            showButton={false}
                            {...pickEvents(props)}
                            customActionButtons={NavigationButtons}
                            actionProps={{handlePrevious}}
                        />
                    </IBox>
                </Step>
                <Step path={steps.STEP_3}>
                    <IBox title="Billing">
                        <BillingForm {...presets} buttonLabel="Register" {...pickEvents(props)}/>
                    </IBox>
                </Step>
            </Steps>
    );
};

export default RegistrationWizard;
