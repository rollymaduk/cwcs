import React from 'react';
import PropTypes from 'prop-types';
import {get} from 'lodash/fp';
const getStatus = get('packageType.name');
import classnames from 'classnames';

const UserField = ({value, griddleKey, rowData,pathToProfile}) => {
  const classNames = classnames('fa','m-r-sm',{'fa-gift': getStatus(rowData)});
  return (
        <div>
            <strong><a href={pathToProfile(rowData._id)}>{value}</a></strong>
            <br/>
            <i className={classNames}/><small>{getStatus(rowData)}</small>
        </div>
    );
};

UserField.propTypes = {};
UserField.defaultProps = {};

export default UserField;
