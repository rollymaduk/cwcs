import React from 'react';
import PropTypes from 'prop-types';
import {get} from 'lodash/fp';
const getPhone = get('profile.phone');
import classnames from 'classnames';

const ContactField = ({value, griddleKey, rowData}) => {
    const classNames = classnames('fa','m-r-sm',{'fa-phone': getPhone(rowData)});
    return (
        <div>
            {value}
            <br/>
            <i className={classNames}/><small>{getPhone(rowData)}</small>
        </div>
    );
};

ContactField.propTypes = {};
ContactField.defaultProps = {};

export default ContactField;
