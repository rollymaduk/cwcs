import React from 'react';
import PropTypes from 'prop-types';
import {get} from 'lodash/fp';
const getTeller = get('billing.tellerNumber')

const BillingField = ({griddleKey, value, rowData}) => {
    return (
        <div>
            {value}
            <br/>
            <small>{getTeller(rowData)}</small>
        </div>
    );
};

BillingField.propTypes = {};
BillingField.defaultProps = {};

export default BillingField;
