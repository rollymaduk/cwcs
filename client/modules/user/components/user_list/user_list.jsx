import React from 'react';
import {CustomListViewer,IBox,LinkButton} from '../../../../../imports/components/index';
import ActionComponent from '../../containers/user_list/action_field';

const UserList = ({data,fields,startPage,pageSize,newPath,fetchData}) => {
  return (
        <IBox title="Users" actionComponent={<LinkButton to={newPath}/>}>
            <CustomListViewer
                startPage={startPage}
                pageSize={pageSize}
                fields={fields}
                fetchData={fetchData}
                data={data}
                actionComponent={ActionComponent}
            />
        </IBox>

    );
};

export default UserList;
