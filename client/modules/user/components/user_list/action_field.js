import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {LoadingButton} from '../../../../../imports/components';

const ActionField = ({rowData, value,update}) => {
  const classNames = classnames(
        'btn-outline','btn','btn-sm'
        ,{'btn-primary': rowData.isActive}
        ,{'btn-danger': !rowData.isActive});
  return (
        <div className="center-orientation">
        <LoadingButton
            handleClick={() => update({_id: rowData._id,isActive: !rowData.isActive})}
            className={classNames}
            label={(rowData.isActive) ? 'deactivate' : 'activate'}/>
        </div>
    );
};

ActionField.propTypes = {};
ActionField.defaultProps = {};

export default ActionField;
