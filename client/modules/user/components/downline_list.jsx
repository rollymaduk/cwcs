import React from 'react';
import {CustomListViewer,IBox} from '/imports/components';

const UserList = ({data,fields,startPage,pageSize,fetchData}) => {
    return (
        <IBox title="Downlines">
            <CustomListViewer
                startPage={startPage}
                pageSize={pageSize}
                fields={fields}
                fetchData={fetchData}
                data={data}
            />
        </IBox>

    );
};

export default UserList;
