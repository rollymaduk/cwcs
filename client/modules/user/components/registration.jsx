import React,{Component} from 'react';
import {pick} from 'lodash/fp';
import {Wizard} from 'react-albus';
const pickEvents = pick([ 'handleSubmit','handleValidate' ]);
import RegistrationSteps from './registration_steps';
import validators from '../libs/validators';
import {schemaToValidation} from '/imports/utils';
import {steps} from '../libs/constants';


class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValues: {}
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleValidate = this.handleValidate.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
  }

  handleSubmit(values,wizard,done) {
    const {formValues} = this.state;
    const {createUser} = this.props;
    const {path} = wizard.step;
    const updatedValues = {...formValues,...values};
    if (path === steps.STEP_3) {
      createUser(updatedValues,done);
    } else {
      this.setState({formValues: updatedValues});
      wizard.next();
    }
  }

  handlePrevious(wizard) {
    wizard.previous();
  }

  handleValidate(values,wizard) {
    const {path} = wizard.step;
    const constraint = validators[path];
    return schemaToValidation(constraint,values);
  }

  render() {
    const {presets} = this.props;
    return (
        <Wizard render={(wizard) => (
            <RegistrationSteps
                presets={presets}
                handleSubmit={(values,state,props,done) => this.handleSubmit(values,wizard,done)}
                handlePrevious ={() => this.handlePrevious(wizard)}
                handleValidate={(values) => this.handleValidate(values,wizard)}
            />
        )
        }/>);
  }
}

Registration.propTypes = {};
Registration.defaultProps = {};

export default Registration;


