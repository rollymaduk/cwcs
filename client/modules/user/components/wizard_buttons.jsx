import React from 'react';
import PropTypes from 'prop-types';

const WizardButtons = ({handlePrevious,nextLabel='Next',prevLabel='Previous'}) => (
  <div>
    <a onClick={handlePrevious} className="btn btn-primary btn-md m-r-sm">{prevLabel}</a>
    <button type="submit" className="btn btn-primary btn-md m-l-sm">{nextLabel}</button>
  </div>
);

WizardButtons.propTypes = {
  handlePrevious: PropTypes.func.isRequired
};

export default WizardButtons;
