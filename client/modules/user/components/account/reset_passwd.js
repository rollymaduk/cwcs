import React, {Component} from 'react';
import {CustomForm} from '/imports/components/index';
import ResetPasswordFields from './reset_passwd_fields';
const ResetPasswordForm = CustomForm(ResetPasswordFields);
import validators from '../../libs/validators';
import {schemaToValidation} from '/imports/utils';
import PropTypes from 'prop-types';
const VALIDATE_PATH = 'resetPassword';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.handleValidate = this.handleValidate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    const {resetPassword} = this.props;
    resetPassword(values.password);
  }

  handleValidate(values) {
    const constraint = validators[VALIDATE_PATH];
    return schemaToValidation(constraint,values);
  }

  render() {
    return (
            <div>
                <p>
                    Enter your new password below
                </p>
                <ResetPasswordForm
                    handleValidate={this.handleValidate}
                    handleSubmit = {this.handleSubmit}
                    buttonLabel="Change Password"
                    buttonClass="block full-width m-b btn-primary"
                />
            </div>
        );
  }
}

ResetPassword.propTypes = {};
ResetPassword.defaultProps = {};

export default ResetPassword;



