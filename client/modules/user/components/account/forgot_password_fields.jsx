import React from 'react';
import {FieldInput} from '/imports/components/index';
import {Text} from 'react-form';

const ForgotPasswordFields = () => (
    <div>
        <FieldInput field="email" placeholder="registered email" component={Text} />
    </div>
);

export default ForgotPasswordFields;
