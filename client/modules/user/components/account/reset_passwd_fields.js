import React from 'react';
import {FieldInput} from '/imports/components/index';
import {Text} from 'react-form';

const ResetPasswordFields = () => (
    <div>
        <FieldInput field="password" type="password" placeholder="password" component={Text} />
        <FieldInput field="confirmPassword" type="password"
                    placeholder="confirm password" component={Text} />
    </div>
);

export default ResetPasswordFields;
