import React,{Component} from 'react';
import {CustomForm,LinkButton} from '/imports/components/index';
import ForgotPasswordFields from './forgot_password_fields';
const ForgotPasswordForm = CustomForm(ForgotPasswordFields);
import validators from '../../libs/validators';
import {schemaToValidation} from '/imports/utils';
const VALIDATION_PATH = 'forgotPassword';

import PropTypes from 'prop-types';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleValidate = this.handleValidate.bind(this);
  }

  handleSubmit(values) {
    const {forgotPassword} = this.props;
    console.log(values)
    forgotPassword(values.email);
  }

  handleValidate(values) {
    const constraint = validators[VALIDATION_PATH];
    return schemaToValidation(constraint,values);
  }

  render() {
    const {showSuccess} = this.props;
    return (
            <div>
                {
                    (showSuccess) ?
                    <p>
                        An email has been sent, with a link to reset your password
                    </p> :
                        <div>
                        <p>
                            Enter your email address below and let us help you recover your lost
                            password
                        </p>
                        <ForgotPasswordForm
                            handleSubmit={this.handleSubmit}
                            handleValidate={this.handleValidate}
                    buttonLabel="Recover"
                    buttonClass="block full-width m-b btn-primary"
                    />
                        </div>
                }


                <LinkButton to='./login' color="white" size="sm"
                            className="btn-block" label="Back to Login"/>
            </div>
        );
  }
}

export default ForgotPassword;
