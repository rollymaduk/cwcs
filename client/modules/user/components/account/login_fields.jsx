import React from 'react';
import {FieldInput} from '/imports/components/index';
import {Text} from 'react-form';

const LoginFields = () => (
  <div>
      <FieldInput field="email" placeholder="email" component={Text} />
      <FieldInput field="password" type="password" placeholder="password" component={Text} />
  </div>
);

export default LoginFields;
