import React, {Component} from 'react';
import {CustomForm,LinkButton} from '/imports/components/index';
import LoginFields from './login_fields';
const LoginForm = CustomForm(LoginFields);
const RECOVER_PASSWD_PATH = './forgot-password';
const REGISTER_PATH = '../public/register';
import {schemaToValidation} from '/imports/utils';
import validators from '../../libs/validators';
const SELECTOR = 'login';

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleValidate = this.handleValidate.bind(this);
  }

  handleSubmit(values) {
    const {login} = this.props;
    const {email,password} = values;
    login(email,password);
  }

  handleValidate(values) {
    const constraints = validators[SELECTOR];
    return schemaToValidation(constraints,values);
  }

  render() {
    const {isBusy} = this.props;
    return (
            <div>
                <h3>Welcome to CWCS Portal</h3>
                <p>
                    Manage your downlines and earnings effortlessly on our portal
                </p>
                <p>
                    Login in. To see it in action.
                </p>
                <LoginForm
                    controlState={false}
                    isBusy={isBusy}
                    handleSubmit={this.handleSubmit}
                    handleValidate={this.handleValidate}
                    buttonLabel="Login"
                    buttonClass="block full-width m-b btn-primary"
                />
                <a href={RECOVER_PASSWD_PATH}><small>Forgot password?</small></a>
                <p className="text-muted text-center"><small>Do not have an account?</small></p>
                <LinkButton to={REGISTER_PATH} color="white" size="sm" className="btn-block" label="Create an account"/>
            </div>
        );
  }
}

Login.propTypes = {};
Login.defaultProps = {};

export default Login;
