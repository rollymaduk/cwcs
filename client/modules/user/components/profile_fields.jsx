import React from 'react';
import {FieldInput,CustomDatePicker,CustomRadioGroup} from '../../../../imports/components';
import {Text,Select} from 'react-form';

const ProfileFields = (props) => {
  const {states = [],gender = [],country = [ {label: 'Nigeria',value: 'Nigeria' } ]} = props;
  return (

        <div>
            <div className="col-lg-6">
                <FieldInput field="profile.firstName" label="First name" component={Text}/>
                <FieldInput field="profile.lastName" label="Last name" component={Text}/>
                <div className="hr-line-dashed"/>
                <FieldInput field='profile.DOB' label="D.O.B" component={CustomDatePicker}/>
                <FieldInput field='profile.gender'
                            name="gender"
                            options={gender}
                            label="Gender"
                            component={CustomRadioGroup}/>
                <FieldInput field="profile.phone" label="Phone" component={Text}/>
            </div>
            <div className="col-lg-6">
                <FieldInput field="profile.address.line1" label="Address Line1" component={Text}/>
                <FieldInput field="profile.address.line2" label="Address Line2" component={Text}/>
                <div className="hr-line-dashed"/>
                <FieldInput field="profile.address.city" label="City" component={Text}/>
                <FieldInput field="profile.address.state" options={states}
                            label="State" component={Select}/>
                <FieldInput field="profile.address.country" options={country}
                            label="Country" component={Select}/>
            </div>

        </div>
    );
};

export default ProfileFields;
