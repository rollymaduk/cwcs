import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import RegisterSuccess from '../register_success.jsx';

storiesOf('user.RegisterSuccess', module)
  .add('default view', () => {
    return (
      <RegisterSuccess />
    );
  })
