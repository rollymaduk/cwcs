import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import RegistrationWizard from '../registration_steps.jsx';

storiesOf('user.RegistrationWizard', module)
  .add('default view', () => {
    return (
      <RegistrationWizard />
    );
  })
