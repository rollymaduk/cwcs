import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import BillingFields from '../billing_fields.jsx';

storiesOf('user.BillingFields', module)
  .add('default view', () => {
    return (
      <BillingFields />
    );
  })
