import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import WizardButtons from '../wizard_buttons.jsx';

storiesOf('user.WizardButtons', module)
  .add('default view', () => {
    return (
      <WizardButtons />
    );
  })
