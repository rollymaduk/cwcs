import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import Registration from '../registration.jsx';

storiesOf('user.Registration', module)
  .add('default view', () => {
    return (
      <Registration />
    );
  })
