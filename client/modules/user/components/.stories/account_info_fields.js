import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import AccountInfoFields from '../account_info_fields.jsx';

storiesOf('user.AccountInfoFields', module)
  .add('default view', () => {
    return (
      <AccountInfoFields />
    );
  })
