import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import UserList from '../user_list/user_list.jsx';

storiesOf('user.UserList', module)
  .add('default view', () => {
    return (
      <UserList />
    );
  })
