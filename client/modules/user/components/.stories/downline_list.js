import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import DownlineList from '../downline_list.jsx';

storiesOf('user.DownlineList', module)
  .add('default view', () => {
    return (
      <DownlineList />
    );
  })
