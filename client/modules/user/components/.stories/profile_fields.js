import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import ProfileFields from '../profile_fields.jsx';

storiesOf('user.ProfileFields', module)
  .add('default view', () => {
    return (
      <ProfileFields />
    );
  })
