import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import AdminForm from '../../../admin/components/admin_form_fields.jsx';

storiesOf('user.AdminForm', module)
  .add('default view', () => {
    return (
      <AdminForm />
    );
  })
