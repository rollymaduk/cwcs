import React from 'react';
import {FieldInput} from '../../../../imports/components';
import {Select,Text} from 'react-form';

const BillingFields = (props) => {
  const {banks = []} = props;
  return (
        <div>
            <FieldInput field='billing.bankName' options={banks} label="Bank" component={Select}/>
            <FieldInput field='billing.tellerNumber' label="Teller Nos." component={Text}/>
        </div>
    );
};

export default BillingFields;
