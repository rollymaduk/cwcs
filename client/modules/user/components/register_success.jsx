import React from 'react';
const PATH_HOME = '../../'
const PATH_LOGIN = '../accounts/login'

const RegisterSuccess = () => (
    <div className="col-lg-12 center-block">
        <div className="jumbotron">
            <h1>Thank You!</h1>
            <p> For registering on the CWCS platform,
                we are pretty excited to have you join us.
                A welcome email has been sent to your inbox, where you will be required to verify
                your registration.
            </p>
            <p>
                <a role="button" href={PATH_HOME}
                   className="btn btn-primary btn-lg m-r-sm">Return Home</a>
                <a role="button" href={PATH_LOGIN}
                   className="btn btn-primary btn-lg m-l-sm">Login</a>
            </p>
        </div>
    </div>
);

export default RegisterSuccess;
