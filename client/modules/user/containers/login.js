import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';

import Login from '../components/account/login.jsx';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();

  onData(null, {isBusy: Meteor.loggingIn()});
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  login: actions.users.login
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(Login);
