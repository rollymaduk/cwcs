import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';

import ResetPassword from '../components/account/reset_passwd';

export const composer = ({context}, onData) => {
    const {Meteor, Collections} = context();

    onData(null, {});
};

export const depsMapper = (context, actions) => ({
    context: () => context,
    resetPassword : actions.users.resetPassword
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(ResetPassword);
