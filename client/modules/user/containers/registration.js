import {useDeps, composeAll, composeWithTracker} from 'mantra-core';
import {BANKS,STATES,GENDER} from '../libs/constants';
import Registration from '../components/registration.jsx';
const FIRST_STAGE = 'STAGE1'; const FIRST_LEVEL = 1;

export const composer = ({context,fetchData}, onData) => {
  const {Models} = context();
  const packageTypes = Models.PackageType.find({});
  if (!packageTypes.length) {
    fetchData({selector: {level: FIRST_LEVEL,stage: FIRST_STAGE},
    options: {fields: {category: 1,name: 1,level: 1,stage: 1}},pageNumber: 1});
  }
  onData(null, {
    presets: {
      states: STATES.map((state) => ({label: state,value: state})),
      banks: BANKS.map((bank) => ({label: bank,value: bank})),
      gender: GENDER,
      packageTypes: packageTypes
            .map((packageType) => ({label: packageType.category,value: packageType._id}))
    }
  });
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  fetchData: actions.packageTypes.fetchData,
  createUser: actions.users.create,
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(Registration);
