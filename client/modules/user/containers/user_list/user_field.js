/* eslint no-unused-vars:0*/
import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {routes} from '/client/modules/core/constants'
import UserField from '../../components/user_list/user_field';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  onData(null, {});
};

export const depsMapper = (context) => {
  const {FlowRouter} = context;
  return {
    context: () => context,
    pathToProfile: (userId) => FlowRouter.path(routes.DASHBOARD_NAME,{userId})
  };
};

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(UserField);
