import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {UserList,ContactField,BillingField} from '../../components/user_list/index';
import UserField from './user_field';
import {states,routes} from '../../libs/constants';
import {PAGE_SIZE} from '/lib/constants';


export const composer = ({context,fetchData,startPage: pageNumber,pageSize}, onData) => {
  const {LocalState} = context();
  LocalState.get(states.REFRESH_USERLIST_TOKEN);
  fetchData({pageNumber,pageSize},({data}) => {
    onData(null, {data});
  });
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  fetchData: actions.users.fetchData,
  fields: [
      {id: 'fullName', title: 'User', customComponent: UserField},
      {id: 'emails.0.address', title: 'Contact',customComponent: ContactField},
      {id: 'billing.bankName', title: 'Billing',customComponent: BillingField},
      {id: 'downlines', title: 'Dowlines'},
      {id: 'earnings', title: 'Earnings'},
  ],
  pageSize: PAGE_SIZE,
  startPage: 1,
  newPath: context.FlowRouter.path(routes.USER_CREATE_NAME)
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(UserList);
