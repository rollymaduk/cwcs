import {useDeps, composeAll, composeWithTracker} from 'mantra-core';
import {ContactField,BillingField} from '../../components/user_list';
import UserField from './user_field';
import DownLineList from '../../components/downline_list';
import {PAGE_SIZE} from '/lib/constants';


export const composer = ({context,fetchData,startPage: pageNumber,pageSize}, onData) => {
  fetchData({pageNumber,pageSize},({data}) => {
    onData(null, {data});
  });
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  fetchData: actions.users.fetchDownlines,
  fields: [
        {id: 'fullName', title: 'User', customComponent: UserField},
        {id: 'emails.0.address', title: 'Contact',customComponent: ContactField},
  ],
  pageSize: PAGE_SIZE,
  startPage: 1,
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(DownLineList);
