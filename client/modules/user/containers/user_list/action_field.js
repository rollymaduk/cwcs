import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';

import ActionField from '../../components/user_list/action_field';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();

  onData(null, {});
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  update: actions.users.update
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(ActionField);
