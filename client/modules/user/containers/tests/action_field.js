const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer,depsMapper} from '../user_list/action_field';

describe('user.containers.action_field', () => {
  describe('depsMapper',function(){
    it('should map to actions',function(){
      const actions = {users:{update:spy()}}
      const result =depsMapper({},actions);
      expect(result.update).to.deep.equal(actions.users.update)
    })
  })
  describe('composer', () => {

//    const Tracker = {nonreactive: cb => cb()};
//    const getCollections = (post) => {
//      const Collections = {
//        Posts: {findOne: stub()}
//      };
//      Collections.Posts.findOne.returns(post);
//      return Collections;
//    };

    it('should do something');
  });
});
