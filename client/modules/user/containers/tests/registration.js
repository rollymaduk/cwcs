const {describe, it} = global;
import {expect} from 'chai';
import {stub, spy} from 'sinon';
import {composer,depsMapper} from '../registration';

describe('user.containers.registration', () => {
  describe('composer', () => {

//    const Tracker = {nonreactive: cb => cb()};
//    const getCollections = (post) => {
//      const Collections = {
//        Posts: {findOne: stub()}
//      };
//      Collections.Posts.findOne.returns(post);
//      return Collections;
//    };
    it('should map actions correctly',function () {
      const actions = {packageTypes: {create: spy()},users: {create: spy()}};
      const context = spy();
      const result = depsMapper(context,actions);
      expect(result.createUser).to.equal(actions.users.create);
    });

    it.skip('should load presets',function () {
      const onData = spy();
      expect(composer({},onData));
    });
  });
});
