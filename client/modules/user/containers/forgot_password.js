import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {states} from '../libs/constants';

import ForgotPassword from '../components/account/forgot_password.jsx';

export const composer = ({context}, onData) => {
  const {Meteor, Collections,LocalState} = context();

  onData(null, {
    showSuccess: LocalState.get(states.FORGOT_PASS_SUCCESS)
  });
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  forgotPassword: actions.users.forgotPassword

});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(ForgotPassword);
