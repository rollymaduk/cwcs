import actions from './actions';
import routes from './routes.jsx';
import initDefaults from './configs';
export default {
  routes,
  actions,
  load(context) {
    initDefaults(context);
  }
};
