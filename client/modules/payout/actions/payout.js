import {messages} from '../libs/constants';
const USER_ID_PARAM = 'userId';
export default {
  fetchData({Queries,Utils,Tracker,FlowRouter},params,callback) {
    const {payout} = Queries;
    const userId = FlowRouter.getParam(USER_ID_PARAM);
    const allParams = (userId) ? {...params,selector: {userId}} : params;
    return Utils.fetchData(payout(),allParams,callback,Tracker);
  },
  process({Models,ShowAlerts},data) {
    const {Payout} = Models;
    const payout = Payout.findOne(data._id);
    if (payout) {
      payout.update(data,(err,res) => {
        if (res) {
          ShowAlerts(messages.PAYOUT_UPDATE_SUCCESS,'success');
        } else {
          ShowAlerts(`${messages.PAYOUT_UPDATE_FAILURE} ${err && err.message}`,'error');
        }
      });
    }
  }
};
