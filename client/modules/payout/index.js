import actions from './actions';
import routes from './routes.jsx';
import loadMethodStubs from './configs/methods';

export default {
  routes,
  actions,
  load(context) {
    loadMethodStubs(context);
  }
};
