import React from 'react';
import classnames from 'classnames';
import {statusClasses} from './util';

function PayoutStatusField({rowData}) {
  const {status} = rowData;
  const classNames = classnames('label',`label-${statusClasses[status]}`);
  return (
        <div>
            <small className={classNames}>
                {status}
            </small>
        </div>
    );
}

PayoutStatusField.propTypes = {};
PayoutStatusField.defaultProps = {};

export default PayoutStatusField;
