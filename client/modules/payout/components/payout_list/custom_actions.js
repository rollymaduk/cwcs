import React from 'react';
import {DropDownButton} from '/imports/components';
import {statusClasses} from './util'
import {get} from 'lodash/fp';
const getStatus = get('status');
const STATUS_NEW = 'new';

export default ({rowData,menus,process}) => {
  const status = getStatus(rowData) || STATUS_NEW;
  const className = `btn-${statusClasses[status]}`;
  const filteredMenus = menus.filter((menu) => menu.value !== status);
  return (
        <div className="center-orientation">
          <span className="m-r-xs">
              <DropDownButton
                  handleClick={(value) => process({_id: rowData._id,status: value})}
                  menus={filteredMenus}
                  status ={status}
                  className={className}/>
          </span>
        </div>
    );
};


