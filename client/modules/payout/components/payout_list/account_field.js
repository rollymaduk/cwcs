import React from 'react';
import {get} from 'lodash/fp';
const getAccountNumber = get('account.bankDetails.accountNumber');
const getBank = get('account.bankDetails.bankName');

const AccountField = ({rowData}) => {
  return (
        <div>
            {getBank(rowData.user)}
            <br/>
            <small>{getAccountNumber(rowData.user)}</small>
        </div>
    );
};

AccountField.propTypes = {};
AccountField.defaultProps = {};

export default AccountField;
