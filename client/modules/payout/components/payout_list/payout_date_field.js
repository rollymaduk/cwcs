import React from 'react';
import moment from 'moment';


function PayoutDateField({rowData}) {
  const {updatedAt} = rowData;
  console.log(rowData)
  const period = moment(updatedAt).format('MMM Do YY');
  return (<small>{period}</small>);
}

PayoutDateField.propTypes = {};
PayoutDateField.defaultProps = {};

export default PayoutDateField;
