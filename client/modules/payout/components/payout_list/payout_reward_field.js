import React from 'react';

function PayoutRewardField({rowData}) {
  const {details = []} = rowData;
  const rewards = details.map(detail => detail.item);
  return (
        <small>
            {rewards.join(',')}
        </small>
    );
}

PayoutRewardField.propTypes = {};
PayoutRewardField.defaultProps = {};

export default PayoutRewardField;
