import React from 'react';
import {getFullName} from '/imports/utils';

function UserField({value,rowData}) {
  return (
        <div>
            {getFullName(rowData.user)}
            <br/>
          <small>{rowData.packageType}</small>
        </div>
    );
}

UserField.propTypes = {};
UserField.defaultProps = {};

export default UserField;
