import React from 'react';
import {CustomListViewer,IBox} from '/imports/components/index';
import ActionComponent from '../../containers/custom_actions';


const PayoutList = ({data,fields,startPage,recordCount,pageSize,fetchData}) => {
  return (
        <IBox title="Payments">
            <CustomListViewer
                fetchData={fetchData}
                startPage={startPage}
                pageSize={pageSize}
                recordCount={recordCount}
                fields={fields}
                data={data}
                actionComponent={ActionComponent}
            />
        </IBox>

    );
};

export default PayoutList;
