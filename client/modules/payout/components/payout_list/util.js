export default function (rewards) {
  const fullfilled = rewards.filter(reward => reward.isFulfilled).length;
}


export const statusClasses = {
  processing: 'info',
  new: 'warning',
  complete: 'primary'
};

