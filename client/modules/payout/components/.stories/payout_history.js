import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import PayoutHistory from '../payout_history.jsx';

storiesOf('payout.PayoutHistory', module)
  .add('default view', () => {
    return (
      <PayoutHistory />
    );
  })
