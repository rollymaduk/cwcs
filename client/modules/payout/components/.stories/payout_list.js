import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import PayoutList from '../payout_list/payout_list.jsx';

storiesOf('payout.PayoutList', module)
  .add('default view', () => {
    return (
      <PayoutList />
    );
  })
