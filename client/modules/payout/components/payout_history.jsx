import React from 'react';
import {CustomListViewer,IBox} from '/imports/components/index';


const PayoutHistory = ({data,fields,startPage,recordCount,pageSize,fetchData}) => {
  return (
        <IBox title="Payment History">
            <CustomListViewer
                startPage={startPage}
                pageSize={pageSize}
                recordCount={recordCount}
                fields={fields}
                fetchData={fetchData}
                data={data}
            />
        </IBox>

    );
};

export default PayoutHistory;
