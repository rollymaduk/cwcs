import React from 'react';
import {mount} from 'react-mounter';
import {routes} from './libs/constants';
import {pick} from 'lodash';
import PayoutList from './containers/payout_list';
import PaymentHistory from './containers/payout_history';
import {createAdminGroup,createAppGroup} from '/client/lib/routeGroups';

import MainLayout from '/client/modules/core/components/main_layout.jsx';

export default function (injectDeps, context) {
  const MainLayoutCtx = injectDeps(MainLayout);

  const appGroup = createAppGroup(context);
  const adminGroup = createAdminGroup(context);

  adminGroup.route(routes.PAYOUT_LIST_PATH, {
    name: routes.PAYOUT_LIST_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<PayoutList />)
      });
    }
  });

  appGroup.route(routes.PAYOUT_HISTORY_PATH, {
    name: routes.PAYOUT_HISTORY_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<PaymentHistory />)
      });
    }
  });
}
