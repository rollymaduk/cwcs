import {useDeps, composeAll, composeWithTracker} from 'mantra-core';
import {UserField,StatusField,DateField,RewardField} from '../components/payout_list';
import {PAGE_SIZE} from '/lib/constants';
import PaymentHistory from '../components/payout_history';

export const composer = ({context,fetchData,startPage: pageNumber,pageSize}, onData) => {
  const {Models} = context();
  const {getCount} = Models.Counters;
  fetchData({pageNumber,pageSize},({data}) => {
    onData(null,
      {
        data,
        recordCount: getCount(Models.Payout)
      });
  });
};


export const depsMapper = (context, actions) => ({
  context: () => context,
  fetchData: actions.payout.fetchData,
  fields: [ {id: 'user',title: 'Name',customComponent: UserField },
        {id: 'loanAmount',title: 'Earnings'},
        {id: 'details',title: 'Rewards'},
        {id: 'updatedAt',title: 'Last Update',customComponent: DateField},
        {id: 'status',title: 'Status',customComponent: StatusField}
    ],
  pageSize: PAGE_SIZE,
  startPage: 1,
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(PaymentHistory);
