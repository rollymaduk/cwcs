import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {Status} from '/lib/models/payout';

import CustomActions from '../components/payout_list/custom_actions';

const prepareDropDownBtnPresets = () => {
  return Status.getIdentifiers()
      .map((item) => ({label: item,value: item}));
};

export const composer = ({context,status}, onData) => {
  const {Meteor, Collections} = context();
  const menus = prepareDropDownBtnPresets();
  onData(null, {menus});
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  process: actions.payout.process,

});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(CustomActions);
