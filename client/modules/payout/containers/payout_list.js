import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {PAGE_SIZE} from '/lib/constants';
import {
    PayoutList,
    DateField,
    RewardField,
    StatusField,
    UserField,
    AccountField,

} from '../components/payout_list';

export const composer = ({context,fetchData,startPage: pageNumber,pageSize}, onData) => {
  const {Models} = context();
  const {getCount} = Models.Counters;
  fetchData({pageNumber,pageSize},({data}) => {
    onData(null,
      {
        data,
        recordCount: getCount(Models.Payout)
      });
  });
};


export const depsMapper = (context, actions) => ({
  context: () => context,
  fetchData: actions.payout.fetchData,
  fields: [ {id: 'user',title: 'Name',customComponent: UserField },
      {id: 'account',title: 'Account',customComponent: AccountField },
      {id: 'loanAmount',title: 'Earnings'},
      {id: 'details',title: 'Rewards', customComponent: RewardField},
      {id: 'updatedAt',title: 'Last Update', customComponent: DateField},
      ],
  pageSize: PAGE_SIZE,
  startPage: 1,
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(PayoutList);
