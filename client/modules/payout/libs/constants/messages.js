export default {
  PAYOUT_UPDATE_FAILURE: 'Payout update failure',
  PAYOUT_UPDATE_SUCCESS: 'Payout successfully updated!'
};
