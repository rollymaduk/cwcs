export default {
  PAYOUT_LIST_NAME: '@payout_list.list',
  PAYOUT_LIST_PATH: '/payouts',
  PAYOUT_HISTORY_NAME: '@payout_list.history',
  PAYOUT_HISTORY_PATH: '/payouts/:userId',
};
