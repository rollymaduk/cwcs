import React from 'react';
import {mount} from 'react-mounter';
import {globalRoutes} from '/lib/constants';
import {routes} from './constants';
import MainLayout from './components/main_layout.jsx';
import Dashboard from './containers/dashboard';
import AdminLanding from './containers/admin_landing.js';
import NotFound from './components/not_found';
import {createAdminGroup,createAppGroup} from '/client/lib/routeGroups';


export default function (injectDeps, context) {
  const MainLayoutCtx = injectDeps(MainLayout);
  const {Meteor,Roles,FlowRouter} = context;


  const appGroup = createAppGroup(context);

  const adminGroup = createAdminGroup(context);


  appGroup.route(globalRoutes.ROUTE_HOME_PATH,{
    name: globalRoutes.ROUTE_HOME_NAME,
    action() {
      if (!Roles.userIsInRole(Meteor.userId(),[ '@admin' ])) {
        mount(MainLayoutCtx,{
          content: () => (<Dashboard userId={Meteor.userId()}/>)
        });
      } else {
        mount(MainLayoutCtx,{
          content: () => (<AdminLanding />)
        });
      }
    }
  });

  appGroup.route(routes.DASHBOARD_PATH, {
    name: routes.DASHBOARD_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<Dashboard />)
      });
    }
  });

  FlowRouter.notFound = {
    action() {
      mount(NotFound,{dashboardUrl: './dashboard'});
    }
  };


  adminGroup.route(routes.ADMIN_DASHBOARD_PATH, {
    name: routes.ADMIN_DASHBOARD_NAME,
    action() {
      mount(MainLayoutCtx, {
        content: () => (<AdminLanding />)
      });
    }
  });
}
