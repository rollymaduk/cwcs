import React from 'react';
import classNames from 'classnames';

const MenuItem = ({to,icon,name,className,hasArrow}) => {
  const iconClass = classNames('fa',`fa-${icon}`);
  const classes = classNames('nav-label',className);
  return (
        <li>
            <a href={to}>
                <i className={iconClass}/>
                <span className={classes}>{name}</span>
                {hasArrow ? <span className="fa arrow"/> : null}
            </a>
        </li>
    );
};

MenuItem.defaultProps = {
  hasArrow: true
};

export default MenuItem;
