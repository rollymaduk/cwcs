import React from 'react';
import ProfileWidget from './dashboard_profile';
import StatusWidget from './dashboard_status';
import ChartWidget from './dashboard_chart';
import TimelineWidget from './dashboard_timeline';

const Dashboard = ({profileData,statusData,chartData,timelineData}) => (
    <div>
        <div className="row m-b-lg m-t-lg">
            <div className="col-lg-4">
                <ProfileWidget {...profileData}/>
            </div>
            <div className="col-lg-2">
                <StatusWidget {...statusData}/>
            </div>
            <div className="col-lg-6">
                <ChartWidget {...chartData}/>
            </div>
        </div>
        <div className="row">
            <TimelineWidget {...timelineData} />
        </div>
    </div>


);

Dashboard.defaultProps = {
  statusData: {category: 'RUBY',stage: 'STAGE1',level: '1'},
  profileData: {fullName: 'Ronald Maduka',status: 'RUBY-STAGE1-1',
      email: 'rolly.maduk@gmail.com',phone: '+2348032016618'},
  chartData: {earnings: 30,downlines: 40},
  timelineData: {timeLines: [
      {title: 'Ruby-stage1-3',
          description: 'You moved up to Ruby-stage1-3  and earned!',createdAt: '2017-03-05'},
      {title: 'Ruby-stage1-2',
          description: 'You moved up to Ruby-stage1-2 and earned!',createdAt: '2017-04-05'},
  ]
  }
};


export default Dashboard;
