import React from 'react';

const Header = ({logout,name,toggleNav}) => {
  return (

        <div className="row border-bottom">
            <nav className="navbar navbar-static-top" role="navigation" style={{marginBottom: 0}}>
                <div className="navbar-header">
                    <a onClick={toggleNav}
                       className="navbar-minimalize minimalize-styl-2 btn btn-primary"
                       href="#"
                    >
                        <i className="fa fa-bars"/>
                    </a>
                    {/* <form role="search" className="navbar-form-custom" >
                     <div className="form-group">
                     <input
                     type="text"
                     placeholder="Search for something..."
                     className="form-control"
                     name="top-search"
                     id="top-search"
                     />
                     </div>
                     </form>*/}
                </div>
                <ul className="nav navbar-top-links navbar-right">
                    <li>
                    <span className="m-r-sm text-muted welcome-message">
                        Hello {name}! Welcome to CWCS portal
                    </span>
                    </li>
                    {/* <li>
                     <a className="right-sidebar-toggle">
                     <i className="fa fa-cog"/>
                     </a>
                     </li>*/}
                    <li>
                        <a onClick={() => logout()}>
                            <i className="fa fa-sign-out"/> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
    );
};

export default Header;
