import React from 'react';
import PropTypes from 'prop-types';

const PlainLayout = ({content = () => null }) => {
    return (
        <div className="middle-box text-center animated fadeInDown">
            {content()}
        </div>
    );
};

PlainLayout.propTypes = {};
PlainLayout.defaultProps = {};

export default PlainLayout;
