import React from 'react';
import PropTypes from 'prop-types';
import Footer from './footer';
import Notifications from 'react-notify-toast';
import RegisterHeader from './public_header';
;
function RegisterLayout({content = () => null }) {
  return (
        <div id="wrapper">
            <Notifications/>
            <div id="page-wrapper" className="gray-bg">
                <RegisterHeader/>
                <div className="wrapper wrapper-content">
                    <div className="container">
                        {content()}
                    </div>
                </div>
                <Footer/>
            </div>
        </div>
    );
}

RegisterLayout.propTypes = {};
RegisterLayout.defaultProps = {};

export default RegisterLayout;
