import React from 'react';
import Header from '../containers/header';
import Footer from './footer';
import Sidebar from '../containers/sidebar';
import Notifications from 'react-notify-toast';
import Container from './container';
/* todo remove this painfull hack in component*/
import {toggleBodyClass} from '/imports/utils';
const RESP_CLASS = 'body-small';
const RESP_WIDTH = 768;
/* ===================================*/

const Layout = ({content = () => null }) => (
  <div>
      <Notifications/>
      <div className="pace  pace-inactive">
          <div
              className="pace-progress"
              data-progress-text="100%"
              data-progress="99"
              style={{transform: 'translate3d(100%, 0px, 0px)'}}
          >
              <div className="pace-progress-inner"/>
          </div>
          <div className="pace-activity"/>
      </div>
      <div id="wrapper">
          <Sidebar/>
          <div id="page-wrapper" className="gray-bg" >
              <Header/>
              <div className="wrapper wrapper-content animated fadeInRight">
                    <Container
                        min={{
                          width: RESP_WIDTH,
                          action: toggleBodyClass(false,RESP_CLASS)
                        }}
                        max={{
                          width: RESP_WIDTH,
                          action: toggleBodyClass(true,RESP_CLASS)
                        }}
                    >
                      {content()}
                    </Container>
              </div>
              <Footer/>
          </div>

      </div>

  </div>
);

export default Layout;
