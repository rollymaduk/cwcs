import React from 'react';

const Footer = () => (
    <div className="footer">
        <div>
            <strong>Copyright</strong> Synetica Plus © 2014-2017
        </div>
    </div>
);

export default Footer;
