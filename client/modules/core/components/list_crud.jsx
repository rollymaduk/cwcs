import React from 'react';
import {LoadingButton} from '../../../../imports/components';

export default ({rowData,crudEdit,crudRemove}) => {

  return (
        <div className="center-orientation">
      <span className="m-r-xs">
          <LoadingButton
              handleClick={() => crudEdit(rowData._id)}
              className="btn-outline btn-primary btn btn-sm"
              label='edit'/>
      </span>
            <span className="m-l-xs">
        <LoadingButton
            handleClick={() => crudRemove(rowData)}
            className="btn-outline btn-danger btn btn-sm"
            label='remove'/>
      </span>
        </div>
    );
};


