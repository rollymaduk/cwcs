import React, {Component} from 'react';
import Responsive from 'react-responsive-decorator'
import PropTypes from 'prop-types';

const setupHandler = (obj,prop,media)=>{
    const mediaPropObj = {}
    if(obj){
        const {width,action,args=[]}=obj
        mediaPropObj[prop]=width
        media(mediaPropObj,()=>{
            if(action){
                action(...args)
            }
        });
    }
};

@Responsive
class Container extends Component {
    componentDidMount() {
        const {min,max,media} = this.props;
        setupHandler(min,'minWidth',media);
        setupHandler(max,'maxWidth',media);
    }

    render() {
        const {children} = this.props
        return (
            <div>{children}</div>
        );
    }
}

Container.propTypes = {};
Container.defaultProps = {};

export default Container;
