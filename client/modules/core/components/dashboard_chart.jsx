import React from 'react';
import {CustomKnob} from '../../../../imports/components';
const MIN_VAL = 0; const MAX_VAL = 100; const SIZE = 150; const COLOR = '#1AB394';
import PropTypes from 'prop-types';


const DashboardChart = ({earnings,downlines}) => (
    <div>
        <div className="col-sm-6 text-center">
            <CustomKnob
                onChange={() => {}}
                min={MIN_VAL}
                max={MAX_VAL}
                isDisable
                value={earnings}
                fgColor={COLOR}
                width={SIZE}
                height={SIZE}
                disableTextInput
            />

            <div><strong>Earnings</strong></div>
        </div>
        <div className="col-sm-6 text-center">
            <CustomKnob
                onChange={() => {}}
                min={MIN_VAL}
                max={MAX_VAL}
                isDisable
                value={downlines}
                fgColor={COLOR}
                width={SIZE}
                height={SIZE}
                disableTextInput
            />
            <div><strong>Downlines</strong></div>
        </div>
    </div>


);

DashboardChart.proptypes = {
  earnings: PropTypes.number,
  downlines: PropTypes.number
};

DashboardChart.defaultProps = {
  earnings: 0,
  downlines: 0
};

export default DashboardChart;
