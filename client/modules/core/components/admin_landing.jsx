import React from 'react';
import {Widget} from '../../../../imports/components';


const AdminLanding = ({metrics,paths}) => {
  const {userMetric,packageMetric,adminMetric,payoutMetric} = metrics;
  const {userPath,packagePath,adminPath,payoutPath} = paths;
  return (
        <div className="row">
            <div className="col-lg-4">
                <Widget label="Users" to={userPath} icon='user' color="red"
                        metric={userMetric} text="total platform users"/>
            </div>
            <div className="col-lg-4">
                <Widget to={packagePath} label="Packages" icon='gift' color="lazur"
                        metric={packageMetric} text="total packages"/>
            </div>
            <div className="col-lg-4">
                <Widget to={adminPath} label="Administrators" icon='users' color="yellow"
                        metric={adminMetric} text="total administrators"/>
            </div>
            <div className="col-lg-4">
                <Widget to={payoutPath} label="Payouts" icon='money' color="navy"
                        metric={payoutMetric} text="pending payments"/>
            </div>
        </div>
    );
};
AdminLanding.defaultProps = {
  userMetric: 0,
  packageMetric: 0,
  adminMetric: 0,
  payoutMetric: 0
};
export default AdminLanding;
