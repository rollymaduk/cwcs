import React from 'react';
import PropTypes from 'prop-types';

const DashboardStatus = ({category,stage,level}) => (
    <table className="table small m-b-xs">
        <tbody>
        <tr>
            <td>
                <span className="m-r-md"><strong>CATEGORY: </strong></span> {category}
            </td>

        </tr>
        <tr>
            <td>
                <span className="m-r-md"><strong>STAGE:</strong></span>{stage}</td>
        </tr>
        <tr>
            <td>
                <span className="m-r-md"><strong>LEVEL: </strong></span>{level}</td>

        </tr>
        </tbody>
    </table>
);

DashboardStatus.propTypes = {
  category: PropTypes.string,
  level: PropTypes.number,
  stage: PropTypes.string,
};
export default DashboardStatus;
