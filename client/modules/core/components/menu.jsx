import React from 'react';
import MenuItem from './menu_item';
import ProfileItem from './profile_item';
import {intersection} from 'lodash';

const Menu = ({menuItems,profile,roles,logout}) => {
  return (
      <ul className="nav metismenu" id="side-menu">
          <ProfileItem profile={profile} logout={logout}/>
          {(menuItems && menuItems.length) ?
            menuItems
                .filter(item => !item.roles ||
                intersection(roles,item.roles).length)
                .map((menuItem,i) =>
                    (<MenuItem key={i} {...menuItem}/>)) : null}
      </ul>
    );
};

export default Menu;
