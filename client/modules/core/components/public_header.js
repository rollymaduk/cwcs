import React from 'react';
import PropTypes from 'prop-types';


const RegisterHeader = ({loginPath}) => {
    return (
        <div className="row border-bottom white-bg">
            <nav className="navbar navbar-static-top" role="navigation">
                <div className="navbar-header">
                    <button aria-controls="navbar"
                            aria-expanded="false"
                            data-target="#navbar"
                            data-toggle="collapse"
                            className="navbar-toggle collapsed"
                            type="button">
                        <i className="fa fa-reorder"/>
                    </button>
                    <a href="#" className="navbar-brand">CWCS</a>
                </div>
                <div className="navbar-collapse collapse" id="navbar">
                    <ul className="nav navbar-nav">
                        <li className="active">
                            <a aria-expanded="false" role="button" href={loginPath}>
                                Back to Login page
                            </a>
                        </li>
                    </ul>

                </div>
            </nav>
        </div>
    );
};

RegisterHeader.propTypes = {};
RegisterHeader.defaultProps = {
    loginPath:'../accounts/login'
};

export default RegisterHeader;
