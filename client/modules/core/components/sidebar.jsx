import React from 'react';
import Menu from './menu';

const Sidebar = ({menuItems,profile,roles,logout}) => (
    <nav className="navbar-default navbar-static-side" role="navigation">
        <div className="sidebar-collapse">
            <Menu menuItems={menuItems} roles={roles} profile={profile} logout={logout}/>
        </div>
    </nav>
);

Sidebar.defaultProps = {
  roles: [ '@admin','@user' ]
};
export default Sidebar;
