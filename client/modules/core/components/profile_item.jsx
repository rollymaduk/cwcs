import React from 'react';

const ProfileItem = ({profile,logout}) => {
  const {fullName ,status ,photoUrl} = profile || {};
  return (
    <li className="nav-header">
        <div className="dropdown profile-element">
            <span>
                {(photoUrl) ?
                    <img alt="image" width={48} height={48}
                         className="img-circle" src={photoUrl} /> : null}
            </span>
            <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                            <span className="clear">
                                <span className="block m-t-xs">
                                    <strong className="font-bold">
                                        {fullName}
                                    </strong>
                                </span>
                                <span className="text-muted text-xs block">
                                    {status} <b className="caret"/>
                                </span>
                            </span>
            </a>
            <ul className="dropdown-menu animated fadeInRight m-t-xs">
                <li><a onClick={() => logout()}>Logout</a></li>
            </ul>
        </div>
        <div className="logo-element">
            CWCS
        </div>
    </li>
);};

export default ProfileItem;
