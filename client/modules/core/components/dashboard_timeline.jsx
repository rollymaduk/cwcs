import React from 'react';
import classnames from 'classnames';
import {IBox} from '/imports/components';
import moment from 'moment';
import PropTypes from 'prop-types';

const DashboardTimeline = ({timeLines}) => (
  <IBox className="inspinia-timeline" title="Timeline">
      {(timeLines) ? timeLines.map((timeline,i) =>
          <div key={i}>{TimeLineItem(timeline)}</div>
      ) : null}
  </IBox>

);

const TimeLineItem = ({createdAt,description,title,icon}) => {
  const timeAgo = moment(createdAt).fromNow();
  const period = moment(createdAt).format('MMM Do YY');
  const iconClasses = classnames('fa',`fa-${icon || 'clock-o'}`);
  return (<div className="timeline-item">
        <div className="row">
            <div className="col-xs-3 date">
                <i className={iconClasses}/>
                {period}
                    <br/>
                    <small className="text-navy">{timeAgo}</small>
            </div>
            <div className="col-xs-7 content no-top-border">
                <p className="m-b-xs"><em>Milestone - </em><strong>{title}</strong></p>

                <p>{description}</p>
                 </div>
        </div>
    </div>);
};

DashboardTimeline.propTypes = {
  timeLines: PropTypes.arrayOf(PropTypes.shape({
    createdAt: PropTypes.object.isRequired,
    description: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    icon: PropTypes.string,
  }))

};

export default DashboardTimeline;
