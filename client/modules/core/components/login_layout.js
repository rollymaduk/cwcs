import React from 'react';
import Footer from './footer';
import Notifications from 'react-notify-toast';

const LoginLayout = ({content = () => null }) => (
    <div>
        <Notifications/>
        <div className="middle-box text-center loginscreen animated fadeInDown">

            <div>
                <h3 className="logo-name">CWCS</h3>
            </div>
            {content()}
            <div className="m-t"> <small>footer here</small> </div>
        </div>
    </div>

);

export default LoginLayout;
