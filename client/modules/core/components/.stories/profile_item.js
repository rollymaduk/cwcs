import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import ProfileItem from '../profile_item.jsx';

storiesOf('core.ProfileItem', module)
  .add('default view', () => {
    return (
      <ProfileItem />
    );
  })
