import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import DashboardTimeline from '../dashboard_timeline.jsx';

storiesOf('core.DashboardTimeline', module)
  .add('default view', () => {
    return (
      <DashboardTimeline />
    );
  })
