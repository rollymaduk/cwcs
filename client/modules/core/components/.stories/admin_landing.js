import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import AdminLanding from '../admin_landing.jsx';

storiesOf('core.AdminLanding', module)
  .add('default view', () => {
    return (
      <AdminLanding />
    );
  })
