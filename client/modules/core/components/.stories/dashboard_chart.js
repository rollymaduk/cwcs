import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import DashboardChart from '../dashboard_chart.jsx';

storiesOf('core.DashboardChart', module)
  .add('default view', () => {
    return (
      <DashboardChart />
    );
  })
