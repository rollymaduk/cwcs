import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import Login from '../../../user/components/account/login.jsx';

storiesOf('core.Login', module)
  .add('default view', () => {
    return (
      <Login />
    );
  })
