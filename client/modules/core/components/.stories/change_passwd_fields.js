import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import ChangePasswdFields from '../../../user/components/account/forgot_password_fields.jsx';

storiesOf('core.ChangePasswdFields', module)
  .add('default view', () => {
    return (
      <ChangePasswdFields />
    );
  })
