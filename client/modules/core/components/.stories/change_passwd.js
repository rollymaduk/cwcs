import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import ChangePasswd from '../../../user/components/account/forgot_password.jsx';

storiesOf('core.ChangePasswd', module)
  .add('default view', () => {
    return (
      <ChangePasswd />
    );
  })
