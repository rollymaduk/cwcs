import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import DashboardStatus from '../dashboard_status.jsx';

storiesOf('core.DashboardStatus', module)
  .add('default view', () => {
    return (
      <DashboardStatus />
    );
  })
