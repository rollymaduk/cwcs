import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import IboxHeader from '../../../../../imports/components/ibox_header.jsx';

storiesOf('core.IboxHeader', module)
  .add('default view', () => {
    return (
      <IboxHeader />
    );
  })
