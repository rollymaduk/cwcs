import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import DashboardProfile from '../dashboard_profile.jsx';

storiesOf('core.DashboardProfile', module)
  .add('default view', () => {
    return (
      <DashboardProfile />
    );
  })
