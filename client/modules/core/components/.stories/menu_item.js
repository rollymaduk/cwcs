import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import MenuItem from '../menu_item.jsx';

storiesOf('core.MenuItem', module)
  .add('default view', () => {
    return (
      <MenuItem to={action('link clicked')} name="test link" />
    );
  })
