import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import LoginFields from '../../../user/components/account/login_fields.jsx';

storiesOf('core.LoginFields', module)
  .add('default view', () => {
    return (
      <LoginFields />
    );
  })
