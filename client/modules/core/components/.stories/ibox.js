import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { setComposerStub } from 'react-komposer';
import Ibox from '../../../../../imports/components/ibox.jsx';

storiesOf('core.Ibox', module)
  .add('default view', () => {
    return (
      <Ibox />
    );
  })
