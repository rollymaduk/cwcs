import React from 'react';
import PropTypes from 'prop-types';
const AVATAR_SRC = '../../imgs/user.png';

const DashboardProfile = ({fullName,status,phone,email,img}) => (
    <div>
        <div className="profile-image">
            <img src={img} className="img-circle circle-border m-b-md" alt="profile"/>
        </div>
        <div className="profile-info">
            <div className="">
                <div>
                    <h2 className="no-margins">
                        {fullName}
                    </h2>
                    <h4>{status}</h4>
                    <small>
                        <div className="m-b-sm"><i className="fa fa-envelope"/>
                            <span className="m-l-sm">{email}</span>
                        </div>
                        <div className="m-t-sm"><i className="fa fa-phone"/>
                            <span className="m-l-sm">{phone}</span>
                        </div>
                    </small>
                </div>
            </div>
        </div>
    </div>
);

DashboardProfile.propTypes = {
  fullName: PropTypes.string,
  status: PropTypes.string,
  phone: PropTypes.string,
  email: PropTypes.string,
  img: PropTypes.string,
};

DashboardProfile.defaultProps = {
  img: AVATAR_SRC
};

export default DashboardProfile;
