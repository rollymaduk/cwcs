export default function (context) {
    const {LocalState} = context;
    context.Utils.resetAlert({LocalState});
}