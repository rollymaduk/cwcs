import initAlerts from './alerts';
// import initMetaTags from './meta_tags';

export default function (context) {
  initAlerts(context);
  // initMetaTags(context);
}
