export default [
    {to: './',name: 'Dashboard',icon: 'dashboard',hasArrow: true},
    {to: './packages',name: 'Packages',icon: 'gift',hasArrow: true},
    {to: './downlines',name: 'Downlines',icon: 'user',hasArrow: true},
    {to: './payments',name: 'Payout',icon: 'money',hasArrow: true},
];

