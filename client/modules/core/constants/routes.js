export default {
  ADMIN_DASHBOARD_NAME: '@admin.dashboard',
  ADMIN_DASHBOARD_PATH: '/admin-dashboard',
  DASHBOARD_PATH: '/dashboard/:userId',
  DASHBOARD_NAME: '@dashboard'
};
