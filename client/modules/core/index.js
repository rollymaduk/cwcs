import actions from './actions/index';
import routes from './routes.jsx';
import setAppDefaults from './configs';


export default {
  routes,
  actions,
  load(context) {
    const {FlowRouter,Roles,Tracker} = context;

    FlowRouter.wait();
    Tracker.autorun(() => {
      if (Roles.subscription.ready() && !FlowRouter._initialized) {
        FlowRouter.initialize();
      }
    });

    setAppDefaults(context);
  }
};
