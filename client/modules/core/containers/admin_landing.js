import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {routes} from '../constants';
import {routes as userRoutes} from '/client/modules/user/libs/constants';
import {routes as adminRoutes} from '/client/modules/admin/libs/constants';
import {routes as packageRoutes} from '/client/modules/package_type/lib/constants';
import {routes as payoutRoutes} from '/client/modules/payout/libs/constants';
import AdminLanding from '../components/admin_landing.jsx';

export const depsMapper = (context, actions) => ({
  context: () => context,
});

const preparePaths = (FlowRouter) => {
  const userPath = FlowRouter.path(userRoutes.USER_LIST_NAME);
  const adminPath = FlowRouter.path(adminRoutes.ADMIN_LIST_NAME);
  const packagePath = FlowRouter.path(packageRoutes.PACKAGE_TYPE_LIST_NAME);
  const payoutPath = FlowRouter.path(payoutRoutes.PAYOUT_LIST_NAME);
  return {userPath,adminPath,packagePath,payoutPath};
};

const prepareMetrics = (Models) => {
  const {getCount} = Models.Counters;
  const packageMetric = getCount(Models.PackageType);
  const userMetric = getCount(Models.User);
  const adminMetric = getCount(Models.User,'@admin');
  const payoutMetric = getCount(Models.Payout,'@new');
  return {packageMetric,userMetric,adminMetric,payoutMetric};
};
export const composer = ({context}, onData) => {
  const {Models,FlowRouter} = context();
  onData(null, {paths: preparePaths(FlowRouter),metrics: prepareMetrics(Models)});
};

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(AdminLanding);
