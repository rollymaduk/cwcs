import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import SideBar from '../components/sidebar.jsx';
import {get} from 'lodash/fp';
import {routes as core} from '../constants';
import {getFullName} from '/imports/utils';
import {routes as users} from '../../user/libs/constants';
import {routes as admins} from '../../admin/libs/constants';
import {routes as pkg} from '../../package_type/lib/constants';
import {routes as pay} from '../../payout/libs/constants';
const getEmail = get('emails.0.address');

function getProfileItem(Meteor,FlowRouter) {
  return {
    fullName: getFullName(Meteor.user()),
    photoUrl: FlowRouter.path('/imgs/user.png')
  };
}

function prepareMenuItems(FlowRouter,Meteor) {
  // const {path} = FlowRouter;
  const email = getEmail(Meteor.user());
  const userId = Meteor.userId();
  return [
    {
      to: FlowRouter.path(core.DASHBOARD_NAME,{userId}),
      name: 'Dashboard',icon: 'dashboard',
      roles: [ '@user' ]
    },
    {
      to: FlowRouter.path(core.ADMIN_DASHBOARD_NAME),
      name: 'Dashboard',icon: 'dashboard',
      roles: [ '@admin' ]
    },
    {
      to: FlowRouter.path(pkg.PACKAGE_TYPE_LIST_NAME),
      roles: [ '@admin' ],
      name: 'Packages',icon: 'gift'
    },
    {
      to: FlowRouter.path(users.DOWNLINE_LIST_NAME,{userId: email}),
      roles: [ '@user' ],
      name: 'Downlines',icon: 'user'
    },
    {
      to: FlowRouter.path(admins.ADMIN_LIST_NAME),
      roles: [ '@admin' ],
      name: 'Administrators',icon: 'users'
    },
    {
      to: FlowRouter.path(users.USER_LIST_NAME),
      roles: [ '@admin' ],
      name: 'Users',icon: 'user'
    },
    {
      to: FlowRouter.path(pay.PAYOUT_LIST_NAME),
      roles: [ '@admin' ],
      name: 'Payout',icon: 'money'
    },
    {
      to: FlowRouter.path(pay.PAYOUT_HISTORY_NAME,{userId}),
      roles: [ '@user' ],
      name: 'Payments',icon: 'money'
    },
  ];
}

export const composer = ({context}, onData) => {
  const {Meteor,FlowRouter} = context();

  onData(null, {
    menuItems: prepareMenuItems(FlowRouter,Meteor),
    profile: getProfileItem(Meteor,FlowRouter),
    roles: Meteor.user() && Meteor.user().roles || []
  });
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  logout: actions.users.logout
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(SideBar);
