import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import Container from '../components/container';
import {toggleBodyClass} from '/imports/utils';
const RESPONSIVE_CLASS = 'body-small';
const RESPONSIVE_WIDTH = 768;
const prepareMediaQueries = () => {
  const width = {width: RESPONSIVE_WIDTH};
  const min = {width,action: toggleBodyClass(false,RESPONSIVE_CLASS)};
  const max = {width,action: toggleBodyClass(true,RESPONSIVE_CLASS)};
  return {min,max};
};

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();
  onData(null, {...prepareMediaQueries()});
};

export const depsMapper = (context, actions) => ({
  context: () => context,
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(Container);
