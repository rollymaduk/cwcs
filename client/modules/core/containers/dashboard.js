import {useDeps, composeAll, composeWithTracker} from 'mantra-core';
import {get} from 'lodash/fp';
const PARAMS_PATH = 'userId';
const getStatus = get('packageType.name');
const getEmail = get('emails.0.address');



import Dashboard from '../components/dashboard.jsx';

const getProfileData = (data) => {
  const {phone} = data.profile;
  const {fullName} = data;
  return {
    status: getStatus(data),
    email: getEmail(data),
    phone,
    fullName
  };
};
const getStatusData = (data) => {
  const {stage,level} = data.packageType;
  const {category} = data;
  return {category,level,stage};
};

const getChartData = (data) => {
  const {
    earningsPercent: earnings,
      downlinesPercent: downlines} = data;
  return {earnings,downlines};
};

const getTimelineData = (data) => {
  const {timelines: timeLines} = data;
  return {timeLines};
};

const transformToDashboardData = (data) => {
  if (data) {
    return {
      profileData: getProfileData(data),
      timelineData: getTimelineData(data),
      statusData: getStatusData(data),
      chartData: getChartData(data),
    };
  }

};

export const composer = ({context,fetchData,pageSize,userId}, onData) => {
  const {FlowRouter} = context();
  const selectedId = userId || FlowRouter.getParam(PARAMS_PATH);
  const selector = {_id: selectedId};
  fetchData({pageSize,selector},({data}) => {
    onData(null, {...transformToDashboardData(data && data[0])});
  });
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  fetchData: actions.users.fetchData,
  pageSize: 1
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(Dashboard);
