import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';
import {get} from 'lodash/fp';
import Header from '../components/header.jsx';
const getFirstName = get('profile.firstName');
import {toggleBodyClass} from '/imports/utils';

export const composer = ({context}, onData) => {
  const {Meteor} = context();

  onData(null, {name: getFirstName(Meteor.user())});
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  logout: actions.users.logout,
  toggleNav: toggleBodyClass(null,'mini-navbar'),
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(Header);
