import * as Models from '/lib/models';
import * as Collections from '/lib/collections';
import * as Queries from '/lib/queries';
import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {ReactiveDict} from 'meteor/reactive-dict';
import {Tracker} from 'meteor/tracker';
import * as Utils from '/imports/utils';
import {notify} from 'react-notify-toast';
import {Roles} from 'meteor/alanning:roles';
const ShowAlerts = notify.createShowQueue();
const AppSubs = {};

export default function () {
  return {
    Meteor,
    Roles,
    Accounts,
    FlowRouter,
    Queries,
    Models,
    Collections,
    LocalState: new ReactiveDict(),
    Tracker,
    Utils,
    AppSubs,
    ShowAlerts,
  };
}
