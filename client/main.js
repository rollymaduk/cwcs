import {createApp} from 'mantra-core';
import initContext from './configs/context';

// modules
import coreModule from './modules/core/index';
import packageTypeModule from './modules/package_type';
import userModule from './modules/user';
import adminModule from './modules/admin';
import payoutModule from './modules/payout';

// init context
const context = initContext();

// create app
const app = createApp(context);
// load modules
app.loadModule(coreModule);
app.loadModule(packageTypeModule);
app.loadModule(userModule);
app.loadModule(adminModule);
app.loadModule(payoutModule);
app.init();
