import {globalRoutes} from '/lib/constants';
import {routes} from '/client/modules/user/libs/constants';
import {mount} from 'react-mounter';
import NotFound from '../modules/core/components/not_found';
const BODY_GRAY_CLASS = 'gray-bg';
const BODY_CLASS = 'top-navigation';
import {hasAccessFor} from '/imports/utils';


import {toggleBodyClass,createRouterGroup} from '/imports/utils';

const mustHaveRole = (AppContext,roles,path) => (context,redirect,stop) => {
  const {Meteor,Roles} = AppContext;
  if (!hasAccessFor({Meteor,Roles},roles)) {
    if (path) {
      redirect(path);
    } else {
      mount(NotFound);
      stop();
    }
  }
};


export const createAppGroup = ({FlowRouter,Meteor,Roles}) => {
  return createRouterGroup(FlowRouter,{
    prefix: globalRoutes.ROUTE_APP_PREFIX,
    name: globalRoutes.ROUTE_APP_NAME,
    triggersEnter: [ mustHaveRole({Meteor,Roles},[ '@admin','@user' ],routes.LOGIN_NAME) ],
  });

};

export const createAdminGroup = ({FlowRouter,Meteor,Roles}) => {
  return createRouterGroup(FlowRouter,{
    prefix: globalRoutes.ROUTE_ADMIN_PREFIX,
    name: globalRoutes.ROUTE_ADMIN_NAME,
    triggersEnter: [ mustHaveRole({Meteor,Roles},[ '@admin' ],routes.LOGIN_NAME) ],
  });
};

export const createAccountGroup = ({FlowRouter}) => {
  return createRouterGroup(FlowRouter,
    {
      prefix: globalRoutes.ROUTE_ACCOUNTS_PREFIX,
      name: globalRoutes.ROUTE_ACCOUNTS_NAME,
      triggersExit: [ toggleBodyClass(false,BODY_GRAY_CLASS) ],
      triggersEnter: [ toggleBodyClass(true,BODY_GRAY_CLASS) ],
    });
};

export const createPublicGroup = ({FlowRouter}) => {
  return createRouterGroup(FlowRouter,
    {
      prefix: globalRoutes.ROUTE_PUBLIC_PREFIX,
      name: globalRoutes.ROUTE_PUBLIC_NAME,
      triggersExit: [ toggleBodyClass(false,BODY_CLASS) ],
      triggersEnter: [ toggleBodyClass(true,BODY_CLASS) ],
    });
};
