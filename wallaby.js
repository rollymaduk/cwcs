module.exports = function (wallaby) {
    // There is a weird error with the mui and mantra.
    // See: https://goo.gl/cLH8ib
    // Using require here seems to be the error.
    // Renaming it into `load` just fixed the issue.
  const load = require;

  return {
    files: [
      /* 'client/modules/!**!/actions/!*.js',
      'client/modules/!**!/containers/!*.js',
      'client/modules/!**!/components/!*.js?(x)',
      'client/modules/!**!/libs/!*.js',
      'client/modules/!**!/constants/!*.js',*/
      'client/**/*.js?(x)',
      'server/**/*.js',
      'providers/**/*.js',
      'imports/**/*.js?(x)',
      'libs/**/*.js?(x)',
      '/lib/**/*.js?(x)',
        {pattern: 'imports/**/tests/*js?(x)',ignore: true},
        {pattern: 'client/**/tests/*js?(x)',ignore: true},
        {pattern: 'server/**/tests/*js',ignore: true},
        {pattern: 'providers/**/tests/*js',ignore: true}
    ],
    tests: [
      'client/**/tests/*.js?(x)',
      'server/**/tests/*.js?(x)',
      'providers/**/tests/*.js?(x)',
      'imports/**/tests/*.js?(x)',
    ],
    compilers: {
      '**/*.js*': wallaby.compilers.babel({
        babel: load('babel-core'),
        presets: [ 'es2015', 'stage-2', 'react' ]
      })
    },
    env: {
      type: 'node'
    },
    testFramework: 'mocha',
    setup() {
      global.React = require('react');

        // Taken from https://github.com/airbnb/enzyme/blob/master/docs/guides/jsdom.md
      const jsdom = require('jsdom').jsdom;

      const exposedProperties = [ 'window', 'navigator', 'document' ];

      global.document = jsdom('');
      global.window = document.defaultView;
      Object.keys(document.defaultView).forEach((property) => {
        if (typeof global[property] === 'undefined') {
          exposedProperties.push(property);
          global[property] = document.defaultView[property];
        }
      });

      global.navigator = {
        userAgent: 'node.js'
      };
    }
  };
};
