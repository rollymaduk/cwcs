/**
 * Created by ronald on 5/30/17.
 */
import {configure} from '@kadira/storybook'
import '../client/modules/core/themes/stylesheets/font-awesome.less'
import '../client/modules/core/themes/stylesheets/bootstrap.min.css'
import '../client/modules/core/bootstrap.min.js'
import '../client/modules/core/themes/stylesheets/style.less'


function loadStories(){
  require('../client/modules/payout/components/.stories/index.js');
  require('../client/modules/admin/components/.stories/index.js');
  require('../client/modules/user/components/.stories/index.js');
  require('../client/modules/package_type/components/.stories/index.js');
    require('../client/modules/core/components/.stories/index')
}

configure(loadStories,module)