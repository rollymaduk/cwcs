/**
 * Created by ronald on 5/30/17.
 */
const path = require('path');
const webpack = require('webpack')

module.exports = {
    module: {
        loaders: [
            {
                test: /\.(jpe?g|jpg|png|woff|eot|bmp|svg|woff2|ttf)(\?[a-z0-9=&.]+)?$/,
                loader:"file-loader",
                include: path.resolve(__dirname, '../'),
            },
            {
                test: /\.less$/,
                loaders: ["style-loader", "css-loader","less-loader"],
                include: path.resolve(__dirname, '../')
            },
            {
                test: /\.css$/,
                loaders: ["style-loader", "css-loader"],
                include: path.resolve(__dirname, '../')
            },

        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
}