import {Accounts} from 'meteor/accounts-base';
import {Meteor} from 'meteor/meteor';
import {Roles} from 'meteor/alanning:roles';
import {User} from '/lib/models';
import {get,set,includes} from 'lodash';
import {SaveModel,normalizeMailUrl} from '../util';
import {getVerifyEmailToken,getFullName} from '/imports/utils';
const WELCOME_EMAIL_TEMPLATE = 'welcome';
const TRY_SEND_INTERVAL = 3000;
const TOKEN_PATH = 'services.email.verificationTokens';


const prepareMail = (address,data,token) => {
  let url = normalizeMailUrl(Accounts.urls.verifyEmail(token));
  return {
    template: WELCOME_EMAIL_TEMPLATE,
    data: {...data,url},
    address
  };
};

const setUserToken = (user,token) => {
  const existingTokens = get(user,TOKEN_PATH) || [];
  set(user,TOKEN_PATH,existingTokens.concat([ token ]));
  return user;
};


export default function (mailServer) {
  Accounts.onCreateUser((options,user) => {
    const {profile,account,billing,packageTypeId,email: address,category} = options.profile;
    /* send mail on a different thread after a certain timeout*/
    const handle = Meteor.setInterval(() => {
      try {
        // console.log(address);
        const newUser = User.findOne({'emails.0.address': address});
        if (newUser) {
            /* update token*/
          if (Roles.userIsInRole(newUser._id,[ '@admin' ])) {
            return Meteor.clearInterval(handle);
          }
          const tokenRecord = getVerifyEmailToken(address);
          SaveModel(setUserToken(newUser,tokenRecord),newUser);

          /* send mail*/
          const fullName = getFullName(options.profile);
          const mail = prepareMail(address,{fullName},tokenRecord.token);
          mailServer.create(mail);
          return Meteor.clearInterval(handle);
        }

      } catch (err) {
        Meteor.clearInterval(handle);
        throw new Meteor.Error(err);
      }

    },TRY_SEND_INTERVAL);
    return {...user,profile,billing,account,packageTypeId,category};
  });
}
