import {PackageType} from '../../../../lib/models/index';

PackageType.extend({
  fields: {
    name: {
      type: String,
    }
  }
});
