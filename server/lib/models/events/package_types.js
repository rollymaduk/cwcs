import {PackageType} from '/lib/models';
import {getPackageTitle} from '/imports/utils';

PackageType.extend({
  events: {
    beforeSave: (e) => {
      e.currentTarget.name = getPackageTitle(e.currentTarget);
    }
  }
});
