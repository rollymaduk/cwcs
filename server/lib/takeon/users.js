import {SUPER_ADMIN,ADMIN_USER} from '../constants/index';
import {Accounts} from 'meteor/accounts-base';
import {Meteor} from 'meteor/meteor';
import {compose,get} from 'lodash/fp';
import {prepareUserForCreate} from '../../methods/user/util';
import {CommitTransaction} from '../../util/index';
import {Roles} from 'meteor/alanning:roles';
const getUser = get([ 'user' ]);
const TAKE_ON_EMAIL = 'super@admin.com';
const TAKE_ON_PASS = 'password';



export const prepareAdminForCreate = compose(getUser,prepareUserForCreate);



const PhantomUser = {
  email: TAKE_ON_EMAIL,
  password: TAKE_ON_PASS,
  profile: {
    firstName: 'Super',
    lastName: 'Admin',
    dob: '1/1/1909',
    gender: 'M'
  },
  billing: {
    bankName: 'fake Bank',
    tellerNumber: '123',

  },
  account: {
    packageType: 'non-existent',
    referral: 'none',
    bankDetails: {
      bankName: 'None',
      accountNumber: '456'
    }
  }
};


export default function (neo4jServer) {
  Meteor.startup(() => {
    if (!Accounts.findUserByEmail(TAKE_ON_EMAIL)) {
      const params = prepareAdminForCreate(PhantomUser);
      const tranx = neo4jServer.getTransaction();
      try {
        neo4jServer.addUser(params,tranx);
        const userId = Accounts.createUser({email: TAKE_ON_EMAIL,
            password: TAKE_ON_PASS,profile: PhantomUser});
        Roles.addUsersToRoles(userId,[ SUPER_ADMIN,ADMIN_USER ]);
        CommitTransaction(tranx);
      } catch (err) {
        CommitTransaction(tranx,false);
        throw err;
      }

    }
  });
}

