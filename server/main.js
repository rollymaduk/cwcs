import publications from './publications';
import methods from './methods';
import hooks from './hooks';
import accounts from './lib/accounts';
import takeon from './lib/takeon';
import mailProvider from '/imports/providers/email';
import dotenv from 'dotenv';
import {getNeo4JServer} from '/imports/providers/neo4j';
dotenv.config({path: process.env.PWD + '/.env'});

const {SEND_WITH_US_API_KEY: swApiKey} = process.env;
const neo4jServer = getNeo4JServer();
const mailServer = mailProvider(swApiKey);

takeon(neo4jServer);
publications();
methods(neo4jServer,mailServer);
hooks();
accounts(mailServer);

