import packageTypeMethods from './package_types';
import userMethods from './user/index';
import adminMethods from './admin';
import payoutMethods from './payout';

export default function (neo4jServer,mailServer) {
  packageTypeMethods(neo4jServer);
  userMethods(neo4jServer,mailServer);
  adminMethods();
  payoutMethods();
}

