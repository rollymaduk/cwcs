import {PackageType} from '../../../lib/models';
import {getPackageTitle,queryBuilder} from '../../../imports/utils';
import {Promise} from 'meteor/promise';
import {SaveModel,CommitTransaction,checkAccessFor} from '../../util/index';
import {get,omit} from 'lodash/fp';
const getUpdateFields = omit([ 'packageId' ]);
const getPackageId = get('_id');
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';
import UUID from 'cuid';
const ACCEPTED_ROLES = [ '@admin' ];

/* todo refactor packagetype methods*/

const packageExists = (packageType) => {
  const {category,level,stage} = packageType;
  const {selector} = queryBuilder({selector: {category,stage,level}});
  return PackageType.findOne(selector);
};

const preparePackageForEdit = (packageType) => {
  const params = preparePackageParams(packageType);
  const edit = getUpdateFields(params);
  return {edit,packageId: params.packageId};
};

const preparePackageParams = (packageType) => {
  const title = getPackageTitle(packageType);
  const {category: group,level,minimumDownlines: maxDownlines,_id} = packageType;
  return {packageId: _id,title,group,level,maxDownlines};

};


const RemoveOnNeo4J = (server, packageType,tranx) => {
  const packageId = getPackageTitle(packageType);
  return Promise.await(server.removePackage(packageId,tranx));
};

export default function (neo4jServer) {
  PackageType.extend({
    meteorMethods: {
      update(packageType) {
        check(packageType,Object);
        checkAccessFor(ACCEPTED_ROLES);
        const tranx = neo4jServer.getTransaction();
        try {
          const {edit,packageId} = preparePackageForEdit(packageType);
          Promise.await(neo4jServer
                  .updatePackage({packageId,edit},tranx));
          const res = SaveModel(packageType,this);
          CommitTransaction(tranx,true);
          return res;
        } catch (err) {
          CommitTransaction(tranx,false);
          throw err;
        }
      },

      create(packageType) {
        check(packageType,Object);
        checkAccessFor(ACCEPTED_ROLES);
        const tranx = neo4jServer.getTransaction();
        if (!getPackageId(this) && packageExists(packageType)) {
          throw new Meteor.Error('invalid action','package already exists!');
        }
        try {
          const _id = UUID();
          const params = preparePackageParams({...packageType,_id});
          Promise.await(neo4jServer
                .addPackage(params,tranx));
          const res = SaveModel({...packageType,_id,name: getPackageTitle(packageType)},this);
          CommitTransaction(tranx,true);
          return res;
        } catch (err) {
                    /* rollback db here*/
          CommitTransaction(tranx,false);
          throw err;
        }
      },

      removePackage(packageType) {
        check(packageType,Object);
        checkAccessFor(ACCEPTED_ROLES);
        const tranx = neo4jServer.getTransaction();
        try {
          RemoveOnNeo4J(neo4jServer,packageType,tranx);
          const res = this.remove(packageType._id);
          CommitTransaction(tranx);
          return res;
        } catch (err) {
          CommitTransaction(tranx,false);
          throw err;
        }
      }
    }
  });
}

