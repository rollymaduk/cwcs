import {Payout} from '../../../lib/models';
import {SaveModel,checkAccessFor} from '../../util/index';
import {check} from 'meteor/check';
const ACCEPTED_ROLES = [ '@admin' ];

export default function () {
  Payout.extend({
    meteorMethods: {
      update(data) {
        try {
          check(data,{_id: String,status: String});
          checkAccessFor(ACCEPTED_ROLES);
          return SaveModel(data,this);
        } catch (err) {
          throw err;
        }
      },
    }
  });


}
