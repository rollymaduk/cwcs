import {AdminUser} from '../../../lib/models';
import {check} from 'meteor/check';
import {Roles} from 'meteor/alanning:roles';
import {SaveModel,checkAccessFor} from '../../util/index';
import {Accounts} from 'meteor/accounts-base';
import {ADMIN_USER,FIXED_PASSWORD} from '../../lib/constants/index';
const ACCEPTED_ROLES = '@admin';

export default function () {
  AdminUser.extend({
    meteorMethods: {
      create(userItem,moreRoles = []) {
        check(userItem,Object);
        const roles = moreRoles.concat([ ADMIN_USER ]);
        checkAccessFor(ACCEPTED_ROLES);
        const {email,password = FIXED_PASSWORD} = userItem;
        try {
          const userId = Accounts.createUser({email,profile: userItem,password});
          Roles.addUsersToRoles(userId,roles);
          return userId;
        } catch (err) {
          throw err;
        }
      },
      removeAdmin(adminId) {
        check(adminId,String);
        checkAccessFor(ACCEPTED_ROLES);
        return this.remove(adminId);
      },
      update(admin) {
        check(admin,Object);
        checkAccessFor(ACCEPTED_ROLES);
        return SaveModel(admin,this);
      }
    }
  });


}
