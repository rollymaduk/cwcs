const {describe,it} = global;
import {castUserDOB} from '../util';
import {expect} from 'chai';
const FAKE_DOB = '1982-05-04T23:00:00.000Z';
const FAKE_ACCT_NUMBER = '0988778883763';
const user = {
  profile: {
    firstName: 'foo',
    lastName: 'bar',
    DOB: FAKE_DOB
  },
  account: {
    bankDetails: {
      accountNumber: FAKE_ACCT_NUMBER
    }
  }
};
describe('util.castUser',function () {
  it('should cast user profile  DOB to date',function () {
    expect(castUserDOB(user).profile.DOB).to.deep.equal(new Date(FAKE_DOB));
  });
});


