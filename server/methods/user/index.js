import crud from './crud';
import accounts from './accounts';
import downlines from './downlines';

export default function (neo4jServer,mailServer) {
  crud(neo4jServer);
  accounts(mailServer);
  downlines(neo4jServer);

}
