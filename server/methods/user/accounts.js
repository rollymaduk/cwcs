import {User} from '/lib/models';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';
import {Accounts} from 'meteor/accounts-base';
import {get} from 'lodash/fp';
import {set} from 'lodash';
import {getResetEmailToken} from '/imports/utils';
import {SaveModel,checkAccessFor,normalizeMailUrl} from '../../util';
const getUserEmail = get('emails.0.address');
const RESET_PATH = 'services.password.reset';
// const setToken = set('services.password.reset');
const RESET_PASSWD_TEMPLATE = 'resetPassword';
const ACCEPTED_ROLES = [ '@user','@admin' ];

const handleError = (msg, throwError = true) => {
  const error = new Meteor.Error(
        403,
        Accounts._options.ambiguousErrorMessages ?
            'Login failure. Please check your login credentials.' : msg
    );
  if (throwError) {
    throw error;
  }
  return error;
};

const getUserFromDb = (emailOrId) => {
  if (typeof emailOrId === 'string') {
    if (emailOrId.indexOf('@') !== -1) {
      return User.findOne({'emails.0.address': emailOrId});
    }
    return User.findOne(emailOrId);
  }
};

const prepareEmail = (address,token) => {
  let url = normalizeMailUrl(Accounts.urls.resetPassword(token));
  url = url.replace('#','accounts');
  return {
    data: {url},
    address,
    template: RESET_PASSWD_TEMPLATE
  };
};

const setUserToken = (user,token) => {
  set(user,RESET_PATH,token);
  return user;
};

const sendResetEmail = (user,email,server) => {
  const userEmail = email || getUserEmail(user);

  if (!userEmail) {
    handleError('No such email for user.');
  }

  const tokenRecord = getResetEmailToken(userEmail);

  SaveModel(setUserToken(user,tokenRecord),user);

  Meteor.defer(() => {
    server.create(prepareEmail(userEmail,tokenRecord.token));
  });

  return 1;
};

export default function (mailServer) {
  User.extend({
    meteorMethods: {

      sendResetPasswordEmail(userId,email) {
        check(userId,String);
        check(email,String);
        const user = getUserFromDb(userId);
        return sendResetEmail(user,email,mailServer);
      },

      forgotPassword(email) {
        check(email,String);
        const user = getUserFromDb(email);
        return sendResetEmail(user,email,mailServer);
      },

      setPassword(userId,password) {
        check(userId,String);
        checkAccessFor(ACCEPTED_ROLES);
        return Accounts.setPassword(userId,password);
      }

    }
  });
}
