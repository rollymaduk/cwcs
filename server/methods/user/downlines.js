import {User} from '/lib/models';
import {Meteor} from 'meteor/meteor';
import {Promise} from 'meteor/promise';
import {flatten,get} from 'lodash/fp';
import {omit} from 'lodash';
const getUserId = get('selector.userId');
import {downlines} from '/lib/queries';
import {check} from 'meteor/check';
import {checkAccessFor} from '../../util';
const ACCEPTED_ROLES = [ '@admin','@user' ];

const prepareDownlineFilters = (mySelector,options,dLines) => {
  const emails = dLines.map(downline => (downline.d.email));
  const selector = {...mySelector,'emails.0.address': {$in: emails}};
  return {selector,options};
};

export default function (neo4JServer) {
  User.extend({
    meteorMethods: {
      getDownlineCount({userId}) {
        try {
          check(userId,String);
          checkAccessFor(ACCEPTED_ROLES);
          const res = Promise.await(neo4JServer.getDownlineCount(userId));
          return res[0].downlines;
        } catch (err) {
          throw new Meteor.Error('500',err.message);
        }
      },

      getDownlines(params) {
        try {
          check(params,Object);
          checkAccessFor(ACCEPTED_ROLES);
          const res = Promise.await(neo4JServer.getDownlines(getUserId(params)));
          const result = flatten(res);
          if (result.length) {
            let {selector,options} = params;
            selector = omit(params.selector,[ 'userId' ]);
            const query = downlines(prepareDownlineFilters(selector,options,result));
            return query.fetch();
          }
          return flatten(res);
        } catch (err) {
          throw new Meteor.Error('500',err.message);
        }

      }
    }
  });
}

