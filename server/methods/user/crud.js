import {User,PackageType} from '/lib/models';
import {Accounts} from 'meteor/accounts-base';
import {Users} from '/lib/collections';
import {CommitTransaction,SaveModel,checkAccessFor} from '../../util/index';
import {NORMAL_USER,FIXED_PASSWORD} from '../../lib/constants';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';
import {Roles} from 'meteor/alanning:roles';
import {Promise} from 'meteor/promise';
import {flatten,get} from 'lodash/fp';
import {
    prepareUserForCreate,
    prepareUserForUpdate,
    castUserDOB,

} from './util';

const getUpline = get('upline.id');
const getPackage = get('package.id');
const getEmail = get('emails.0.address');

const ACCEPTED_ROLES = [ '@admin','@user' ];

const rewardUplines = (payload) => {
  if (payload && payload.length) {
    payload.map((item) => {
      const user = User.findOne({'emails.0.address': getUpline(item),
                roles: {$in: [ NORMAL_USER ]}});
      if (user) {
        SaveModel({
          packageTypeId: getPackage(item)
        },user);
        const packageLink = Users.getLink(user,'packageType');
        packageLink.set(getPackage(item));
        return getPackage(item);
      }
    });
  }
};

const LinkPackageToUser = (user,packageId) => {
  const packageLink = Meteor.users.getLink(user,'packageType');
  return packageLink.set(packageId);

};

const uplineExists = (user) => {
  const {referral: uplineId} = user.account;
  return Accounts.findUserByEmail(uplineId);
};

const getDownlines = (user) => {
  const {packageTypeId} = user;
  return PackageType.findOne(packageTypeId).minimumDownlines;
};


export default function (neo4JServer) {
  User.extend({
    meteorMethods: {
      update(user) {
        check(user,Object);
        checkAccessFor(ACCEPTED_ROLES);
        const userForUpdate = User.findOne(user._id);
        const email = getEmail(userForUpdate);
        const tranx = neo4JServer.getTransaction();
        const params = prepareUserForUpdate({...user,email});
        try {
          Promise.await(neo4JServer.updateUser(params,tranx));
          const res = SaveModel(user,userForUpdate);
          CommitTransaction(tranx);
          return res;
        } catch (err) {
          CommitTransaction(tranx,false);
          throw err;
        }
      },



      create(user) {
        check(user,Object);
        let userIsCreated = false;
        const referral = uplineExists(user);
        if (!referral) {
          throw new Meteor.Error('Invalid Action','Referral email does not exist!');
        }

        if (!referral.isActive && Roles.userIsInRole(referral._id,[ '@user' ])) {
          throw new Meteor.Error('500', 'Referral has not been activated, contact admin!');
        }

        const packageType = PackageType.findOne(user.packageTypeId);
        if (!packageType) {
          throw new Meteor.Error('Invalid Action','Package does not exist!');
        }

        const category = packageType.category;
        const maxDownlines = getDownlines(user);
        const {email,password = FIXED_PASSWORD} = user;
        const tranx = neo4JServer.getTransaction();
        const params = prepareUserForCreate({...user,maxDownlines});
        try {
          Promise.await(neo4JServer.addUserToUpline(params,tranx));

          const payload = Promise.await(neo4JServer.rewardUserUplines(email,tranx));

          const userId = Accounts.createUser(
                        {email,profile: castUserDOB({...user,category}),password});

          userIsCreated = true;

          Roles.addUsersToRoles(userId,[ NORMAL_USER ]);

          LinkPackageToUser(userId,packageType._id);

          rewardUplines(flatten(payload));

          CommitTransaction(tranx);

          return userId;
        } catch (err) {
          if (userIsCreated) {
            const userItem = User.findOne({'emails.0.address': email});
            userItem.remove();
          }
          CommitTransaction(tranx,false);
          throw err;
        }
      },

    }
  });
}

