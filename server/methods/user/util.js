import {getFullName} from '../../../imports/utils';
import {get,set} from 'lodash/fp';
const DATE_PATH = 'profile.DOB';
const getDate = get(DATE_PATH);


export const castUserDOB = (user) => {
  return set(DATE_PATH,new Date(getDate(user)),user);
};


export const prepareUserForCreate = (userItem) => {
  const fullname = getFullName(userItem);
  const {referral: uplineId} = userItem.account;
  const {email: userId,email,isActive = false,maxDownlines,
      packageTypeId: packageId} = userItem;
  const user = {userId,email,isActive,fullname};
  return {uplineId,packageId,maxDownlines,user };
};


export const prepareUserForUpdate = (userItem) => {
  const {email: userId,isActive} = userItem;
  return {userId,edit: {isActive}};
};




