import {Meteor} from 'meteor/meteor';
import {Users} from '/lib/collections';
import {NORMAL_USER} from '../lib/constants';


const publishOnlyNormalRoles = (selector) => {
  return {...selector,roles: {$in: [ NORMAL_USER ]}};
};

export default function () {
  Meteor.users.expose();
}



