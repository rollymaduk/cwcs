import packageType from './package_types';
import payout from './payout';
import user from './user';
/* import admin from './admin';*/

export default function () {
  packageType();
  user();
  payout();
}
