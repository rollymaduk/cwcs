import {Payout,Timeline} from '/lib/models';
import {Timelines} from '/lib/collections';
import {TIMELINE_TEXT,
    TIMELINE_TITLE,EVENT_MILESTONE} from '/lib/constants';
import {SaveModel} from '../../util';


require('string.format');

const prepareTimeLine = (name) => {
  return {
    title: TIMELINE_TITLE.format({name}),
    description: TIMELINE_TEXT.format({name}),
    type: EVENT_MILESTONE
  };
};

export default function () {
  return Payout.extend({
    events: {
      beforeInsert(e) {
        const doc = e.currentTarget;
        const timeline = new Timeline();
        const name = doc.packageType;
        const result = SaveModel(prepareTimeLine(name),
          timeline);
        const userLink = Timelines.getLink(result,'user');
        userLink.set(doc.userId);
      }
    }
  });
}


