import {User,Payout,PackageType} from '/lib/models';
import {Payouts} from '/lib/collections';
import {get} from 'lodash/fp';
import {Meteor} from 'meteor/meteor';
import {SaveModel} from '../../util';

const PACKAGE_TYPE_PATH = 'packageTypeId';
const EMAIL_PATH = 'emails.0.address';
const REWARD_EMAIL_TEMPLATE = 'reward';

const getPackageId = get(PACKAGE_TYPE_PATH);
const getEmail = get(EMAIL_PATH);

const prepareMail = ({user,status}) => {
  return {
    template: REWARD_EMAIL_TEMPLATE,
    data: {status},
    address: getEmail(user)
  };
};


const getPackageName = (doc) => {
  return PackageType.findOne(getPackageId(doc),
        {fields: {name: 1}});
};


export default function (mailServer) {
  return User.extend({
    events: {
      beforeUpdate(e) {
        const doc = e.currentTarget;
        if (doc.isModified(PACKAGE_TYPE_PATH)) {
          const payout = new Payout();

          const packageType = getPackageName(doc);

          const details = packageType.reward.map(item => {
            return {item: item.text};
          });

          const result = SaveModel({
            packageType: packageType.name,
            loanAmount: packageType.loanAmount,
            details,
            userId: doc._id,
          },payout);

          const userLink = Payouts.getLink(result,'user');
          userLink.set(doc._id);
        }
      },

      afterUpdate(e) {
        const doc = e.currentTarget;
        if (doc.isModified(PACKAGE_TYPE_PATH)) {
          const packageType = getPackageName(doc);
          const status = packageType && packageType.name;
          const mail = prepareMail({user: doc,status});
          Meteor.defer(() => {mailServer.create(mail);});
        }
      }
    }
  });
}


