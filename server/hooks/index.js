import userHooks from './user';
import payoutHooks from './payout';

export default function () {
  userHooks();
  payoutHooks();
}
