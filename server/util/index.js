const defaultOptions = {cast: true,merge: true};
import {Meteor} from 'meteor/meteor';
import {hasAccessFor} from '/imports/utils';
import {curry} from 'lodash/fp';
import {Roles} from 'meteor/alanning:roles';

export const SaveModel = (model,context,options = {}) => {
  context.set(model,{...options,...defaultOptions});
  return context.save();
};

export const CommitTransaction = curry((tranx,canCommit = true) => {
  if (canCommit) {
    return Meteor.wrapAsync(tranx.commit,tranx)();
  }
  return Meteor.wrapAsync(tranx.rollback,tranx)();
});

export const checkAccessFor = (roles) => {
  hasAccessFor({Meteor,Roles},roles,{statusCode: 403,message: 'Access denied!'});
};

export const normalizeMailUrl = (url) => {
  url = (url.indexOf('https://') !== -1) ? url.replace('http://','') : url;
  return url;
};
